package functionaltests.tests.invalidscenarios;

import calendar.interview.common.enums.Login;
import functionaltests.actions.InterviewHttpActions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

import java.io.IOException;

import static testsupport.examples.AbstractTestClass.TAG_FUNCTIONAL;

@Tag(TAG_FUNCTIONAL)
@ExtendWith(SpringExtension.class)
public class Invalid_Calls_To_Get_Interview_Arrangements_FT extends AbstractTestClass {

    private Example src = new Example();

    /**
     * Clean DB
     */
    @BeforeEach
    public void init() throws IOException {

        InterviewHttpActions.deleteAllByLogins();
        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Last_Start_Slot_Valid_String_Format,
                        localDateTime_Last_End_Slot_Valid_String_Format,
                        Login.JPINTO),
                200);

    }


    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    public void should_Get_Error_Get_Interview_Arrangements_Invalid() throws IOException {

        InterviewHttpActions.getInterviewArrangements(
                null,
                null,
                400);

    }


    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    public void should_Get_Error_Get_Interview_Arrangements_Invalid_Candidate() throws IOException {

        InterviewHttpActions.getInterviewArrangements(
                src.login_Interviewers_List_String_Format,
                "",
                404);

        InterviewHttpActions.getInterviewArrangements(
                src.login_Interviewers_List_String_Format,
                interviewer_Login.name(),
                422);

    }

    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    public void should_Get_Error_Get_Interview_Arrangements_Invalid_Interviewer() throws IOException {

        InterviewHttpActions.getInterviewArrangements(
                null,
                Login.NGOMES.name(),
                400);

        InterviewHttpActions.getInterviewArrangements(
                src.login_Candidates_List_String_Format,
                Login.NGOMES.name(),
                422);

    }


}
