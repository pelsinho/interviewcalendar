package functionaltests.tests.invalidscenarios;

import calendar.interview.common.enums.Login;
import functionaltests.actions.InterviewHttpActions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

import java.io.IOException;

import static testsupport.examples.AbstractTestClass.TAG_FUNCTIONAL;

@Tag(TAG_FUNCTIONAL)
@ExtendWith(SpringExtension.class)
public class Invalid_Calls_To_Get_By_Login_FT extends AbstractTestClass {

    private Example src = new Example();

    /**
     * Clean DB
     * Create a User to Have some control data there
     */
    @BeforeEach
    public void init() throws IOException {
        InterviewHttpActions.deleteAllByLogins();
        InterviewHttpActions.createCalendarSlot(
                    src.getCreateCalendarDTO(
                    localDateTime_Last_Start_Slot_Valid_String_Format,
                    localDateTime_Last_End_Slot_Valid_String_Format,
                    Login.JPINTO),
                200);
    }

    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    public void expectError_Get_By_Login_User_Has_No_Register() throws IOException {
        InterviewHttpActions.getByLogin(Login.NGOMES.name(), 400);
    }

    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    public void should_Get_Error_Get_By_Login_User_Invalid() throws IOException {
        InterviewHttpActions.getByLogin("InvalidLogin", 400);
        InterviewHttpActions.getByLogin("", 404);
        InterviewHttpActions.getByLogin(null, 400);
    }

}
