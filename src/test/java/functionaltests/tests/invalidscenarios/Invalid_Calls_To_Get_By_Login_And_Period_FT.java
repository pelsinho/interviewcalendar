package functionaltests.tests.invalidscenarios;

import calendar.interview.common.enums.Login;
import functionaltests.actions.InterviewHttpActions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

import java.io.IOException;

import static testsupport.examples.AbstractTestClass.TAG_FUNCTIONAL;

@Tag(TAG_FUNCTIONAL)
@ExtendWith(SpringExtension.class)
public class Invalid_Calls_To_Get_By_Login_And_Period_FT extends AbstractTestClass {

    private Example src = new Example();

    /**
     * Clean DB
     * Create a User to Have some control data there
     */
    @BeforeEach
    public void init() throws IOException {
        InterviewHttpActions.deleteAllByLogins();
        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                    localDateTime_Last_Start_Slot_Valid_String_Format,
                    localDateTime_Last_End_Slot_Valid_String_Format,
                    Login.NGOMES),
                200);
    }


    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    public void should_Get_Error_Get_By_Login_And_Period_User_Invalid() throws IOException {
        InterviewHttpActions.getByLoginAndPeriod("InvalidLogin", localDateTime_Last_Start_Slot_Valid_String_Format, localDateTime_Last_End_Slot_Valid_String_Format, 400);
        InterviewHttpActions.getByLoginAndPeriod("", localDateTime_Last_Start_Slot_Valid_String_Format, localDateTime_Last_End_Slot_Valid_String_Format, 404);
        InterviewHttpActions.getByLoginAndPeriod(null, localDateTime_Last_Start_Slot_Valid_String_Format, localDateTime_Last_End_Slot_Valid_String_Format, 400);
    }

    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    public void expectError_Get_By_Login_And_Period_Invalid_Start_Date() throws IOException {
        InterviewHttpActions.getByLoginAndPeriod(Login.NGOMES.name(), localDateTime_Invalid_String_Format_1, localDateTime_Last_End_Slot_Valid_String_Format, 400);
        InterviewHttpActions.getByLoginAndPeriod(Login.NGOMES.name(), localDateTime_Invalid_String_Format_2, localDateTime_Last_End_Slot_Valid_String_Format, 400);
        InterviewHttpActions.getByLoginAndPeriod(Login.NGOMES.name(), localDateTime_Invalid_String_Format_3, localDateTime_Last_End_Slot_Valid_String_Format, 400);
        InterviewHttpActions.getByLoginAndPeriod(Login.NGOMES.name(), localDateTime_Invalid_String_Format_4, localDateTime_Last_End_Slot_Valid_String_Format, 400);
        InterviewHttpActions.getByLoginAndPeriod(Login.NGOMES.name(), localDateTime_Invalid_String_Format_5, localDateTime_Last_End_Slot_Valid_String_Format, 400);
        InterviewHttpActions.getByLoginAndPeriod(Login.NGOMES.name(), localDateTime_Invalid_Hour_19_String_Format, localDateTime_Last_End_Slot_Valid_String_Format, 400);
        InterviewHttpActions.getByLoginAndPeriod(Login.NGOMES.name(), src.getLocalDateTime_String_Format("31", "12", "18"), localDateTime_Last_End_Slot_Valid_String_Format, 400);
    }

    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    public void expectError_Get_By_Login_And_Period_Invalid_End_Date() throws IOException {
        InterviewHttpActions.getByLoginAndPeriod(Login.NGOMES.name(), localDateTime_Last_Start_Slot_Valid_String_Format, localDateTime_Invalid_Hour_08_String_Format, 400);
        InterviewHttpActions.getByLoginAndPeriod(Login.NGOMES.name(), localDateTime_Last_Start_Slot_Valid_String_Format, src.getLocalDateTime_String_Format("31", "12", "16"), 400);
    }


}
