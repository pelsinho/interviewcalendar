package functionaltests.tests.invalidscenarios;

import calendar.interview.common.enums.Login;
import functionaltests.actions.InterviewHttpActions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

import java.io.IOException;

import static testsupport.examples.AbstractTestClass.TAG_FUNCTIONAL;

@Tag(TAG_FUNCTIONAL)
@ExtendWith(SpringExtension.class)
public class Invalid_Calls_To_Create_Calendar_FT extends AbstractTestClass {

    private Example src = new Example();

    /**
     * Clean DB
     */
    @BeforeEach
    public void init() {
        InterviewHttpActions.deleteAllByLogins();
    }

    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    public void should_Get_Error_Create_Calendar_Invalid_User() throws IOException {

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Last_Start_Slot_Valid_String_Format,
                        localDateTime_Last_End_Slot_Valid_String_Format,
                        null),
                400);

    }

    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    public void should_Get_Error_Create_Calendar_Invalid_Start_Date() throws IOException {

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Invalid_Year_String_Format,
                        localDateTime_Last_End_Slot_Valid_String_Format,
                        Login.NGOMES),
                400);

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Invalid_String_Format_1,
                        localDateTime_Last_End_Slot_Valid_String_Format,
                        Login.NGOMES),
                400);

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Invalid_String_Format_2,
                        localDateTime_Last_End_Slot_Valid_String_Format,
                        Login.NGOMES),
                400);

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Invalid_String_Format_3,
                        localDateTime_Last_End_Slot_Valid_String_Format,
                        Login.NGOMES),
                400);

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Invalid_String_Format_4,
                        localDateTime_Last_End_Slot_Valid_String_Format,
                        Login.NGOMES),
                400);

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Invalid_String_Format_5,
                        localDateTime_Last_End_Slot_Valid_String_Format,
                        Login.NGOMES),
                400);

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Invalid_Hour_08_String_Format,
                        localDateTime_Last_End_Slot_Valid_String_Format,
                        Login.NGOMES),
                400);

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Invalid_Hour_19_String_Format,
                        localDateTime_Last_End_Slot_Valid_String_Format,
                        Login.NGOMES),
                400);

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Last_End_Slot_Valid_String_Format,
                        localDateTime_Last_End_Slot_Valid_String_Format,
                        Login.NGOMES),
                400);

    }


    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    public void should_Get_Error_Create_Calendar_Invalid_End_Date() throws IOException {

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Last_Start_Slot_Valid_String_Format,
                        localDateTime_Invalid_Year_String_Format,
                        Login.NGOMES),
                400);

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Last_Start_Slot_Valid_String_Format,
                        localDateTime_Invalid_String_Format_1,
                        Login.NGOMES),
                400);

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Last_Start_Slot_Valid_String_Format,
                        localDateTime_Invalid_String_Format_2,
                        Login.NGOMES),
                400);

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Last_Start_Slot_Valid_String_Format,
                        localDateTime_Invalid_String_Format_3,
                        Login.NGOMES),
                400);

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Last_Start_Slot_Valid_String_Format,
                        localDateTime_Invalid_String_Format_4,
                        Login.NGOMES),
                400);

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Last_Start_Slot_Valid_String_Format,
                        localDateTime_Invalid_String_Format_5,
                        Login.NGOMES),
                400);

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Last_Start_Slot_Valid_String_Format,
                        localDateTime_Invalid_Hour_08_String_Format,
                        Login.NGOMES),
                400);

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Last_Start_Slot_Valid_String_Format,
                        localDateTime_Invalid_Hour_19_String_Format,
                        Login.NGOMES),
                400);

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        localDateTime_Last_Start_Slot_Valid_String_Format,
                        localDateTime_Last_Start_Slot_Valid_String_Format,
                        Login.NGOMES),
                422);

    }


}
