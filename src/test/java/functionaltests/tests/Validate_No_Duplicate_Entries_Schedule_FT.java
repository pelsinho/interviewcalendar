package functionaltests.tests;

import calendar.interview.common.enums.Login;
import calendar.interview.data.dto.UserReportDTO;
import functionaltests.actions.InterviewHttpActions;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;
import testsupport.validation.ValidateObject;

import java.io.IOException;

import static testsupport.examples.AbstractTestClass.TAG_FUNCTIONAL;

@Tag(TAG_FUNCTIONAL)
@ExtendWith(SpringExtension.class)
public class Validate_No_Duplicate_Entries_Schedule_FT extends AbstractTestClass {

    private Example src = new Example();

    /**
     * Clean DB
     */
    @BeforeEach
    public void init() {
        InterviewHttpActions.deleteAllByLogins();
    }

    /**
     * This is going to validate that we are not going to allow duplicate slots in schedule calendar DB
     */
    @Test
    public void should_Validate_No_Duplicate_Entries_Schedule_FT_Test() throws IOException {

        /**
         * First Lets Check , if really no data for the Candidate
         */

        InterviewHttpActions.getByLogin(Login.NGOMES.name(), 400);

        /**
         * So Let´s create a user with one full day schedule - 9 slots
         */

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                    src.getLocalDateTime_String_Format("31", "12", "09"),
                    src.getLocalDateTime_String_Format("31", "12", "18"),
                    Login.NGOMES),
                200);

        /**
         * Now Lets get By Login
         */

        UserReportDTO userReportDTO_1 = InterviewHttpActions.getByLogin(Login.NGOMES.name(), 200);
        ValidateObject.validate_UserReportDTO(userReportDTO_1, Login.NGOMES, 9);

        /**
         * No Lets create using same data
         */

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                    src.getLocalDateTime_String_Format("31", "12", "09"),
                    src.getLocalDateTime_String_Format("31", "12", "18"),
                    Login.NGOMES),
                200);

        /**
         * Now Lets get By Login Again
         */

        UserReportDTO userReportDTO_2 = InterviewHttpActions.getByLogin(Login.NGOMES.name(), 200);
        ValidateObject.validate_UserReportDTO(userReportDTO_2, Login.NGOMES, 9);


        /**
         * Should not have any new entries in the second get process
         */

        Assert.assertEquals(userReportDTO_1.getCalendarList().size(), userReportDTO_2.getCalendarList().size());

        /**
         * No Lets create using different data, one more 1 day
         */

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                    src.getLocalDateTime_String_Format("30", "12", "09"),
                    src.getLocalDateTime_String_Format("31", "12", "18"),
                    Login.NGOMES),
                200);

        /**
         * Now Lets get By Login Again, expect 9 slots from day 1 + 9 slots from day 2
         */

        UserReportDTO userReportDTO_3 = InterviewHttpActions.getByLogin(Login.NGOMES.name(), 200);
        ValidateObject.validate_UserReportDTO(userReportDTO_3, Login.NGOMES, 18);

    }







}
