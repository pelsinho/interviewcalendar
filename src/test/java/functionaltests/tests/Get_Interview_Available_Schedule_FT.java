package functionaltests.tests;

import calendar.interview.common.enums.Login;
import calendar.interview.data.dto.InterviewAvailableScheduleReportDTO;
import functionaltests.actions.InterviewHttpActions;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

import java.io.IOException;
import java.util.List;

import static testsupport.examples.AbstractTestClass.TAG_FUNCTIONAL;

@Tag(TAG_FUNCTIONAL)
@Slf4j
@ExtendWith(SpringExtension.class)
public class Get_Interview_Available_Schedule_FT extends AbstractTestClass {

    private Example src = new Example();

    /**
     * Clean DB
     */
    @BeforeEach
    public void init() {
        InterviewHttpActions.deleteAllByLogins();
    }

    /**
     * This is going to test the whole flow
     */
    @Test
    public void should_Get_Interview_Available_Schedule() throws IOException {

        /**
         * CREATE CANDIDATES AND INTERVIEWERS TO VALIDATE THIS PROCESS
         */
        createCandidates();
        createInterviewers();

        /**
         * CANDIDATE : NGOMES - 09:00 TO 18:00 - DECEMBER 31
         * INTERVIEW : KMOTA 09:00 TO 12:00 - DECEMBER
         * INTERVIEW : RSANCHES 12:00 TO 18:00 - DECEMBER
         * Result : 0 - No Schedule Matches for All of them
         */

        InterviewAvailableScheduleReportDTO interviewArrangements = InterviewHttpActions.getInterviewArrangements(
                List.of(Login.RSANCHES.name(), Login.KMOTA.name()),
                Login.NGOMES.name(),
                200);

        Assert.assertEquals(0, interviewArrangements.getInterviewAvailableSchedules().size());

        /**
         * CANDIDATE : NGOMES - 09:00 TO 18:00 - DECEMBER 31
         * INTERVIEW : HMOURA 11:00 TO 13:00 - DECEMBER
         * INTERVIEW : RSANCHES 12:00 TO 18:00 - DECEMBER
         * Result : 1 - From 12 to 13
         */

        interviewArrangements = InterviewHttpActions.getInterviewArrangements(
                List.of(Login.RSANCHES.name(), Login.HMOURA.name()),
                Login.NGOMES.name(),
                200);

        Assert.assertEquals(1, interviewArrangements.getInterviewAvailableSchedules().size());


        /**
         * CANDIDATE : JPINTO - 09:00 TO 18:00 - DECEMBER 30
         * INTERVIEW : HMOURA 11:00 TO 13:00 - DECEMBER
         * INTERVIEW : RSANCHES 12:00 TO 18:00 - DECEMBER
         * Result : 0 - No Schedule Matches for All of them (JPINTO Different Day)
         */

        interviewArrangements = InterviewHttpActions.getInterviewArrangements(
                List.of(Login.RSANCHES.name(), Login.HMOURA.name()),
                Login.JPINTO.name(),
                200);

        Assert.assertEquals(1, interviewArrangements.getInterviewAvailableSchedules().size());


        /**
         * CANDIDATE : NGOMES - 09:00 TO 18:00 - DECEMBER 31
         * INTERVIEW : HMOURA 11:00 TO 13:00 - DECEMBER
         * INTERVIEW : RSANCHES 12:00 TO 18:00 - DECEMBER
         * Result : 1 - From 12 to 13
         */

        interviewArrangements = InterviewHttpActions.getInterviewArrangements(
                List.of(Login.RSANCHES.name(), Login.HMOURA.name()),
                Login.NGOMES.name(),
                200);

        Assert.assertEquals(1, interviewArrangements.getInterviewAvailableSchedules().size());


        /**
         * CANDIDATE : NGOMES - 09:00 TO 18:00 - DECEMBER 31
         * INTERVIEW : PCOELHO 10:00 TO 15:00 - DECEMBER 31
         * INTERVIEW : RSANCHES 12:00 TO 18:00 - DECEMBER 31
         * Result : 3 - From 12 to 13, 13 TO 14, 14 TO 15
         */

        interviewArrangements = InterviewHttpActions.getInterviewArrangements(
                List.of(Login.RSANCHES.name(), Login.PCOELHO.name()),
                Login.NGOMES.name(),
                200);

        Assert.assertEquals(3, interviewArrangements.getInterviewAvailableSchedules().size());


        /**
         * CANDIDATE : NGOMES - 09:00 TO 18:00 - DECEMBER 31
         * INTERVIEW : PCOELHO 10:00 TO 15:00 - DECEMBER 31
         * INTERVIEW : RSANCHES 12:00 TO 18:00 - DECEMBER 31
         * INTERVIEW : TMOTA 09:00 TO 18:00 - DECEMBER 30
         * Result : 0 - TMOTA only on December 30
         */

        interviewArrangements = InterviewHttpActions.getInterviewArrangements(
                List.of(Login.RSANCHES.name(), Login.PCOELHO.name(), Login.TMOTA.name()),
                Login.NGOMES.name(),
                200);

        Assert.assertEquals(0, interviewArrangements.getInterviewAvailableSchedules().size());


        /**
         * CANDIDATE : NGOMES - 09:00 TO 18:00 - DECEMBER 31
         * INTERVIEW : PCOELHO 10:00 TO 15:00 - DECEMBER 31
         * INTERVIEW : RSANCHES 12:00 TO 18:00 - DECEMBER 31
         * INTERVIEW : HMOURA 11:00 TO 13:00 - DECEMBER 31
         * Result : 1 - From 12 TO 13
         */

        interviewArrangements = InterviewHttpActions.getInterviewArrangements(
                List.of(Login.RSANCHES.name(), Login.PCOELHO.name(), Login.HMOURA.name()),
                Login.NGOMES.name(),
                200);

        Assert.assertEquals(1, interviewArrangements.getInterviewAvailableSchedules().size());


    }



    private void createInterviewers() throws IOException {

        /**
         * Lets first create a interview
         * KMOTA
         * From 09 - 12 - Same Day of Candidate - NGOMES
         */
        InterviewHttpActions.createCalendarSlot(
                    src.getCreateCalendarDTO(
                    src.getLocalDateTime_String_Format("31", "12", "09"),
                    src.getLocalDateTime_String_Format("31", "12", "12"),
                    Login.KMOTA),
                200);

        /**
         * Lets first create a interview
         * RSANCHES
         * From 12 - 18 - Same Day of Candidate - NGOMES
         */
        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                    src.getLocalDateTime_String_Format("31", "12", "12"),
                    src.getLocalDateTime_String_Format("31", "12", "18"),
                    Login.RSANCHES),
                200);

        /**
         * Lets first create a interview
         * HMOURA
         * From 11 - 13 - Same Day of Candidate - NGOMES
         */
        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                    src.getLocalDateTime_String_Format("31", "12", "11"),
                    src.getLocalDateTime_String_Format("31", "12", "13"),
                    Login.HMOURA),
                200);

        /**
         * Lets first create a interview
         * PCOELHO
         * From 10 - 15 - Same Day of Candidate - NGOMES
         */
        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                    src.getLocalDateTime_String_Format("31", "12", "10"),
                    src.getLocalDateTime_String_Format("31", "12", "15"),
                    Login.PCOELHO),
                200);

        /**
         * Lets first create a interview
         * TMOTA
         * From 09 - 18 - Different Day of Candidate - NGOMES
         */
        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                    src.getLocalDateTime_String_Format("30", "12", "11"),
                    src.getLocalDateTime_String_Format("30", "12", "13"),
                    Login.TMOTA),
                200);

    }


    private void createCandidates() throws IOException {

        /**
         * Lets create our first candidate (MAIN ONE)
         * NGOMES
         * From 09 - 18
         */
        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                    src.getLocalDateTime_String_Format("31", "12", "09"),
                    src.getLocalDateTime_String_Format("31", "12", "18"),
                    Login.NGOMES),
                200);

        /**
         * Lets create our second candidate (BACKUP ONE)
         * JPINTO
         * From 09 - 18
         */
        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                    src.getLocalDateTime_String_Format("31", "12", "09"),
                    src.getLocalDateTime_String_Format("31", "12", "18"),
                    Login.JPINTO),
                200);

    }


}
