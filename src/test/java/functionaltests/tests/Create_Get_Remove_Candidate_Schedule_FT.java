package functionaltests.tests;

import calendar.interview.common.enums.Login;
import calendar.interview.data.dto.UserReportDTO;
import functionaltests.actions.InterviewHttpActions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;
import testsupport.validation.ValidateObject;

import java.io.IOException;

import static testsupport.examples.AbstractTestClass.TAG_FUNCTIONAL;

@Tag(TAG_FUNCTIONAL)
@ExtendWith(SpringExtension.class)
public class Create_Get_Remove_Candidate_Schedule_FT extends AbstractTestClass {

    private Example src = new Example();

    /**
     * Clean DB
     */
    @BeforeEach
    public void init() {
        InterviewHttpActions.deleteAllByLogins();
    }

    /**
     * This is going to test the whole flow
     */
    @Test
    public void create_Get_Remove_Candidate_Schedule() throws IOException {

        /**
         * Get Random Login
         */

        Login login = src.getRandomLogin();

        /**
         * First Lets Check , if really no data for the Candidate
         */

        InterviewHttpActions.getByLogin(login.name(), 400);

        /**
         * So Let´s create a user with one full day schedule
         */

        InterviewHttpActions.createCalendarSlot(
                    src.getCreateCalendarDTO(
                        src.getLocalDateTime_String_Format("31", "12", "09"),
                        src.getLocalDateTime_String_Format("31", "12", "18"),
                        login),
                200);

        /**
         * Now Lets get By Login
         */

        UserReportDTO userReportDTO = InterviewHttpActions.getByLogin(login.name(), 200);
        ValidateObject.validate_UserReportDTO(userReportDTO, login, 9);

        /**
         * Now Lets get By Login and period
         */

        userReportDTO = InterviewHttpActions.getByLoginAndPeriod(
                login.name(),
                src.getLocalDateTime_String_Format("31", "12", "09"),
                src.getLocalDateTime_String_Format("31", "12", "12"),
                200);
        ValidateObject.validate_UserReportDTO(userReportDTO, login, 3);

        /**
         * Now Lets Remove By Login and period
         */

        InterviewHttpActions.removeByLoginAndPeriod(
                login.name(),
                src.getLocalDateTime_String_Format("31", "12", "09"),
                src.getLocalDateTime_String_Format("31", "12", "12"),
                200);

        /**
         * Now Lets get By Login
         * Now its 6 the size of Calendar, because we removed 3
         */

        userReportDTO = InterviewHttpActions.getByLogin(login.name(), 200);
        ValidateObject.validate_UserReportDTO(userReportDTO, login, 6);

        /**
         * Now Lets get By Login and period
         * Now should be no valid, we´ve remove this schedules
         */

         InterviewHttpActions.getByLoginAndPeriod(
                login.name(),
                src.getLocalDateTime_String_Format("31", "12", "09"),
                src.getLocalDateTime_String_Format("31", "12", "12"),
                400);

        /**
         * Now Lets get By Login and period
         * But the rest of the day should be there available to schedule a meeting
         */

        userReportDTO = InterviewHttpActions.getByLoginAndPeriod(
                login.name(),
                src.getLocalDateTime_String_Format("31", "12", "12"),
                src.getLocalDateTime_String_Format("31", "12", "18"),
                200);
        ValidateObject.validate_UserReportDTO(userReportDTO, login, 6);


    }







}
