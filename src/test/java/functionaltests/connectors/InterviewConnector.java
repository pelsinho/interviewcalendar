package functionaltests.connectors;

import calendar.interview.common.predicates.GenericPredicates;
import calendar.interview.data.dto.CreateCalendarDTO;
import org.apache.http.HttpResponse;
import testsupport.conectors.ApacheHttpConnector;

import java.io.IOException;
import java.util.List;

import static functionaltests.connectors.InterviewEndpoint.*;

public class InterviewConnector {

    public static HttpResponse createCalendarSpot(
            CreateCalendarDTO createCalendarDTO) throws IOException {

        String url = CREATE_CALENDAR_SPOT.getEndpoint();
        return new ApacheHttpConnector().httpPost(url, createCalendarDTO);
    }

    public static HttpResponse getInterviewArrangements(
            List<String> interviewersLogins,
            String candidateLogin) throws IOException {

        String url;

        if (GenericPredicates.checkIfNullOrEmpty.negate().test(interviewersLogins)) {
            String adjustInterviewLogins = interviewersLogins.toString()
                    .replace("[", "")
                    .replace("]", "")
                    .replace(" ", "");
            url = String.format(GET_INTERVIEW_ARRANGEMENTS.getEndpoint(), adjustInterviewLogins, candidateLogin);
        } else {
            url = String.format(GET_INTERVIEW_ARRANGEMENTS.getEndpoint(), interviewersLogins, candidateLogin);
        }

        return new ApacheHttpConnector().httpGet(url);

    }

    public static HttpResponse getByLogin(String login) throws IOException {

        String url = String.format(GET_BY_LOGIN.getEndpoint(), login);
        return new ApacheHttpConnector().httpGet(url);
    }

    public static HttpResponse getByLoginAndPeriod(
            String login,
            String startLocalDateTime,
            String endLocalDateTime) throws IOException {

        String url = String.format(GET_BY_LOGIN_AND_PERIOD.getEndpoint(), login, startLocalDateTime, endLocalDateTime);
        return new ApacheHttpConnector().httpGet(url);

    }

    public static HttpResponse removeByLogin(String login) throws IOException {

        String url = String.format(REMOVE_BY_LOGIN.getEndpoint(), login);
        return new ApacheHttpConnector().httpDelete(url);
    }

    public static HttpResponse removeByLoginAndPeriod(
            String login,
            String startLocalDateTime,
            String endLocalDateTime) throws IOException {

        String url = String.format(REMOVE_BY_LOGIN_AND_PERIOD.getEndpoint(), login, startLocalDateTime, endLocalDateTime);
        return new ApacheHttpConnector().httpDelete(url);
    }


}
