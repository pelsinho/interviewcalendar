package functionaltests.connectors;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum InterviewEndpoint {

    MAIN_URL("http://localhost:8080"),

    CALENDAR(MAIN_URL.getEndpoint() + "/calendar"),

    CREATE_CALENDAR_SPOT(CALENDAR.getEndpoint()),

    GET_INTERVIEW_ARRANGEMENTS(CALENDAR.getEndpoint() + "/interviewers/%s/candidate/%s"),

    GET_BY_LOGIN(CALENDAR.getEndpoint() + "/byLogin/%s"),

    GET_BY_LOGIN_AND_PERIOD(CALENDAR.getEndpoint() + "/byLogin/%s/start/%s/end/%s"),

    REMOVE_BY_LOGIN(CALENDAR.getEndpoint() + "/byLogin/%s"),

    REMOVE_BY_LOGIN_AND_PERIOD(CALENDAR.getEndpoint() + "/byLogin/%s/start/%s/end/%s");

    private final String endpoint;

}
