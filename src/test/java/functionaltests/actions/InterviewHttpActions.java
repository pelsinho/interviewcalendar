package functionaltests.actions;

import calendar.interview.common.enums.Login;
import calendar.interview.data.dto.CreateCalendarDTO;
import calendar.interview.data.dto.InterviewAvailableScheduleReportDTO;
import calendar.interview.data.dto.UserReportDTO;
import functionaltests.connectors.InterviewConnector;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import testsupport.conectors.ApacheHttpConnector;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static testsupport.examples.AbstractTestClass.EXPECT_ERROR_STATUS_LIST;

@Slf4j
public class InterviewHttpActions {


    /**
     * CREATE DATA
     */

    public static UUID createCalendarSlot(
            CreateCalendarDTO createCalendarDTO,
            Integer expectedStatus) throws IOException {

        HttpResponse httpResponse = InterviewConnector.createCalendarSpot(createCalendarDTO);

        ApacheHttpConnector.validateStatus(expectedStatus, httpResponse);

        if (EXPECT_ERROR_STATUS_LIST.contains(expectedStatus)) return null;

        return ApacheHttpConnector.readResponse(httpResponse, UUID.class);

    }


    /**
     * GET DATA
     */


    public static InterviewAvailableScheduleReportDTO getInterviewArrangements(
            List<String> interviewers,
            String candidateLogin,
            Integer expectedStatus) throws IOException {

        HttpResponse httpResponse = InterviewConnector.getInterviewArrangements(interviewers, candidateLogin);

        ApacheHttpConnector.validateStatus(expectedStatus, httpResponse);

        if (EXPECT_ERROR_STATUS_LIST.contains(expectedStatus)) return new InterviewAvailableScheduleReportDTO();

        return ApacheHttpConnector.readResponse(httpResponse, InterviewAvailableScheduleReportDTO.class);

    }


    public static UserReportDTO getByLogin(
            String login,
            Integer expectedStatus) throws IOException {

        HttpResponse httpResponse = InterviewConnector.getByLogin(login);

        ApacheHttpConnector.validateStatus(expectedStatus, httpResponse);

        if (EXPECT_ERROR_STATUS_LIST.contains(expectedStatus)) return new UserReportDTO();

        return ApacheHttpConnector.readResponse(httpResponse, UserReportDTO.class);

    }


    public static UserReportDTO getByLoginAndPeriod(
            String login,
            String startLocalDateTime,
            String endLocalDateTime,
            Integer expectedStatus) throws IOException {

        HttpResponse httpResponse = InterviewConnector.getByLoginAndPeriod(login, startLocalDateTime, endLocalDateTime);

        ApacheHttpConnector.validateStatus(expectedStatus, httpResponse);

        if (EXPECT_ERROR_STATUS_LIST.contains(expectedStatus)) return new UserReportDTO();

        return ApacheHttpConnector.readResponse(httpResponse, UserReportDTO.class);

    }

    /**
     * REMOVE DATA
     */

    public static void removeByLogin(
            String login,
            Integer expectedStatus) throws IOException {

        HttpResponse httpResponse = InterviewConnector.removeByLogin(login);

        ApacheHttpConnector.validateStatus(expectedStatus, httpResponse);

    }

    public static void removeByLoginAndPeriod(
            String login,
            String startLocalDateTime,
            String endLocalDateTime,
            Integer expectedStatus) throws IOException {

        HttpResponse httpResponse = InterviewConnector.removeByLoginAndPeriod(login, startLocalDateTime, endLocalDateTime);

        ApacheHttpConnector.validateStatus(expectedStatus, httpResponse);

    }

    public static void deleteAllByLogins() {

        Login[] allLogin = Login.values();
        Arrays.stream(allLogin).forEach(
                login -> {
                    try {
                        InterviewConnector.removeByLogin(login.name());
                    } catch (IOException e) {
                        log.info("Not Found. No need to Delete data.");
                    }
                });

    }




}
