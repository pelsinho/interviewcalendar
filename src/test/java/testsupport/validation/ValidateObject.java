package testsupport.validation;

import calendar.interview.common.enums.Login;
import calendar.interview.common.functions.GenericFunctions;
import calendar.interview.common.predicates.GenericPredicates;
import calendar.interview.data.dto.CalendarReportDTO;
import calendar.interview.data.dto.CreateCalendarDTO;
import calendar.interview.data.dto.UserReportDTO;
import calendar.interview.data.entities.CalendarDB;
import calendar.interview.data.entities.UserDB;
import calendar.interview.data.model.CreateCalendar;
import org.junit.Assert;

import java.util.List;

public class ValidateObject {


    public static void validate_UserReportDTO(UserReportDTO userReportDTO, Login login, Integer calendarListSize) {

        Assert.assertEquals(userReportDTO.getRoles(), login.getRole());
        Assert.assertEquals(userReportDTO.getLogin(), login.name());

        if (calendarListSize == 0) {
            Assert.assertTrue(GenericPredicates.checkIfNullOrEmpty.test(userReportDTO.getCalendarList()));
        } else {
            Assert.assertEquals(userReportDTO.getCalendarList().size(), (int) calendarListSize);
        }

    }


    public static void compare_CalendarReportDTOList_With_CalendarDBList(
            List<CalendarReportDTO> calendarReportDTOList, List<CalendarDB> calendarDBList) {

        Assert.assertNotNull(calendarReportDTOList);
        Assert.assertNotNull(calendarDBList);

        Assert.assertFalse(calendarReportDTOList.isEmpty());
        Assert.assertFalse(calendarDBList.isEmpty());

        Assert.assertEquals(calendarReportDTOList.size(), calendarDBList.size());

        for (int i=0; i<calendarReportDTOList.size(); i++) {

            Assert.assertEquals(calendarReportDTOList.get(i).getId(), calendarDBList.get(i).getId());
            Assert.assertEquals(calendarReportDTOList.get(i).getSlotDateTime(), calendarDBList.get(i).getSlotDateTime().toLocalDateTime());

        }

    }

    public static void compare_UserReportDTO_With_CalendarDBList(UserReportDTO userReportDTO, List<CalendarDB> calendarList) {

        Assert.assertNotNull(userReportDTO);
        Assert.assertNotNull(calendarList);

        Assert.assertFalse(calendarList.isEmpty());

        /**
         * User Side
         */

        Assert.assertEquals(userReportDTO.getId(), calendarList.get(0).getId());
        Assert.assertEquals(userReportDTO.getLogin(), calendarList.get(0).getUser().getLogin());
        Assert.assertEquals(userReportDTO.getRoles(), calendarList.get(0).getUser().getRoles());
        Assert.assertEquals(userReportDTO.getCalendarList().size(), calendarList.get(0).getUser().getCalendarList().size());

        /**
         * Calendar Side
         */

        for (int i=0; i<userReportDTO.getCalendarList().size(); i++) {

            Assert.assertEquals(calendarList.get(i).getId(), userReportDTO.getCalendarList().get(i).getId());
            Assert.assertEquals(calendarList.get(i).getSlotDateTime().toLocalDateTime(), userReportDTO.getCalendarList().get(i).getSlotDateTime());

        }


    }

    public static void compare_UserReportDTO_With_UserDB(UserReportDTO userReportDTO, UserDB userDB) {

        Assert.assertNotNull(userReportDTO);
        Assert.assertNotNull(userDB);

        /**
         * User Side
         */

        Assert.assertEquals(userReportDTO.getId(), userDB.getId());
        Assert.assertEquals(userReportDTO.getLogin(), userDB.getLogin());
        Assert.assertEquals(userReportDTO.getRoles(), userDB.getRoles());
        Assert.assertEquals(userReportDTO.getCalendarList().size(), userDB.getCalendarList().size());

        for (int i=0; i<userReportDTO.getCalendarList().size(); i++) {

            Assert.assertEquals(userDB.getCalendarList().get(i).getId(), userReportDTO.getCalendarList().get(i).getId());
            Assert.assertEquals(userDB.getCalendarList().get(i).getSlotDateTime().toLocalDateTime(), userReportDTO.getCalendarList().get(i).getSlotDateTime());

        }

    }


    public static void compare_CreateCalendarDTO_With_CreateCalendar(CreateCalendarDTO createCalendarDTO, CreateCalendar createCalendar) {

        Assert.assertNotNull(createCalendarDTO);
        Assert.assertNotNull(createCalendar);

        Assert.assertEquals(createCalendarDTO.getLogin(), createCalendar.getLogin());
        Assert.assertEquals(GenericFunctions.getSlotDateTimeFormatter.apply(createCalendarDTO.getStartCalendarLocalDateTime()), createCalendar.getStartCalendarLocalDateTime());
        Assert.assertEquals(GenericFunctions.getSlotDateTimeFormatter.apply(createCalendarDTO.getEndCalendarLocalDateTime()), createCalendar.getEndCalendarLocalDateTime());

    }
}
