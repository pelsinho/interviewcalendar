package testsupport.examples;


import calendar.interview.common.enums.Login;
import calendar.interview.common.functions.GenericFunctions;
import calendar.interview.data.dto.CreateCalendarDTO;
import calendar.interview.data.entities.CalendarDB;
import calendar.interview.data.entities.UserDB;
import calendar.interview.data.model.CreateCalendar;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

public abstract class AbstractTestClass {

    /**
     * MAIN TEST URL
     */

    public static final String MAIN_URL = "localhost";
    public static final String MAIN_PORT = "8080";


    /**
     * TEST TAGS
     */

    public static final String TAG_FUNCTIONAL = "functional";
    public static final String TAG_ENVIRONMENT = "environment";


    /**
     * Expect Error Status List
     */
    public static final List<Integer> EXPECT_ERROR_STATUS_LIST = List.of(400, 402, 404, 422, 500);



    /**
     * Constants
     */

    public final Integer seconds_In_A_Hour = 3600;


    /**
     * Local Date Time String
     */

    public final String localDateTime_Invalid_Year_String_Format = "31-12-" + Year.now().plusYears(100) + "-14";
    public final String localDateTime_Invalid_Saturday_String_Format = "04-09-" + Year.now() + "-14";
    public final String localDateTime_Invalid_Sunday_String_Format = "05-09-" + Year.now() + "-14";
    public final String localDateTime_Invalid_Start_Before_Today_String_Format = "10-12-" + Year.now().minusYears(2) + "-14";

    public final String localDateTime_Invalid_Hour_08_String_Format = "31-12-" + Year.now() + "-08";
    public final String localDateTime_Invalid_Hour_19_String_Format = "31-12-" + Year.now() + "-19";

    public final String localDateTime_Last_Start_Slot_Valid_String_Format = "31-12-" + Year.now() + "-17";
    public final String localDateTime_Last_End_Slot_Valid_String_Format = "31-12-" + Year.now() + "-18";

    public final String localDateTime_Invalid_String_Format_1 = "40-12-2020-22";
    public final String localDateTime_Invalid_String_Format_2 = "20-12-2020-22:00";
    public final String localDateTime_Invalid_String_Format_3 = "2000-12-20-22";
    public final String localDateTime_Invalid_String_Format_4 = "30-13-2020-22";
    public final String localDateTime_Invalid_String_Format_5 = "30-12-2020";

    /**
     * Local Date Time Object
     */

    public final LocalDateTime localDateTime_Invalid_Year = GenericFunctions.getSlotDateTimeFormatter.apply(localDateTime_Invalid_Year_String_Format);
    public final LocalDateTime localDateTime_Invalid_Saturday = GenericFunctions.getSlotDateTimeFormatter.apply(localDateTime_Invalid_Saturday_String_Format);
    public final LocalDateTime localDateTime_Invalid_Sunday = GenericFunctions.getSlotDateTimeFormatter.apply(localDateTime_Invalid_Sunday_String_Format);
    public final LocalDateTime localDateTime_Invalid_Start_Before_Today = GenericFunctions.getSlotDateTimeFormatter.apply(localDateTime_Invalid_Start_Before_Today_String_Format);

    public final LocalDateTime localDateTime_Invalid_Hour_08 = GenericFunctions.getSlotDateTimeFormatter.apply(localDateTime_Invalid_Hour_08_String_Format);
    public final LocalDateTime localDateTime_Invalid_Hour_19 = GenericFunctions.getSlotDateTimeFormatter.apply(localDateTime_Invalid_Hour_19_String_Format);

    public final LocalDateTime localDateTime_Last_Start_Slot_Valid = GenericFunctions.getSlotDateTimeFormatter.apply(localDateTime_Last_Start_Slot_Valid_String_Format);
    public final LocalDateTime localDateTime_Last_End_Slot_Valid = GenericFunctions.getSlotDateTimeFormatter.apply(localDateTime_Last_End_Slot_Valid_String_Format);


    /**
     * Login
     */

    public final Login interviewer_Login = Login.KMOTA;
    public final Login candidate_Login = Login.NGOMES;

    /**
     * Long
     */

    public final Long randomLong = 1 + (long) (Math.random() * (1000000 - 1));
    public final UUID randomUUID = UUID.randomUUID();

    /**
     * TimeStamp
     */

    public final Timestamp timestamp_valid_1 = Timestamp.from(Instant.now().plusSeconds(seconds_In_A_Hour));
    public final Timestamp timestamp_valid_2 = Timestamp.from(Instant.now().plusSeconds(seconds_In_A_Hour*2));
    public final Timestamp timestamp_valid_3 = Timestamp.from(Instant.now().plusSeconds(seconds_In_A_Hour*3));
    public final Timestamp timestamp_valid_4 = Timestamp.from(Instant.now().plusSeconds(seconds_In_A_Hour*4));
    public final Timestamp timestamp_valid_5 = Timestamp.from(Instant.now().plusSeconds(seconds_In_A_Hour*5));
    public final Timestamp timestamp_valid_6 = Timestamp.from(Instant.now().plusSeconds(seconds_In_A_Hour*6));

    public class Example {

        /**
         * Login List
         */

        public final List<Login> login_Interviewers_List;
        public final List<String> login_Interviewers_List_String_Format;
        public final List<Login> login_Candidates_List;
        public final List<String> login_Candidates_List_String_Format;
        public final List<Login> login_Interviewers_And_Candidates;

        /**
         * TimeStamp List
         */

        public final List<Timestamp> timestamp_List_1_2_3;


        /**
         * getUserDBWithDefaultCalendarDB
         */

        public UserDB getUserDBWithDefaultCalendarDB_1_2_3(Login login) {

            UserDB userDB = new UserDB();
            userDB.setId(randomUUID);
            userDB.setRoles(login.getRole());
            userDB.setLogin(login.name());
            userDB.setCalendarList(getDefaultCalendarDB_1_2_3(userDB));

            return userDB;

        }


        /**
         * getValidCreateCalendarDTO
         */

        public CreateCalendarDTO getCreateCalendarDTO(Login login) {

            CreateCalendarDTO createCalendarDTO = new CreateCalendarDTO();
            createCalendarDTO.setLogin(login);
            createCalendarDTO.setStartCalendarLocalDateTime(getLocalDateTime_String_Format("31", "12", "17"));
            createCalendarDTO.setEndCalendarLocalDateTime(getLocalDateTime_String_Format("31", "12", "18"));

            return createCalendarDTO;

        }


        public CreateCalendar getCreateCalendar(Login login) {

            CreateCalendar createCalendar = new CreateCalendar();
            createCalendar.setLogin(login);
            createCalendar.setStartCalendarLocalDateTime(getLocalDateTime_Day_LocalDateTime("31", "12", "17"));
            createCalendar.setEndCalendarLocalDateTime(getLocalDateTime_Day_LocalDateTime("31", "12", "18"));

            return createCalendar;

        }



        public CreateCalendarDTO getCreateCalendarDTO(String startCalendarLocalDateTime, String endCalendarLocalDateTime, Login login) {

            CreateCalendarDTO createCalendarDTO = new CreateCalendarDTO();
            createCalendarDTO.setLogin(login);
            createCalendarDTO.setStartCalendarLocalDateTime(startCalendarLocalDateTime);
            createCalendarDTO.setEndCalendarLocalDateTime(endCalendarLocalDateTime);

            return createCalendarDTO;

        }



        /**
         * getDefaultCalendarDB
         */

        public List<CalendarDB> getDefaultCalendarDB_1_2_3(UserDB userDB) {

            List<CalendarDB> calendarDBList = new ArrayList<>();

            calendarDBList.add(CalendarDB
                    .builder()
                    .id(UUID.randomUUID())
                    .user(userDB)
                    .slotDateTime(timestamp_valid_1)
                    .build());

            calendarDBList.add(CalendarDB
                    .builder()
                    .id(UUID.randomUUID())
                    .user(userDB)
                    .slotDateTime(timestamp_valid_2)
                    .build());

            calendarDBList.add(CalendarDB
                    .builder()
                    .id(UUID.randomUUID())
                    .user(userDB)
                    .slotDateTime(timestamp_valid_3)
                    .build());

            return calendarDBList;

        }

        public List<CalendarDB> getDefaultCalendarDB_1_2_3(Login login) {

            UserDB userDB = getUserDBWithDefaultCalendarDB_1_2_3(login);

            List<CalendarDB> calendarDBList = new ArrayList<>();

            calendarDBList.add(CalendarDB
                    .builder()
                    .id(UUID.randomUUID())
                    .user(userDB)
                    .slotDateTime(timestamp_valid_4)
                    .build());

            calendarDBList.add(CalendarDB
                    .builder()
                    .id(UUID.randomUUID())
                    .user(userDB)
                    .slotDateTime(timestamp_valid_5)
                    .build());

            calendarDBList.add(CalendarDB
                    .builder()
                    .id(UUID.randomUUID())
                    .user(userDB)
                    .slotDateTime(timestamp_valid_6)
                    .build());

            return calendarDBList;

        }

        /**
         * @param month - 2 Digits, 01, 10, etc.
         * @param hour - 2 Digits, 01, 08, 13, 23, etc.
         * @param day - 2 Digits, 01, 06, 07, 12, 24
         * Year - Current Year
         */
        public String getLocalDateTime_String_Format(String day, String month, String hour) {

            return day + "-" + month + "-" + Year.now() + "-" + hour;

        }

        public LocalDateTime getLocalDateTime_Day_LocalDateTime(String day, String month, String hour) {

            String localDateTime = getLocalDateTime_String_Format(day, month, hour);
            return GenericFunctions.getSlotDateTimeFormatter.apply(localDateTime);

        }

        public Login getRandomLogin() {

            return login_Interviewers_And_Candidates.get(new Random().nextInt(login_Interviewers_And_Candidates.size()));

        }

        public Example() {

            /**
             * Login List
             */

            login_Interviewers_List = List.of(Login.KMOTA, Login.HMOURA, Login.PCOELHO, Login.RSANCHES, Login.TMOTA);
            login_Candidates_List = List.of(Login.JPINTO, Login.NGOMES, Login.PPAULO, Login.RMELO, Login.PSANCHES);

            login_Interviewers_List_String_Format = login_Interviewers_List.stream().map(Enum::name).collect(Collectors.toList());
            login_Candidates_List_String_Format = login_Candidates_List.stream().map(Enum::name).collect(Collectors.toList());

            login_Interviewers_And_Candidates = new ArrayList<>();
            login_Interviewers_And_Candidates.addAll(login_Interviewers_List);
            login_Interviewers_And_Candidates.addAll(login_Candidates_List);

            /**
             * Timestamp List
             */

            timestamp_List_1_2_3 = List.of(timestamp_valid_1, timestamp_valid_2, timestamp_valid_3);

        }


    }


}