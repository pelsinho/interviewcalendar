package calendar.interview.data.mappers;

import calendar.interview.InterviewApplication;
import calendar.interview.common.enums.Login;
import calendar.interview.data.dto.CalendarReportDTO;
import calendar.interview.data.entities.UserDB;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;
import testsupport.validation.ValidateObject;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {InterviewApplication.class})
@ContextConfiguration
class CalendarDBListToCalendarReportDTOMapperTests extends AbstractTestClass {

    private Example src = new Example();

    /**
     * calendarDBListToCalendarReportDTOList
     */

    @Test
    void should_get_empty_CalendarReportDTOList_if_CalendarDBList_is_Null() {

        List<CalendarReportDTO> calendarReportDTOList = CalendarDBListToCalendarReportDTOListMapper.calendarDBListToCalendarReportDTOList(null);

        Assert.assertNotNull(calendarReportDTOList);
        Assert.assertTrue(calendarReportDTOList.isEmpty());

    }

    @Test
    void should_get_empty_CalendarReportDTOList_if_CalendarDBList_is_Empty() {

        List<CalendarReportDTO> calendarReportDTOList = CalendarDBListToCalendarReportDTOListMapper.calendarDBListToCalendarReportDTOList(new ArrayList<>());

        Assert.assertNotNull(calendarReportDTOList);
        Assert.assertTrue(calendarReportDTOList.isEmpty());

    }


    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    void should_get_valid_CalendarReportDTOList_from_CalendarDBList() {

        UserDB userDB = src.getUserDBWithDefaultCalendarDB_1_2_3(Login.NGOMES);

        List<CalendarReportDTO> calendarReportDTOList
                = CalendarDBListToCalendarReportDTOListMapper.calendarDBListToCalendarReportDTOList(userDB.getCalendarList());

        ValidateObject.compare_CalendarReportDTOList_With_CalendarDBList(
                calendarReportDTOList,
                userDB.getCalendarList());

    }


}
