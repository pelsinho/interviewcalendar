package calendar.interview.data.mappers;

import calendar.interview.InterviewApplication;
import calendar.interview.common.enums.Login;
import calendar.interview.data.dto.CreateCalendarDTO;
import calendar.interview.data.model.CreateCalendar;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;
import testsupport.validation.ValidateObject;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {InterviewApplication.class})
@ContextConfiguration
class CreateCalendarDTOToCreateCalendarMapperTests extends AbstractTestClass {

    private Example src = new Example();

    /**
     * userDBToUserReportDTO
     */

    @Test
    void should_get_empty_UserReportDTO_if_userDB_is_Null() {

        CreateCalendar createCalendar = CreateCalendarDTOToCreateCalendarMapper.createCalendarDTOToCreateCalendar(null);

        Assert.assertNotNull(createCalendar);
        Assert.assertNull(createCalendar.getLogin());
        Assert.assertNull(createCalendar.getStartCalendarLocalDateTime());
        Assert.assertNull(createCalendar.getEndCalendarLocalDateTime());

    }

    @Test
    void should_get_empty_UserReportDTO_if_userDB_is_Empty() {

        CreateCalendar createCalendar = CreateCalendarDTOToCreateCalendarMapper.createCalendarDTOToCreateCalendar(new CreateCalendarDTO());

        Assert.assertNotNull(createCalendar);
        Assert.assertNull(createCalendar.getLogin());
        Assert.assertNull(createCalendar.getStartCalendarLocalDateTime());
        Assert.assertNull(createCalendar.getEndCalendarLocalDateTime());

    }

    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    void should_get_valid_UserReportDTO_from_userDB() {

        CreateCalendarDTO createCalendarDTO = src.getCreateCalendarDTO(Login.NGOMES);

        CreateCalendar createCalendar = CreateCalendarDTOToCreateCalendarMapper.createCalendarDTOToCreateCalendar(createCalendarDTO);

        ValidateObject.compare_CreateCalendarDTO_With_CreateCalendar(
                createCalendarDTO,
                createCalendar);

    }

}
