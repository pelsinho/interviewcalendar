package calendar.interview.data.mappers;

import calendar.interview.InterviewApplication;
import calendar.interview.common.enums.Login;
import calendar.interview.data.dto.UserReportDTO;
import calendar.interview.data.entities.UserDB;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;
import testsupport.validation.ValidateObject;

import java.util.ArrayList;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {InterviewApplication.class})
@ContextConfiguration
class CalendarDBListToUserReportDTOMapperTests extends AbstractTestClass {

    private Example src = new Example();

    /**
     * calendarDBToUserReportDTO
     */

    @Test
    void should_get_empty_UserReportDTO_if_CalendarDBList_is_Null() {

        UserReportDTO userReportDTO = CalendarDBListToUserReportDTOMapper.calendarDBToUserReportDTO(null);
        Assert.assertNotNull(userReportDTO);
        Assert.assertNull(userReportDTO.getId());

    }

    @Test
    void should_get_empty_UserReportDTO_if_CalendarDBList_is_Empty() {

        UserReportDTO userReportDTO = CalendarDBListToUserReportDTOMapper.calendarDBToUserReportDTO(new ArrayList<>());
        Assert.assertNotNull(userReportDTO);
        Assert.assertNull(userReportDTO.getId());

    }


    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    void should_get_valid_UserReportDTO_from_CalendarDBList() {

        UserDB userDB = src.getUserDBWithDefaultCalendarDB_1_2_3(Login.NGOMES);

        UserReportDTO userReportDTO
                = CalendarDBListToUserReportDTOMapper.calendarDBToUserReportDTO(userDB.getCalendarList());

        ValidateObject.compare_UserReportDTO_With_CalendarDBList(
                userReportDTO,
                userDB.getCalendarList());

    }


}
