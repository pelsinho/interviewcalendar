package calendar.interview.common.functions;

import calendar.interview.InterviewApplication;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {InterviewApplication.class})
@ContextConfiguration
class GenericFunctionsTests extends AbstractTestClass {

    /**
     * getLocalDateTime
     */

    @Test
    void should_GetLocalDateTime_From_Null_List() {

        List<LocalDateTime> localDateTimeList = GenericFunctions.getLocalDateTime.apply(null);
        Assert.assertNotNull(localDateTimeList);
        Assert.assertTrue(localDateTimeList.isEmpty());

    }

    @Test
    void should_GetLocalDateTime_From_Empty_List() {

        List<LocalDateTime> localDateTimeList = GenericFunctions.getLocalDateTime.apply(new ArrayList<>());
        Assert.assertNotNull(localDateTimeList);
        Assert.assertTrue(localDateTimeList.isEmpty());

    }

    @Test
    void should_GetLocalDateTime_From_Valid_List() {

        List<LocalDateTime> localDateTimeList = GenericFunctions.getLocalDateTime.apply(List.of(timestamp_valid_1, timestamp_valid_2));
        Assert.assertNotNull(localDateTimeList);
        Assert.assertEquals(2, localDateTimeList.size());

    }


    /**
     * getMapValueAsStringBy
     */

    @Test
    void should_GetMapValueAsStringBy_From_Null() {

        String mapToString = GenericFunctions.getMapValueAsStringBy.apply(null, ",");
        Assert.assertNotNull(mapToString);
        Assert.assertTrue(mapToString.isEmpty());

    }


    @Test
    void should_GetMapValueAsStringBy_From_Empty() {

        String mapToString = GenericFunctions.getMapValueAsStringBy.apply(new HashMap<>(), ",");
        Assert.assertNotNull(mapToString);
        Assert.assertTrue(mapToString.isEmpty());

    }


    @Test
    void should_GetMapValueAsStringBy_From_Valid_Map() {

        Map<String, String> stringStringMap = Map.of("1", "Try", "2", "Check", "3", "Again");

        String mapToString = GenericFunctions.getMapValueAsStringBy.apply(stringStringMap, ",");

        Assert.assertTrue(mapToString.contains("Try"));
        Assert.assertTrue(mapToString.contains("Check"));
        Assert.assertTrue(mapToString.contains("Again"));

    }


    /**
     * getSlotDateTimeFormatter
     */

    @Test
    void should_Throw_NullPointerException_GetSlotDateTimeFormatter_From_Null() {
        Assert.assertThrows(NullPointerException.class, () -> GenericFunctions.getSlotDateTimeFormatter.apply(null));
    }

    @Test
    void should_Throw_DateTimeParseException_GetSlotDateTimeFormatter_From_Empty() {
        Assert.assertThrows(DateTimeParseException.class, () -> GenericFunctions.getSlotDateTimeFormatter.apply(""));
    }

    @Test
    void should_Throw_DateTimeParseException_GetSlotDateTimeFormatter_From_Invalid_Data() {

        Assert.assertThrows(DateTimeParseException.class, () -> GenericFunctions.getSlotDateTimeFormatter.apply(localDateTime_Invalid_String_Format_1));
        Assert.assertThrows(DateTimeParseException.class, () -> GenericFunctions.getSlotDateTimeFormatter.apply(localDateTime_Invalid_String_Format_2));
        Assert.assertThrows(DateTimeParseException.class, () -> GenericFunctions.getSlotDateTimeFormatter.apply(localDateTime_Invalid_String_Format_3));
        Assert.assertThrows(DateTimeParseException.class, () -> GenericFunctions.getSlotDateTimeFormatter.apply(localDateTime_Invalid_String_Format_4));
        Assert.assertThrows(DateTimeParseException.class, () -> GenericFunctions.getSlotDateTimeFormatter.apply(localDateTime_Invalid_String_Format_5));

    }

    @Test
    void should_GetSlotDateTimeFormatter_From_Valid_String() {

        LocalDateTime startLocalDateTime = GenericFunctions.getSlotDateTimeFormatter.apply(localDateTime_Last_Start_Slot_Valid_String_Format);
        Assert.assertEquals("2021-12-31T17:00", startLocalDateTime.toString());

        LocalDateTime endLocalDateTime = GenericFunctions.getSlotDateTimeFormatter.apply(localDateTime_Last_End_Slot_Valid_String_Format);
        Assert.assertEquals("2021-12-31T18:00", endLocalDateTime.toString());

    }

}
