package calendar.interview.common.suppliers;

import calendar.interview.InterviewApplication;
import calendar.interview.common.enums.Login;
import calendar.interview.data.entities.CalendarDB;
import calendar.interview.data.entities.UserDB;
import calendar.interview.data.model.CreateCalendar;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {InterviewApplication.class})
@ContextConfiguration
class CalendarSuppliersTests extends AbstractTestClass {

    private Example src = new Example();

    @Test
    void should_Get_Empty_CalendarDB_List_When_Sending_Null_Empty() {

        List<CalendarDB> calendarDBList = CalendarSuppliers.getCalendarDB(
                null, null).get();

        Assert.assertNotNull(calendarDBList);
        Assert.assertTrue(calendarDBList.isEmpty());

        calendarDBList = CalendarSuppliers.getCalendarDB(
                null, new UserDB()).get();

        Assert.assertNotNull(calendarDBList);
        Assert.assertTrue(calendarDBList.isEmpty());

        calendarDBList = CalendarSuppliers.getCalendarDB(
                new CreateCalendar(), null).get();

        Assert.assertNotNull(calendarDBList);
        Assert.assertTrue(calendarDBList.isEmpty());

        calendarDBList = CalendarSuppliers.getCalendarDB(
                new CreateCalendar(), new UserDB()).get();

        Assert.assertNotNull(calendarDBList);
        Assert.assertTrue(calendarDBList.isEmpty());

    }


    /**
     * Will Test if there is only NEW data in CalendarDBList
     */
    @Test
    void should_Get_New_CalendarDB_List_When_Sending_UserDB_Without_CalendarDB() {

        /**
         * Get User DB With Calendar 1_2_3 example
         * And clear it
         */
        UserDB userDB = src.getUserDBWithDefaultCalendarDB_1_2_3(Login.NGOMES);
        userDB.setCalendarList(new ArrayList<>());

        /**
         * Get Create Calendar
         */
        CreateCalendar createCalendar = src.getCreateCalendar(Login.NGOMES);

        /**
         * Execute Process
         */
        List<CalendarDB> calendarDBList = CalendarSuppliers.getCalendarDB(createCalendar, userDB).get();

        /**
         * Validate Size
         */
        Assert.assertEquals(1, calendarDBList.size());

    }

    /**
     * Will Test if we add new data to existent data in CalendarDBList
     */
    @Test
    void should_Get_Updated_CalendarDB_List_When_Sending_UserDB_With_CalendarDB() {

        /**
         * Get User DB With Calendar 1_2_3 example
         */
        UserDB userDB = src.getUserDBWithDefaultCalendarDB_1_2_3(Login.NGOMES);

        /**
         * Get Create Calendar
         */
        CreateCalendar createCalendar = src.getCreateCalendar(Login.NGOMES);

        /**
         * Execute Process
         */
        List<CalendarDB> calendarDBList = CalendarSuppliers.getCalendarDB(createCalendar, userDB).get();

        /**
         * Validate Size
         */
        Assert.assertEquals(4, calendarDBList.size());

    }

}
