package calendar.interview.common.log;

import calendar.interview.InterviewApplication;
import calendar.interview.common.enums.CalendarValidationPhase;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;
import testsupport.helpers.CoreHelpMethods;

import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {InterviewApplication.class})
@ContextConfiguration
class DefaultLogTests extends AbstractTestClass {


    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    void shouldGenerateDefaultLogError() {

        List<ILoggingEvent> loggingEventList = CoreHelpMethods.startGetLoggerMessagesWithLevel.apply(Level.ERROR);

        DefaultLog.defaultLogError(new Exception());

        CoreHelpMethods.loggingEventsContainsString.accept(loggingEventList, "- StackTrace :");


    }


    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    void shouldGenerateRegisterNotFoundLogError() {

        List<ILoggingEvent> loggingEventList = CoreHelpMethods.startGetLoggerMessagesWithLevel.apply(Level.ERROR);

        DefaultLog.registerNotFoundLogError(getClass().getSimpleName(), "login", "startLocalDateTime", "endLocalDateTime");

        CoreHelpMethods.loggingEventsContainsString.accept(loggingEventList, "Register Not Found");

    }


    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    void shouldGenerateValidationLogError() {

        List<ILoggingEvent> loggingEventList = CoreHelpMethods.startGetLoggerMessagesWithLevel.apply(Level.ERROR);

        DefaultLog.validationLogError(CalendarValidationPhase.CREATE_CALENDAR, "errorMsg");

        CoreHelpMethods.loggingEventsContainsString.accept(loggingEventList, "Error Messages");

    }


}
