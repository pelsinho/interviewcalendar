package calendar.interview.common.constants;

import calendar.interview.InterviewApplication;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

import java.util.regex.Pattern;

import static calendar.interview.common.constants.CalendarInformation.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {InterviewApplication.class})
@ContextConfiguration
class CalendarInformationTests extends AbstractTestClass {

    /**
     * SLOT_DATE_TIME_FORMAT
     */

    @Test
    void should_Be_Valid_Slot_Date_Time_Format() {

        Assert.assertEquals("dd-MM-yyyy-HH", SLOT_DATE_TIME_FORMAT);

    }

    @Test
    void should_Be_Invalid_Slot_Date_Time_Format() {

        Assert.assertNotEquals("dd/MM/yyyy/HH", SLOT_DATE_TIME_FORMAT);
        Assert.assertNotEquals("dd-MM-yyyy HH", SLOT_DATE_TIME_FORMAT);
        Assert.assertNotEquals("dd-MM-yyyy", SLOT_DATE_TIME_FORMAT);
        Assert.assertNotEquals("dd-MM-yyyy-H", SLOT_DATE_TIME_FORMAT);
        Assert.assertNotEquals(null, SLOT_DATE_TIME_FORMAT);
        Assert.assertNotEquals("", SLOT_DATE_TIME_FORMAT);

    }


    /**
     * SLOT_START_DATE_TIME_FORMAT_REGEX
     */

    @Test
    void should_Be_Valid_Start_Date_Regex_Format() {

        Assert.assertTrue(Pattern.compile(SLOT_DATE_TIME_FORMAT_REGEX).matcher(localDateTime_Last_Start_Slot_Valid_String_Format).find());
        Assert.assertTrue(Pattern.compile(SLOT_DATE_TIME_FORMAT_REGEX).matcher(localDateTime_Last_Start_Slot_Valid_String_Format).find());

    }

    @Test
    void should_Be_Invalid_Start_Date_Regex_Format() {

        Assert.assertFalse(Pattern.compile(SLOT_DATE_TIME_FORMAT_REGEX).matcher(localDateTime_Invalid_String_Format_1).find());
        Assert.assertFalse(Pattern.compile(SLOT_DATE_TIME_FORMAT_REGEX).matcher(localDateTime_Invalid_String_Format_2).find());
        Assert.assertFalse(Pattern.compile(SLOT_DATE_TIME_FORMAT_REGEX).matcher(localDateTime_Invalid_String_Format_3).find());
        Assert.assertFalse(Pattern.compile(SLOT_DATE_TIME_FORMAT_REGEX).matcher(localDateTime_Invalid_String_Format_4).find());
        Assert.assertFalse(Pattern.compile(SLOT_DATE_TIME_FORMAT_REGEX).matcher(localDateTime_Invalid_String_Format_5).find());

    }


    /**
     * SLOT_END_DATE_TIME_FORMAT_REGEX
     */

    @Test
    void should_Be_Valid_End_Date_Regex_Format() {

        Assert.assertTrue(Pattern.compile(SLOT_END_DATE_TIME_FORMAT_REGEX).matcher(localDateTime_Last_Start_Slot_Valid_String_Format).find());
        Assert.assertTrue(Pattern.compile(SLOT_END_DATE_TIME_FORMAT_REGEX).matcher(localDateTime_Last_End_Slot_Valid_String_Format).find());


    }

    @Test
    void should_Be_Invalid_End_Date_Regex_Format() {

        Assert.assertFalse(Pattern.compile(SLOT_END_DATE_TIME_FORMAT_REGEX).matcher(localDateTime_Invalid_String_Format_1).find());
        Assert.assertFalse(Pattern.compile(SLOT_END_DATE_TIME_FORMAT_REGEX).matcher(localDateTime_Invalid_String_Format_2).find());
        Assert.assertFalse(Pattern.compile(SLOT_END_DATE_TIME_FORMAT_REGEX).matcher(localDateTime_Invalid_String_Format_3).find());
        Assert.assertFalse(Pattern.compile(SLOT_END_DATE_TIME_FORMAT_REGEX).matcher(localDateTime_Invalid_String_Format_4).find());
        Assert.assertFalse(Pattern.compile(SLOT_END_DATE_TIME_FORMAT_REGEX).matcher(localDateTime_Invalid_String_Format_5).find());

    }


}
