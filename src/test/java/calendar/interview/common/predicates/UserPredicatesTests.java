package calendar.interview.common.predicates;

import calendar.interview.InterviewApplication;
import calendar.interview.common.enums.Roles;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {InterviewApplication.class})
@ContextConfiguration
class UserPredicatesTests extends AbstractTestClass {

    private Example src = new Example();

    /**
     * allHaveThisRole
     */

    @Test
    void should_Get_True_All_Logins_Are_Candidates() {

        UserPredicates.allHaveThisRole.test(src.login_Candidates_List, Roles.CANDIDATE);

    }

    @Test
    void should_Get_True_All_Logins_Are_Interviewers() {

        UserPredicates.allHaveThisRole.test(src.login_Interviewers_List, Roles.INTERVIEWER);

    }


    @Test
    void should_Get_False_All_Logins_Are_Not_Candidates() {

        UserPredicates.allHaveThisRole.test(src.login_Interviewers_And_Candidates, Roles.CANDIDATE);

    }


    @Test
    void should_Get_False_All_Logins_Are_Not_Interviewers() {

        UserPredicates.allHaveThisRole.test(src.login_Interviewers_And_Candidates, Roles.INTERVIEWER);

    }



}
