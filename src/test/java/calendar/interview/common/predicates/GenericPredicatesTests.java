package calendar.interview.common.predicates;

import calendar.interview.InterviewApplication;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

import java.util.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {InterviewApplication.class})
@ContextConfiguration
class GenericPredicatesTests extends AbstractTestClass {

    /**
     * checkIfNullOrEmpty
     */

    @Test
    void should_Get_True_When_Object_Is_Empty() {

        Assert.assertTrue(GenericPredicates.checkIfNullOrEmpty.test(new ArrayList<>()));
        Assert.assertTrue(GenericPredicates.checkIfNullOrEmpty.test(new HashMap<>()));
        Assert.assertTrue(GenericPredicates.checkIfNullOrEmpty.test(new HashSet<>()));
        Assert.assertTrue(GenericPredicates.checkIfNullOrEmpty.test(""));

    }

    @Test
    void should_Get_True_When_Object_Is_Null() {

        Assert.assertTrue(GenericPredicates.checkIfNullOrEmpty.test(null));

    }

    @Test
    void should_Get_False_When_Object_Has_Data() {

        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(List.of("Test")));
        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(Map.of("Test", "Test")));
        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(Set.of("Test")));
        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test("Test"));

    }




}
