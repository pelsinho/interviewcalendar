package calendar.interview.common.predicates;

import calendar.interview.InterviewApplication;
import calendar.interview.data.entities.CalendarDB;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {InterviewApplication.class})
@ContextConfiguration
class CalendarPredicatesTests extends AbstractTestClass {

    private Example src = new Example();

    /**
    * isAWeekendDay
    */

    @Test
    void should_ThrownNullPointerException_isAWeekendDay_Null() {
        Assert.assertThrows(NullPointerException.class, () -> CalendarPredicates.isAWeekendDay.test(null));
    }

    @Test
    void should_return_true_if_isAWeekendDay() {
        Assert.assertTrue(CalendarPredicates.isAWeekendDay.test(DayOfWeek.SATURDAY));
        Assert.assertTrue(CalendarPredicates.isAWeekendDay.test(DayOfWeek.SUNDAY));
    }

    @Test
    void should_return_false_if_not_isAWeekendDay() {
        Assert.assertFalse(CalendarPredicates.isAWeekendDay.test(DayOfWeek.MONDAY));
        Assert.assertFalse(CalendarPredicates.isAWeekendDay.test(DayOfWeek.TUESDAY));
        Assert.assertFalse(CalendarPredicates.isAWeekendDay.test(DayOfWeek.WEDNESDAY));
        Assert.assertFalse(CalendarPredicates.isAWeekendDay.test(DayOfWeek.THURSDAY));
        Assert.assertFalse(CalendarPredicates.isAWeekendDay.test(DayOfWeek.FRIDAY));
    }


    /**
     * isValidSlotStartHour
     */

    @Test
    void should_ThrownNullPointerException_isValidSlotStartHour_Null() {
        Assert.assertThrows(NullPointerException.class, () -> CalendarPredicates.isValidSlotStartHour.test(null));
    }

    @Test
    void should_return_false_if_isInValidSlotStartHour() {
        Assert.assertFalse(CalendarPredicates.isValidSlotStartHour.test(localDateTime_Invalid_Hour_08));
        Assert.assertFalse(CalendarPredicates.isValidSlotStartHour.test(localDateTime_Invalid_Hour_19));
    }

    @Test
    void should_return_true_if_isValidSlotStartHour() {
        Assert.assertTrue(CalendarPredicates.isValidSlotStartHour.test(src.getLocalDateTime_Day_LocalDateTime("31", "12", "09")));
        Assert.assertTrue(CalendarPredicates.isValidSlotStartHour.test(src.getLocalDateTime_Day_LocalDateTime("31", "12", "17")));
    }


    /**
     * isValidSlotLocalDateTime
     */

    @Test
    void should_ThrownNullPointerException_isValidSlotLocalDateTime_Null() {
        Assert.assertThrows(NullPointerException.class, () -> CalendarPredicates.isValidSlotLocalDateTime.test(null));
    }

    @Test
    void should_return_false_if_isInValidSlotLocalDateTime() {
        Assert.assertFalse(CalendarPredicates.isValidSlotLocalDateTime.test(localDateTime_Invalid_Hour_08));
        Assert.assertFalse(CalendarPredicates.isValidSlotLocalDateTime.test(src.getLocalDateTime_Day_LocalDateTime("31", "12", "18")));
        Assert.assertFalse(CalendarPredicates.isValidSlotLocalDateTime.test(localDateTime_Invalid_Saturday));
        Assert.assertFalse(CalendarPredicates.isValidSlotLocalDateTime.test(localDateTime_Invalid_Sunday));
    }

    @Test
    void should_return_true_if_isValidSlotLocalDateTime() {
        Assert.assertTrue(CalendarPredicates.isValidSlotLocalDateTime.test(src.getLocalDateTime_Day_LocalDateTime("31", "12", "09")));;
        Assert.assertTrue(CalendarPredicates.isValidSlotLocalDateTime.test(src.getLocalDateTime_Day_LocalDateTime("31", "12", "17")));;
    }


    /**
     * isValidSlotEndHour
     */

    @Test
    void should_ThrownNullPointerException_isValidSlotEndHour_Null() {
        Assert.assertThrows(NullPointerException.class, () -> CalendarPredicates.isValidSlotEndHour.test(null));
    }

    @Test
    void should_return_false_if_isInValidSlotEndHour() {
        Assert.assertFalse(CalendarPredicates.isValidSlotEndHour.test(localDateTime_Invalid_Hour_08));
        Assert.assertFalse(CalendarPredicates.isValidSlotEndHour.test(localDateTime_Invalid_Hour_19));
        Assert.assertFalse(CalendarPredicates.isValidSlotEndHour.test(src.getLocalDateTime_Day_LocalDateTime("31", "12", "09")));
    }

    @Test
    void should_return_true_if_isValidSlotEndHour() {
        Assert.assertTrue(CalendarPredicates.isValidSlotEndHour.test(src.getLocalDateTime_Day_LocalDateTime("31", "12", "10")));
        Assert.assertTrue(CalendarPredicates.isValidSlotEndHour.test(src.getLocalDateTime_Day_LocalDateTime("31", "12", "17")));
        Assert.assertTrue(CalendarPredicates.isValidSlotEndHour.test(src.getLocalDateTime_Day_LocalDateTime("31", "12", "18")));
    }

    /**
     * isValidSlotEndYear
     */

    @Test
    void should_ThrownNullPointerException_isValidSlotEndYear_Null() {
        Assert.assertThrows(NullPointerException.class, () -> CalendarPredicates.isValidSlotEndYear.test(null));
    }

    @Test
    void should_return_true_if_isValidSlotEndYear() {
        Assert.assertTrue(CalendarPredicates.isValidSlotEndYear.test(src.getLocalDateTime_Day_LocalDateTime("31", "12", "18")));
    }


    @Test
    void should_return_false_if_isInValidSlotEndYear() {
        Assert.assertFalse(CalendarPredicates.isValidSlotEndYear.test(localDateTime_Invalid_Year));
        Assert.assertFalse(CalendarPredicates.isValidSlotEndYear.test(src.getLocalDateTime_Day_LocalDateTime("31", "12", "18").plusYears(1L)));

    }


    /**
     * isStartSlotDateBeforeEndSlotDate
     */

    @Test
    void should_ThrownNullPointerException_isStartSlotDateBeforeEndSlotDate_Null() {
        Assert.assertThrows(NullPointerException.class, () -> CalendarPredicates.isStartSlotDateBeforeEndSlotDate.test(null, null));
    }

    @Test
    void should_return_false_if_isStartSlotDateBeforeEndSlotDate() {

        Assert.assertFalse(CalendarPredicates.isStartSlotDateBeforeEndSlotDate.test(
                LocalDateTime.now(), LocalDateTime.now().minusYears(10)));
    }

    @Test
    void should_return_false_if_isStartSlotDateAfterEndSlotDate() {

        Assert.assertTrue(CalendarPredicates.isStartSlotDateBeforeEndSlotDate.test(
                LocalDateTime.now(), LocalDateTime.now().plusHours(10)));
    }


    /**
     * isStartSlotDateAfterToday
     */

    @Test
    void should_ThrownNullPointerException_isStartSlotDateAfterToday_Null() {
        Assert.assertThrows(NullPointerException.class, () -> CalendarPredicates.isStartSlotDateAfterToday.test(null));
    }

    @Test
    void should_return_true_if_isStartSlotDateAfterToday() {

        Assert.assertTrue(CalendarPredicates.isStartSlotDateAfterToday.test(
                LocalDateTime.now().plusYears(10)));
    }

    @Test
    void should_return_false_if_isStartSlotDateBeforeToday() {

        Assert.assertFalse(CalendarPredicates.isStartSlotDateAfterToday.test(
                LocalDateTime.now().minusYears(10)));
    }


    /**
     * slotAlreadyAllocated
     */

    @Test
    void should_return_false_if_slotAlreadyAllocated_Null() {
        Assert.assertFalse(CalendarPredicates.slotAlreadyAllocated.test(null, null));
    }

    @Test
    void should_return_true_if_slotAlreadyAllocated() {

        Assert.assertTrue(CalendarPredicates.slotAlreadyAllocated.test(
                timestamp_valid_1.toLocalDateTime(),
                List.of(CalendarDB.builder().slotDateTime(timestamp_valid_1).build())));
    }

    @Test
    void should_return_false_if_slot_not_Allocated() {

        Assert.assertFalse(CalendarPredicates.slotAlreadyAllocated.test(
                timestamp_valid_2.toLocalDateTime(),
                List.of(CalendarDB.builder().slotDateTime(timestamp_valid_1).build())));

    }


}
