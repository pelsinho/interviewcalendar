package calendar.interview.services.calendarvalidationbyphase.scheduleinterview;

import calendar.interview.InterviewApplication;
import calendar.interview.common.enums.CalendarValidationPhase;
import calendar.interview.common.enums.Login;
import calendar.interview.data.model.CalendarValidationRequest;
import calendar.interview.data.model.InterviewSchedule;
import calendar.interview.exceptions.CalendarValidationException;
import calendar.interview.services.calendarvalidation.CalendarValidationProcess;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {InterviewApplication.class})
@ContextConfiguration
class InvalidCalendarValidationScheduleInterviewProcessTests extends AbstractTestClass {

    /**
     * This Test will cover de entire Validation Process Integration
     * The predicate methods used to validate the information were been tested in testes-api-common-predicates folder
     * No connection to DB during this process
     * All this tests will except CalendarValidationException
     */

    @Autowired
    private CalendarValidationProcess calendarValidationProcess;

    @Test
    void should_ThrowCalendarValidationException_When_Send_Null_Empty() throws CalendarValidationException {

        //Cannot be Null
        Assert.assertThrows(CalendarValidationException.class, () -> calendarValidationProcess.validateCalendarProcess(
                CalendarValidationRequest
                        .builder()
                        .calendarValidationPhase(CalendarValidationPhase.SCHEDULE_INTERVIEW)
                        .interviewSchedule(null)
                        .build()
        ));


        //Cannot be Empty
        Assert.assertThrows(CalendarValidationException.class, () -> calendarValidationProcess.validateCalendarProcess(
                CalendarValidationRequest
                        .builder()
                        .calendarValidationPhase(CalendarValidationPhase.SCHEDULE_INTERVIEW)
                        .interviewSchedule(new InterviewSchedule())
                        .build()
        ));

    }


    @Test
    void should_ThrowCalendarValidationException_When_Send_Invalid_Data() throws CalendarValidationException {

        //Candidate is a Interviewer
        //CandidateListIsValidHandle
        Assert.assertThrows(CalendarValidationException.class, () -> calendarValidationProcess.validateCalendarProcess(
                CalendarValidationRequest
                        .builder()
                        .calendarValidationPhase(CalendarValidationPhase.SCHEDULE_INTERVIEW)
                        .interviewSchedule(InterviewSchedule
                                .builder()
                                .candidate(Login.HMOURA)
                                .interviewers(List.of(Login.KMOTA, Login.RSANCHES))
                                .build())
                        .build()
        ));


        //One Interviewer is a Candidate
        //InterviewListIsValidHandle
        Assert.assertThrows(CalendarValidationException.class, () -> calendarValidationProcess.validateCalendarProcess(
                CalendarValidationRequest
                        .builder()
                        .calendarValidationPhase(CalendarValidationPhase.SCHEDULE_INTERVIEW)
                        .interviewSchedule(InterviewSchedule
                                .builder()
                                .candidate(Login.NGOMES)
                                .interviewers(List.of(Login.KMOTA, Login.JPINTO))
                                .build())
                        .build()
        ));


    }

}
