package calendar.interview.services.calendarvalidationbyphase.createcalendar;

import calendar.interview.InterviewApplication;
import calendar.interview.common.enums.CalendarValidationPhase;
import calendar.interview.common.enums.Login;
import calendar.interview.data.model.CalendarValidationRequest;
import calendar.interview.data.model.CreateCalendar;
import calendar.interview.services.calendarvalidation.CalendarValidationProcess;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {InterviewApplication.class})
@ContextConfiguration
class ValidCalendarValidationCreateCalendarProcessTests extends AbstractTestClass {

    /**
     * This Test will cover de entire Validation Process Integration
     * The predicate methods used to validate the information were been tested in testes-api-common-predicates folder
     * No connection to DB during this process
     */

    @Autowired
    private CalendarValidationProcess calendarValidationProcess;


    @Test
    void should_Not_ThrowAnyException_When_Send_Valid_Data() {

        try {

            calendarValidationProcess.validateCalendarProcess(
                    CalendarValidationRequest
                            .builder()
                            .calendarValidationPhase(CalendarValidationPhase.CREATE_CALENDAR)
                            .createCalendar(CreateCalendar
                                    .builder()
                                    .login(Login.NGOMES)
                                    .startCalendarLocalDateTime(localDateTime_Last_Start_Slot_Valid)
                                    .endCalendarLocalDateTime(localDateTime_Last_End_Slot_Valid)
                                    .build())
                            .build()
            );

        } catch (Exception e) {

            //Should not throw any exception
            Assert.fail();

        }

    }

}
