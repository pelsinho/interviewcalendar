package calendar.interview.services.calendarvalidationbyphase.createcalendar;

import calendar.interview.InterviewApplication;
import calendar.interview.common.enums.CalendarValidationPhase;
import calendar.interview.common.enums.Login;
import calendar.interview.data.model.CalendarValidationRequest;
import calendar.interview.data.model.CreateCalendar;
import calendar.interview.exceptions.CalendarValidationException;
import calendar.interview.services.calendarvalidation.CalendarValidationProcess;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {InterviewApplication.class})
@ContextConfiguration
class InvalidCalendarValidationCreateCalendarProcessTests extends AbstractTestClass {

    /**
     * This Test will cover de entire Validation Process Integration
     * The predicate methods used to validate the information were been tested in testes-api-common-predicates folder
     * No connection to DB during this process
     * All this tests will throw CalendarValidationException
     */

    @Autowired
    private CalendarValidationProcess calendarValidationProcess;

    @Test
    void should_ThrowCalendarValidationException_When_Send_Null_Empty() throws CalendarValidationException {

        //Cannot be Null
        Assert.assertThrows(CalendarValidationException.class, () -> calendarValidationProcess.validateCalendarProcess(
                CalendarValidationRequest
                        .builder()
                        .calendarValidationPhase(CalendarValidationPhase.CREATE_CALENDAR)
                        .createCalendar(null)
                        .build()
        ));

        //Cannot be Empty
        Assert.assertThrows(CalendarValidationException.class, () -> calendarValidationProcess.validateCalendarProcess(
                CalendarValidationRequest
                        .builder()
                        .calendarValidationPhase(CalendarValidationPhase.CREATE_CALENDAR)
                        .createCalendar(new CreateCalendar())
                        .build()
        ));

    }


    /**
     * Validate Start Date Handles
     */

    @Test
    void should_ThrowCalendarValidationException_When_Send_Invalid_Start_Date() throws CalendarValidationException {

        //StartSlotDateIsBeforeEndSlotDateHandle
        Assert.assertThrows(CalendarValidationException.class, () -> calendarValidationProcess.validateCalendarProcess(
                CalendarValidationRequest
                        .builder()
                        .calendarValidationPhase(CalendarValidationPhase.CREATE_CALENDAR)
                        .createCalendar(CreateCalendar
                                .builder()
                                .login(Login.NGOMES)
                                .startCalendarLocalDateTime(localDateTime_Last_End_Slot_Valid)
                                .endCalendarLocalDateTime(localDateTime_Last_Start_Slot_Valid)
                                .build())
                        .build()
        ));

        //StartSlotDateDayIsValidHandle
        Assert.assertThrows(CalendarValidationException.class, () -> calendarValidationProcess.validateCalendarProcess(
                CalendarValidationRequest
                        .builder()
                        .calendarValidationPhase(CalendarValidationPhase.CREATE_CALENDAR)
                        .createCalendar(CreateCalendar
                                .builder()
                                .login(Login.NGOMES)
                                .startCalendarLocalDateTime(localDateTime_Invalid_Sunday)
                                .endCalendarLocalDateTime(localDateTime_Last_End_Slot_Valid)
                                .build())
                        .build()
        ));

        //StartSlotDateHourIsValidHandle
        Assert.assertThrows(CalendarValidationException.class, () -> calendarValidationProcess.validateCalendarProcess(
                CalendarValidationRequest
                        .builder()
                        .calendarValidationPhase(CalendarValidationPhase.CREATE_CALENDAR)
                        .createCalendar(CreateCalendar
                                .builder()
                                .login(Login.NGOMES)
                                .startCalendarLocalDateTime(localDateTime_Invalid_Hour_08)
                                .endCalendarLocalDateTime(localDateTime_Last_End_Slot_Valid)
                                .build())
                        .build()
        ));


        //StartSlotDateIsValidHandle
        Assert.assertThrows(CalendarValidationException.class, () -> calendarValidationProcess.validateCalendarProcess(
                CalendarValidationRequest
                        .builder()
                        .calendarValidationPhase(CalendarValidationPhase.CREATE_CALENDAR)
                        .createCalendar(CreateCalendar
                                .builder()
                                .login(Login.NGOMES)
                                .startCalendarLocalDateTime(localDateTime_Invalid_Start_Before_Today)
                                .endCalendarLocalDateTime(localDateTime_Last_End_Slot_Valid)
                                .build())
                        .build()
        ));


    }


    /**
     * Validate End Date Handles
     */

    @Test
    void should_ThrowCalendarValidationException_When_Send_Invalid_End_Date() throws CalendarValidationException {


        //EndSlotDateDayIsValidHandle
        Assert.assertThrows(CalendarValidationException.class, () -> calendarValidationProcess.validateCalendarProcess(
                CalendarValidationRequest
                        .builder()
                        .calendarValidationPhase(CalendarValidationPhase.CREATE_CALENDAR)
                        .createCalendar(CreateCalendar
                                .builder()
                                .login(Login.NGOMES)
                                .startCalendarLocalDateTime(localDateTime_Last_Start_Slot_Valid)
                                .endCalendarLocalDateTime(localDateTime_Invalid_Sunday)
                                .build())
                        .build()
        ));


        //EndSlotDateHourIsValidHandle
        Assert.assertThrows(CalendarValidationException.class, () -> calendarValidationProcess.validateCalendarProcess(
                CalendarValidationRequest
                        .builder()
                        .calendarValidationPhase(CalendarValidationPhase.CREATE_CALENDAR)
                        .createCalendar(CreateCalendar
                                .builder()
                                .login(Login.NGOMES)
                                .startCalendarLocalDateTime(localDateTime_Last_Start_Slot_Valid)
                                .endCalendarLocalDateTime(localDateTime_Invalid_Hour_19)
                                .build())
                        .build()
        ));



        //EndSlotDateYearIsValidHandle
        Assert.assertThrows(CalendarValidationException.class, () -> calendarValidationProcess.validateCalendarProcess(
                CalendarValidationRequest
                        .builder()
                        .calendarValidationPhase(CalendarValidationPhase.CREATE_CALENDAR)
                        .createCalendar(CreateCalendar
                                .builder()
                                .login(Login.NGOMES)
                                .startCalendarLocalDateTime(localDateTime_Last_Start_Slot_Valid)
                                .endCalendarLocalDateTime(localDateTime_Invalid_Year)
                                .build())
                        .build()
        ));




    }

}
