package calendar.interview.services.repositories;

import calendar.interview.InterviewApplication;
import calendar.interview.common.enums.Login;
import calendar.interview.data.entities.CalendarDB;
import calendar.interview.exceptions.NoRegisteredException;
import calendar.interview.repositories.CalendarRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

import java.sql.Timestamp;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {InterviewApplication.class})
@ContextConfiguration
class CalendarRepositoryServiceTests extends AbstractTestClass {

    private Example src = new Example();

    @Autowired
    CalendarRepositoryService calendarRepositoryService;

    @MockBean
    CalendarRepository calendarRepository;


    /**
     * getSlotCalendar
     */

    @Test
    void should_ThrownNullPointerException_if_Get_Slot_Parameter_With_Null_Parameter() throws NoRegisteredException {

        String login = candidate_Login.name();

        Assert.assertThrows(NullPointerException.class, ()
                -> calendarRepositoryService.getSlotCalendar(
                login, localDateTime_Last_Start_Slot_Valid_String_Format, null));

        Assert.assertThrows(NullPointerException.class, ()
                -> calendarRepositoryService.getSlotCalendar(
                login, null, localDateTime_Last_End_Slot_Valid_String_Format));

        Assert.assertThrows(NoRegisteredException.class, ()
                -> calendarRepositoryService.getSlotCalendar(
                login, localDateTime_Last_Start_Slot_Valid_String_Format, localDateTime_Last_End_Slot_Valid_String_Format));

    }

    @Test
    void should_ThrownDateTimeParseException_if_Get_Slot_Parameter_With_Empty_Parameter() throws NoRegisteredException {

        String login = candidate_Login.name();

        Assert.assertThrows(DateTimeParseException.class, () ->
                calendarRepositoryService.getSlotCalendar(
                        login, localDateTime_Last_Start_Slot_Valid_String_Format, ""));

        Assert.assertThrows(DateTimeParseException.class, () ->
                calendarRepositoryService.getSlotCalendar(
                        login, "", localDateTime_Last_End_Slot_Valid_String_Format));

        Assert.assertThrows(NoRegisteredException.class, () ->
                calendarRepositoryService.getSlotCalendar(
                        "", localDateTime_Last_Start_Slot_Valid_String_Format, localDateTime_Last_End_Slot_Valid_String_Format));

    }

    @Test
    void should_ThrownNoRegisteredException_if_there_is_no_entry_in_db() throws NoRegisteredException {

        Mockito.when(calendarRepository.getSlotDateTimeByUserAndPeriod(any(), any(), any())).thenReturn(new ArrayList<>());

        Assert.assertThrows(NoRegisteredException.class, ()
                -> calendarRepositoryService.getSlotCalendar(candidate_Login.name(), localDateTime_Last_Start_Slot_Valid_String_Format, localDateTime_Last_End_Slot_Valid_String_Format));

    }

    @Test
    void should_Get_CalendarDB_List() throws NoRegisteredException {

        Mockito.when(calendarRepository.getSlotDateTimeByUserAndPeriod(any(), any(), any())).thenReturn(
                src.getDefaultCalendarDB_1_2_3(Login.NGOMES));

        List<CalendarDB> slotCalendar = calendarRepositoryService.getSlotCalendar(
                candidate_Login.name(), localDateTime_Last_Start_Slot_Valid_String_Format, localDateTime_Last_End_Slot_Valid_String_Format);

        Assert.assertNotNull(slotCalendar);
        Assert.assertFalse(slotCalendar.isEmpty());

    }



    /**
     * removeUserCalendar
     */

    @Test
    void should_Do_Nothing_if_Send_nULL_CalendarDBList() {

        try {
            calendarRepositoryService.removeUserCalendar(null);
        } catch (Exception e) {
            Assert.fail();
        }

    }

    @Test
    void should_Do_Nothing_if_Send_Empty_CalendarDBList() {

        try {
            calendarRepositoryService.removeUserCalendar(new ArrayList<>());
        } catch (Exception e) {
            Assert.fail();
        }

    }


    @Test
    void should_Remove_UserCalendar() {

        try {
            calendarRepositoryService.removeUserCalendar(src.getDefaultCalendarDB_1_2_3(Login.NGOMES));
        } catch (Exception e) {
            Assert.fail();
        }

    }



    /**
     * getInterviewAvailableSchedule
     */
    @Test
    void should_Get_Empty_Timestamp_List_if_Send_Null_LoginList() {

        List<Timestamp> interviewAvailableSchedule = calendarRepositoryService.getInterviewAvailableSchedule(null);
        Assert.assertNotNull(interviewAvailableSchedule);
        Assert.assertTrue(interviewAvailableSchedule.isEmpty());

    }

    @Test
    void should_Get_Empty_Timestamp_List_if_Send_Empty_LoginList() {

        List<Timestamp> interviewAvailableSchedule = calendarRepositoryService.getInterviewAvailableSchedule(new ArrayList<>());
        Assert.assertNotNull(interviewAvailableSchedule);
        Assert.assertTrue(interviewAvailableSchedule.isEmpty());

    }

    @Test
    void should_Get_Valid_Timestamp_List() {

        Mockito.when(calendarRepository.getInterviewAvailableSchedule(
                src.login_Candidates_List.stream().map(Enum::name).collect(Collectors.toList()),
                (long) src.login_Candidates_List.size())).thenReturn(src.timestamp_List_1_2_3);

        List<Timestamp> interviewAvailableSchedule = calendarRepositoryService.getInterviewAvailableSchedule(src.login_Candidates_List);
        Assert.assertNotNull(interviewAvailableSchedule);
        Assert.assertEquals(src.timestamp_List_1_2_3.size(), interviewAvailableSchedule.size());

    }






}
