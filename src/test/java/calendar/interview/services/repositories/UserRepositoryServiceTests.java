package calendar.interview.services.repositories;

import calendar.interview.InterviewApplication;
import calendar.interview.common.enums.Login;
import calendar.interview.common.predicates.GenericPredicates;
import calendar.interview.data.entities.UserDB;
import calendar.interview.data.model.CreateCalendar;
import calendar.interview.exceptions.NoRegisteredException;
import calendar.interview.exceptions.UnableToSaveUserException;
import calendar.interview.repositories.UserRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.doNothing;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {InterviewApplication.class})
@ContextConfiguration
class UserRepositoryServiceTests extends AbstractTestClass {

    private Example src = new Example();

    @Autowired
    UserRepositoryService userRepositoryService;

    @MockBean
    UserRepository userRepository;

    /**
     * getUserSlotCalendar
     */

    @Test
    void should_ThrownNoRegisteredException_if_there_is_no_entry_in_db() throws NoRegisteredException {

        Assert.assertThrows(NoRegisteredException.class, () -> userRepositoryService.getUserSlotCalendar(""));
        Assert.assertThrows(NoRegisteredException.class, () -> userRepositoryService.getUserSlotCalendar("INVALID_USER"));
        Assert.assertThrows(NoRegisteredException.class, () -> userRepositoryService.getUserSlotCalendar(null));

    }

    @Test
    void should_Get_User_Db() throws NoRegisteredException {

        Mockito.when(userRepository.findByLogin(candidate_Login.name())).thenReturn(Optional.of(src.getUserDBWithDefaultCalendarDB_1_2_3(candidate_Login)));

        UserDB userSlotCalendar = userRepositoryService.getUserSlotCalendar(candidate_Login.name());

        Assert.assertNotNull(userSlotCalendar);
        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(userSlotCalendar.getLogin()));
        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(userSlotCalendar.getId()));

    }


    /**
     * removeUser
     */

    @Test
    void should_do_nothing_if_Remove_User_Send_Null_or_Empty_Parameter() {

        try {

            userRepositoryService.removeUser(null);
            userRepositoryService.removeUser(new UserDB());

        } catch (Exception e) {

            Assert.fail();

        }

    }

    @Test
    void should_Remove_User() {

        try {

            UserDB userDB = src.getUserDBWithDefaultCalendarDB_1_2_3(Login.NGOMES);
            doNothing().when(userRepository).delete(userDB);
            userRepositoryService.removeUser(new UserDB());

        } catch (Exception e) {

            Assert.fail();

        }

    }

    /**
     * saveUserCalendar
     */

    @Test
    void should_ThrownNoRegisteredException_if_Save_Send_Null_or_Empty_Parameter() throws UnableToSaveUserException {

        Assert.assertThrows(UnableToSaveUserException.class, () -> userRepositoryService.saveUserCalendar(null));
        Assert.assertThrows(UnableToSaveUserException.class, () -> userRepositoryService.saveUserCalendar(new UserDB()));

    }

    @Test
    void should_Save_User() throws UnableToSaveUserException {

        UserDB userDB = src.getUserDBWithDefaultCalendarDB_1_2_3(Login.NGOMES);

        Mockito.when(userRepository.save(userDB)).thenReturn(userDB);

        UUID userId = userRepositoryService.saveUserCalendar(userDB);

        Assert.assertEquals(userDB.getId(), userId);

    }


    /**
     * buildUserDBWithCalendar
     */

    @Test
    void should_ThrowNullPointerException_if_BuildUserWithCalendar_Send_Null() {

        Assert.assertThrows(NullPointerException.class, () -> userRepositoryService.buildUserDBWithCalendar(null));

    }

    @Test
    void should_ThrowNullPointerException_if_BuildUserWithCalendar_Send_Empty() {

        CreateCalendar createCalendar = new CreateCalendar();
        Assert.assertThrows(NullPointerException.class, () -> userRepositoryService.buildUserDBWithCalendar(createCalendar));

    }

    @Test
    void should_buildUserDBWithCalendar() {

        CreateCalendar createCalendar = src.getCreateCalendar(candidate_Login);

        UserDB userDB = userRepositoryService.buildUserDBWithCalendar(createCalendar);

        Assert.assertNotNull(userDB);
        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(userDB.getLogin()));
        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(userDB.getRoles()));
        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(userDB.getCalendarList()));

    }

}
