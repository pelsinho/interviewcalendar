package calendar.interview.services;

import calendar.interview.InterviewApplication;
import calendar.interview.common.predicates.GenericPredicates;
import calendar.interview.data.dto.UserReportDTO;
import calendar.interview.exceptions.NoRegisteredException;
import calendar.interview.services.repositories.CalendarRepositoryService;
import calendar.interview.services.repositories.UserRepositoryService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {InterviewApplication.class})
@ContextConfiguration
class GetCalendarServiceTests extends AbstractTestClass {

    private Example src = new Example();

    @Autowired
    GetCalendarService getCalendarService;

    @MockBean
    UserRepositoryService userRepositoryService;

    @MockBean
    CalendarRepositoryService calendarRepositoryService;



    /**
     * Get getUserCalendar - String login
     */
    @Test
    void should_ThrownNoRegisteredException_if_Get_User_Calendar_With_Login_Null_Parameter() throws NoRegisteredException {

        Mockito.when(userRepositoryService.getUserSlotCalendar(null)).thenThrow(new NoRegisteredException(""));
        Mockito.when(userRepositoryService.getUserSlotCalendar("")).thenThrow(new NoRegisteredException(""));

        Assert.assertThrows(NoRegisteredException.class, () -> getCalendarService.getUserCalendar(null));
        Assert.assertThrows(NoRegisteredException.class, () -> getCalendarService.getUserCalendar(""));

    }


    @Test
    void should_Get_UserReportDTO_By_Login() throws NoRegisteredException {

        Mockito.when(userRepositoryService.getUserSlotCalendar(candidate_Login.name())).thenReturn(src.getUserDBWithDefaultCalendarDB_1_2_3(candidate_Login));

        UserReportDTO userReportDTO = getCalendarService.getUserCalendar(candidate_Login.name());

        Assert.assertNotNull(userReportDTO);
        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(userReportDTO.getCalendarList()));
        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(userReportDTO.getLogin()));
        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(userReportDTO.getRoles()));


    }



    /**
     * Get getUserCalendar - String login, String startLocalDateTime, String endLocalDateTime
     */

    @Test
    void should_ThrownNoRegisteredException_if_Get_User_Calendar_With_Null_Empty_Parameter() throws NoRegisteredException {

        Mockito.when(calendarRepositoryService.getSlotCalendar(null, localDateTime_Last_Start_Slot_Valid_String_Format, localDateTime_Last_End_Slot_Valid_String_Format)).thenThrow(new NoRegisteredException(""));
        Mockito.when(calendarRepositoryService.getSlotCalendar("", localDateTime_Last_Start_Slot_Valid_String_Format, localDateTime_Last_End_Slot_Valid_String_Format)).thenThrow(new NoRegisteredException(""));
        Mockito.when(calendarRepositoryService.getSlotCalendar(candidate_Login.name(), localDateTime_Last_Start_Slot_Valid_String_Format, null)).thenThrow(new NoRegisteredException(""));
        Mockito.when(calendarRepositoryService.getSlotCalendar(candidate_Login.name(), localDateTime_Last_Start_Slot_Valid_String_Format, "")).thenThrow(new NoRegisteredException(""));
        Mockito.when(calendarRepositoryService.getSlotCalendar(candidate_Login.name(), null, localDateTime_Last_End_Slot_Valid_String_Format)).thenThrow(new NoRegisteredException(""));
        Mockito.when(calendarRepositoryService.getSlotCalendar(candidate_Login.name(), "", localDateTime_Last_End_Slot_Valid_String_Format)).thenThrow(new NoRegisteredException(""));

        Assert.assertThrows(NoRegisteredException.class, () -> getCalendarService.getUserCalendar(null, localDateTime_Last_Start_Slot_Valid_String_Format, localDateTime_Last_End_Slot_Valid_String_Format));
        Assert.assertThrows(NoRegisteredException.class, () -> getCalendarService.getUserCalendar("", localDateTime_Last_Start_Slot_Valid_String_Format, localDateTime_Last_End_Slot_Valid_String_Format));
        Assert.assertThrows(NoRegisteredException.class, () -> getCalendarService.getUserCalendar(candidate_Login.name(), localDateTime_Last_Start_Slot_Valid_String_Format, null));
        Assert.assertThrows(NoRegisteredException.class, () -> getCalendarService.getUserCalendar(candidate_Login.name(), localDateTime_Last_Start_Slot_Valid_String_Format, ""));
        Assert.assertThrows(NoRegisteredException.class, () -> getCalendarService.getUserCalendar(candidate_Login.name(), null, localDateTime_Last_End_Slot_Valid_String_Format));
        Assert.assertThrows(NoRegisteredException.class, () -> getCalendarService.getUserCalendar(candidate_Login.name(), "", localDateTime_Last_End_Slot_Valid_String_Format));

    }

    @Test
    void should_Get_UserReportDTO_By_Login_Start_End_Date() throws NoRegisteredException {

        Mockito.when(calendarRepositoryService.getSlotCalendar(candidate_Login.name(), localDateTime_Last_Start_Slot_Valid_String_Format, localDateTime_Last_End_Slot_Valid_String_Format))
                .thenReturn(src.getDefaultCalendarDB_1_2_3(candidate_Login));

        UserReportDTO userReportDTO = getCalendarService.getUserCalendar(candidate_Login.name(), localDateTime_Last_Start_Slot_Valid_String_Format, localDateTime_Last_End_Slot_Valid_String_Format);

        Assert.assertNotNull(userReportDTO);
        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(userReportDTO.getCalendarList()));
        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(userReportDTO.getLogin()));
        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(userReportDTO.getRoles()));


    }

}
