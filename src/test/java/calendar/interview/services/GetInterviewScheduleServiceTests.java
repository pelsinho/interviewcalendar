package calendar.interview.services;

import calendar.interview.InterviewApplication;
import calendar.interview.common.enums.Login;
import calendar.interview.common.predicates.GenericPredicates;
import calendar.interview.data.dto.InterviewAvailableScheduleReportDTO;
import calendar.interview.exceptions.CalendarValidationException;
import calendar.interview.exceptions.UnableToSaveUserException;
import calendar.interview.services.calendarvalidation.CalendarValidationProcess;
import calendar.interview.services.repositories.CalendarRepositoryService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {InterviewApplication.class})
@ContextConfiguration
class GetInterviewScheduleServiceTests extends AbstractTestClass {

    private Example src = new Example();

    @Autowired
    GetInterviewScheduleService getInterviewScheduleService;

    @MockBean
    CalendarValidationProcess calendarValidationProcess;

    @MockBean
    CalendarRepositoryService calendarRepositoryService;


    @Test
    void should_ThrownRuntimeException_if_Get_User_Calendar_With_Login_Null_Parameter() throws CalendarValidationException, UnableToSaveUserException {

        when(calendarRepositoryService.getInterviewAvailableSchedule(null)).thenThrow(new RuntimeException(""));
        Assert.assertThrows(RuntimeException.class, () -> getInterviewScheduleService.getInterviewAvailableSchedule(null, null));

    }


    @Test
    void should_CreateCalendarSlot() throws CalendarValidationException {

        doNothing().when(calendarValidationProcess).validateCalendarProcess(any());

        List<Login> loginList = new ArrayList();
        loginList.addAll(src.login_Candidates_List);
        loginList.add(candidate_Login);

        when(calendarRepositoryService.getInterviewAvailableSchedule(any())).thenReturn(src.timestamp_List_1_2_3);

        InterviewAvailableScheduleReportDTO interviewAvailableSchedule
                = getInterviewScheduleService.getInterviewAvailableSchedule(src.login_Candidates_List, candidate_Login);

        Assert.assertNotNull(interviewAvailableSchedule);
        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(interviewAvailableSchedule.getCandidate()));
        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(interviewAvailableSchedule.getInterviewers()));
        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(interviewAvailableSchedule.getInterviewAvailableSchedules()));

    }


}
