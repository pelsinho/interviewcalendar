package calendar.interview.services;

import calendar.interview.InterviewApplication;
import calendar.interview.data.dto.CreateCalendarDTO;
import calendar.interview.data.entities.UserDB;
import calendar.interview.data.model.CreateCalendar;
import calendar.interview.exceptions.CalendarValidationException;
import calendar.interview.exceptions.UnableToSaveUserException;
import calendar.interview.services.calendarvalidation.CalendarValidationProcess;
import calendar.interview.services.repositories.UserRepositoryService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {InterviewApplication.class})
@ContextConfiguration
class CreateCalendarServiceTests extends AbstractTestClass {

    private Example src = new Example();

    @Autowired
    CreateCalendarService createCalendarService;

    @MockBean
    CalendarValidationProcess calendarValidationProcess;

    @MockBean
    UserRepositoryService userRepositoryService;

    @Test
    void should_Do_Nothing_if_Get_User_Calendar_With_Login_Null_Parameter() throws CalendarValidationException, UnableToSaveUserException {

        UUID calendarSlot = createCalendarService.createCalendarSlot(null);
        Assert.assertNull(calendarSlot);

    }

    @Test
    void should_ThrownRuntimeException_if_Get_User_Calendar_With_Login_Empty_Parameter() throws CalendarValidationException, UnableToSaveUserException {

        when(userRepositoryService.buildUserDBWithCalendar(new CreateCalendar())).thenThrow(new RuntimeException(""));

        CreateCalendarDTO createCalendarDTO = new CreateCalendarDTO();
        Assert.assertThrows(RuntimeException.class, () -> createCalendarService.createCalendarSlot(createCalendarDTO));

    }

    @Test
    void should_CreateCalendarSlot() throws CalendarValidationException, UnableToSaveUserException {

        doNothing().when(calendarValidationProcess).validateCalendarProcess(any());

        CreateCalendarDTO createCalendarDTO = src.getCreateCalendarDTO(candidate_Login);
        CreateCalendar createCalendar = src.getCreateCalendar(candidate_Login);

        UserDB userDB = src.getUserDBWithDefaultCalendarDB_1_2_3(candidate_Login);

        when(userRepositoryService.buildUserDBWithCalendar(createCalendar)).thenReturn(userDB);
        when(userRepositoryService.saveUserCalendar(userDB)).thenReturn(userDB.getId());

        UUID userId = createCalendarService.createCalendarSlot(createCalendarDTO);

        Assert.assertEquals(userId, userDB.getId());

    }


}
