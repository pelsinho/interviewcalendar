package environmenttests.actions;

import calendar.interview.data.dto.UserReportDTO;
import environmenttests.connectors.EnvironmentConnector;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import testsupport.conectors.ApacheHttpConnector;

import java.io.IOException;

import static testsupport.examples.AbstractTestClass.EXPECT_ERROR_STATUS_LIST;

@Slf4j
public class EnvironmentHttpActions {

    public static UserReportDTO getEnvEurekaByLogin(
            String login,
            Integer expectedStatus) throws IOException {

        HttpResponse httpResponse = EnvironmentConnector.getEnvEurekaByLogin(login);

        ApacheHttpConnector.validateStatus(expectedStatus, httpResponse);

        if (EXPECT_ERROR_STATUS_LIST.contains(expectedStatus)) return new UserReportDTO();

        return ApacheHttpConnector.readResponse(httpResponse, UserReportDTO.class);

    }


    public static String getEnvResilience4J(
            Integer expectedStatus) throws IOException {

        HttpResponse httpResponse = EnvironmentConnector.getEnvResilience4J();

        ApacheHttpConnector.validateStatus(expectedStatus, httpResponse);

        if (EXPECT_ERROR_STATUS_LIST.contains(expectedStatus)) return "Error in Resilience Process";

        return ApacheHttpConnector.readResponse(httpResponse);

    }


    public static String getEnvResilienceThread4J(
            Integer expectedStatus) throws IOException {

        HttpResponse httpResponse = EnvironmentConnector.getEnvResilience4J();

        ApacheHttpConnector.validateStatus(expectedStatus, httpResponse);

        if (EXPECT_ERROR_STATUS_LIST.contains(expectedStatus)) return "Error in Resilience Thread Process";

        return ApacheHttpConnector.readResponse(httpResponse);

    }



}
