package environmenttests.connectors;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum EnvironmentEndpoint {

    MAIN_ENV_URL("http://localhost:8080"),

    ENV_CALENDAR(MAIN_ENV_URL.getEndpoint() + "/env"),

    ENV_EUREKA_GET_BY_LOGIN(ENV_CALENDAR.getEndpoint() + "/eureka/byLogin/%s"),

    ENV_RESILIENCE_4J_GET(ENV_CALENDAR.getEndpoint() + "/resilience4j"),

    ENV_RESILIENCE_4J_THREAD_GET(ENV_CALENDAR.getEndpoint() + "/resilienceThread4j");

    private final String endpoint;

}
