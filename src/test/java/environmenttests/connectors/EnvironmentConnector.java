package environmenttests.connectors;

import org.apache.http.HttpResponse;
import testsupport.conectors.ApacheHttpConnector;

import java.io.IOException;

import static environmenttests.connectors.EnvironmentEndpoint.ENV_EUREKA_GET_BY_LOGIN;
import static environmenttests.connectors.EnvironmentEndpoint.ENV_RESILIENCE_4J_GET;

public class EnvironmentConnector {


    public static HttpResponse getEnvEurekaByLogin(String login) throws IOException {

        String url = String.format(ENV_EUREKA_GET_BY_LOGIN.getEndpoint(), login);
        return new ApacheHttpConnector().httpGet(url);

    }


    public static HttpResponse getEnvResilience4J() throws IOException {

        return new ApacheHttpConnector().httpGet(ENV_RESILIENCE_4J_GET.getEndpoint());

    }

    public static HttpResponse getEnvResilienceThread4J() throws IOException {

        return new ApacheHttpConnector().httpGet(ENV_RESILIENCE_4J_GET.getEndpoint());

    }


}
