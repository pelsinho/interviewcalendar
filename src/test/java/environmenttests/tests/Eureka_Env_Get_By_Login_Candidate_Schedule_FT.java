package environmenttests.tests;

import calendar.interview.common.enums.Login;
import calendar.interview.data.dto.UserReportDTO;
import environmenttests.actions.EnvironmentHttpActions;
import functionaltests.actions.InterviewHttpActions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;
import testsupport.validation.ValidateObject;

import java.io.IOException;

import static testsupport.examples.AbstractTestClass.TAG_ENVIRONMENT;

/**
 * Only Runs on Spring Profile set to Local
 * Test Eureka Discovery Process
 * Will be removed when Reference Data Api is deployed
 */
@Tag(TAG_ENVIRONMENT)
@ExtendWith(SpringExtension.class)
public class Eureka_Env_Get_By_Login_Candidate_Schedule_FT extends AbstractTestClass {

    private Example src = new Example();

    /**
     * Clean DB
     */
    @BeforeEach
    public void init() {
        InterviewHttpActions.deleteAllByLogins();
    }

    /**
     * This is going to test get by Login using Eureka Discovery Server
     */
    @Test
    public void environment_Get_By_Login_Candidate_Schedule_Using_Eureka_FT() throws IOException {

        /**
         * Get Random Login
         */

        Login login = src.getRandomLogin();


        /**
         * So Let´s create a user with one full day schedule
         */

        InterviewHttpActions.createCalendarSlot(
                src.getCreateCalendarDTO(
                        src.getLocalDateTime_String_Format("31", "12", "09"),
                        src.getLocalDateTime_String_Format("31", "12", "18"),
                        login),
                200);


        /**
         * Now Lets get By Login
         */

        UserReportDTO userReportDTO = EnvironmentHttpActions.getEnvEurekaByLogin(login.name(), 200);
        ValidateObject.validate_UserReportDTO(userReportDTO, login, 9);

    }

}
