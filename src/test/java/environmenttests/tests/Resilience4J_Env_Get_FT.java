package environmenttests.tests;

import calendar.interview.common.predicates.GenericPredicates;
import environmenttests.actions.EnvironmentHttpActions;
import functionaltests.actions.InterviewHttpActions;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractTestClass;

import java.io.IOException;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.fail;
import static testsupport.examples.AbstractTestClass.TAG_ENVIRONMENT;
import static testsupport.examples.AbstractTestClass.TAG_FUNCTIONAL;

/**
 * Only Runs on Spring Profile set to Local
 * Test Eureka Discovery Process
 * Will be removed when Reference Data Api is deployed
 */
@Slf4j
@Tag(TAG_ENVIRONMENT)
@ExtendWith(SpringExtension.class)
public class Resilience4J_Env_Get_FT extends AbstractTestClass {

    /**
     * This is going to test Resilience4J FallBack Process
     */

    @Test
    public void resilience4J_Env_Get_And_Not_Trigger_FallBack_Method_FT() throws IOException {

        String resilience4J1 = EnvironmentHttpActions.getEnvResilience4J(200);

        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(resilience4J1));

    }


    @Test
    public void resilience4J_Env_Get_And_Trigger_FallBack_Method_FT() throws IOException {

        final boolean[] test = new boolean[1];

        IntStream.range(1, 20).parallel().forEach(x -> {
            try {

                String resilience4J = EnvironmentHttpActions.getEnvResilience4J(200);

                if (!resilience4J.contains("NOT")) {
                    log.info("Resilience4j FallBack Method has been trigger");
                    test[0] = true;
                }

            } catch (Exception e) {
                throw new RuntimeException("Resilience4j FallBack Method should be trigger");
            }
        });

        if (!test[0]) {
            log.info("Resilience4j FallBack Method should be trigger");
            fail();
        }

    }


    /**
     * This is going to test Resilience4J Thread FallBack Process
     */


    @Test
    public void resilience4J_Thread_Env_Get_And_Not_Trigger_FallBack_Method_FT() throws IOException {

        String resilience4J1 = EnvironmentHttpActions.getEnvResilienceThread4J(200);

        Assert.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(resilience4J1));

    }


    @Test
    public void resilience4J_Thread_Env_Get_And_Trigger_FallBack_Method_FT() throws IOException {

        final boolean[] test = new boolean[1];

        IntStream.range(1, 20).parallel().forEach(x -> {
            try {

                String resilience4J = EnvironmentHttpActions.getEnvResilienceThread4J(200);

                if (!resilience4J.contains("NOT")) {
                    log.info("Resilience4j Thread FallBack Method has been trigger");
                    test[0] = true;
                }

            } catch (Exception e) {
                throw new RuntimeException("Resilience4j Thread FallBack Method should be trigger");
            }
        });

        if (!test[0]) {
            log.info("Resilience4j Thread FallBack Method should be trigger");
            fail();
        }

    }


}
