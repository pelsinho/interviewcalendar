CREATE TABLE IF NOT EXISTS app_user
(

    id          uuid primary key,
    roles       varchar(255),
    login       varchar(255),

    created_at  timestamp with time zone default (now() AT TIME ZONE 'UTC'),
    updated_at  timestamp with time zone default (now() AT TIME ZONE 'UTC'),
    created_by  varchar,
    updated_by  varchar

);

