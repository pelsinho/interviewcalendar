CREATE TABLE calendar
(

    id                  uuid primary key,
    slot_Date_Time      timestamp with time zone default (now() AT TIME ZONE 'UTC'),

    app_user_id         uuid references app_user(id),

    created_at          timestamp with time zone default (now() AT TIME ZONE 'UTC'),
    updated_at          timestamp with time zone default (now() AT TIME ZONE 'UTC'),
    created_by          varchar,
    updated_by          varchar

);