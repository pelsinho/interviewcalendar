package calendar.interview.controllers.local;

import calendar.interview.client.interfaces.IReferenceDataClient;
import calendar.interview.common.enums.Login;
import calendar.interview.common.predicates.GenericPredicates;
import calendar.interview.common.validations.BindingResultValidation;
import calendar.interview.data.dto.CreateCalendarDTO;
import calendar.interview.data.dto.ResponseDTO;
import calendar.interview.data.dto.UserReportDTO;
import calendar.interview.exceptions.CalendarValidationException;
import calendar.interview.exceptions.NoRegisteredException;
import calendar.interview.exceptions.UnableToSaveUserException;
import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static calendar.interview.common.constants.CalendarInformation.SLOT_DATE_TIME_FORMAT;
import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@Slf4j
@Profile("local")
@RequiredArgsConstructor
@RequestMapping("/env")
@Api("Handles Environment Test Process, Like (Eureka Discovery Process), Available only when uses Spring Local Profile")
public class EnvLocalController {

    private final IReferenceDataClient ireferenceDataClient;


    /**
     * GET Using Resilience 4J BulkHead Pattern
     */
    @Bulkhead(name = "resilientBulkHead", fallbackMethod = "resilience4j")
    @GetMapping("/resilience4j")
    public ResponseEntity<String> testResilience4j() throws Exception {

        Thread.sleep(10000);
        return ok().body("Resilience4j FallBack Method has NOT been trigger");

    }

    public ResponseEntity<String> resilience4j(Exception t) throws Exception {
        log.info("Resilience4j FallBack Method has been trigger");
        return ok().body("Resilience4j FallBack Method has been trigger");
    }


    /**
     * GET Using Resilience 4J Thread BulkHead Pattern
     */
    @Bulkhead(name = "resilientThreadBulkHead", fallbackMethod = "resilienceThread4j")
    @GetMapping("/resilienceThread4j")
    public ResponseEntity<String> testResilienceThread4j() throws Exception {

        Thread.sleep(10000);
        return ok().body("Resilience4j Thread FallBack Method has NOT been trigger");

    }

    public ResponseEntity<String> resilienceThread4j(Exception t) throws Exception {
        log.info("Resilience4j Thread FallBack Method has been trigger");
        return ok().body("Resilience4j Thread FallBack Method has been trigger");
    }





    /**
     * GET BY LOGIN Using Eureka
     */

    @GetMapping("/eureka/byLogin/{login}")
    @ApiOperation(value = "Get All Calendar Slots by Login using Eureka Server Discovery Process")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 401, message = "Unauthorised"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<UserReportDTO> getUserCalendarByLogin(
            @ApiParam(required = true, example = "NGOMES") @PathVariable String login) {

        return ok().body(ireferenceDataClient.getScheduleCalendarByLoginByEureka(login));

    }

}
