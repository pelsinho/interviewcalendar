package calendar.interview.controllers;

import calendar.interview.client.interfaces.IReferenceDataClient;
import calendar.interview.common.enums.Login;
import calendar.interview.common.log.DefaultLog;
import calendar.interview.common.predicates.GenericPredicates;
import calendar.interview.common.validations.BindingResultValidation;
import calendar.interview.data.dto.CreateCalendarDTO;
import calendar.interview.data.dto.InterviewAvailableScheduleReportDTO;
import calendar.interview.data.dto.ResponseDTO;
import calendar.interview.data.dto.UserReportDTO;
import calendar.interview.exceptions.*;
import calendar.interview.services.interfaces.ICreateCalendarService;
import calendar.interview.services.interfaces.IGetCalendarService;
import calendar.interview.services.interfaces.IGetInterviewSchedule;
import calendar.interview.services.interfaces.IRemoveCalendarService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static calendar.interview.common.constants.CalendarInformation.SLOT_DATE_TIME_FORMAT;
import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/calendar")
@Api("Handles Interview Calendar Schedule Process")
public class CalendarController {

    private final ICreateCalendarService createCalendarService;
    private final IGetCalendarService getCalendarService;
    private final IGetInterviewSchedule getInterviewArrangements;
    private final IRemoveCalendarService removeCalendarService;

    /**
     * POST / CREATE
     */
    @PostMapping()
    @ApiOperation(value = "Creates a new Calendar Slot",
            notes =
                     "<p> - Please if you don´t have a login please contact our support</p>" +
                     "<p> - Please use this format to send calendar information : " + SLOT_DATE_TIME_FORMAT + "</p>" +
                     "<p> - We can only offer interviews from Monday to Friday during working hours - 9am to 5pm</p>" +
                     "<p> Important - For a better experience. Read API Instructions at th top of Swagger UI Home Page </p>")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 401, message = "Unauthorised"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object> createCalendarSlot(
            @ApiParam(required = true) @Valid @RequestBody CreateCalendarDTO createCalendarDTO, BindingResult bindingResult) throws CalendarValidationException, UnableToSaveUserException {

        //Validate DTO Spring Annotation Errors
        ResponseEntity<ResponseDTO> responseResponseEntity = BindingResultValidation.validateBindingResult(bindingResult);
        if (GenericPredicates.checkIfNullOrEmpty.negate().test(responseResponseEntity)) return badRequest().body(responseResponseEntity);

        return ok().body(createCalendarService.createCalendarSlot(createCalendarDTO));

    }

    /**
     * GET AVAILABLE SCHEDULE INTERVIEW
     */

    @GetMapping("/interviewers/{interviewers}/candidate/{candidate}")
    @ApiOperation(value = "Get possible interview schedules.",
            notes =
                            "<p> Get a collection of periods of time when it’s possible arrange an interview for a particular candidate and one or more interviewers</p>" +
                            "<p> - All Fields Are Mandatory</p>" +
                            "<p> - We are going to taking in consideration only schedules slots that the candidate and all interviewers are available</p>" +
                            "<p> Example : Candidate 1 - 09 to 18 , Interview1 - 09 to 13 , Interview2 - 12 to 18. Available Schedule Response will be 12 to 13</p> " +
                            "<p> Important - For a better experience. Read API Instructions at th top of Swagger UI Home Page </p>")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 401, message = "Unauthorised"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<InterviewAvailableScheduleReportDTO> getInterviewArrangements(
            @ApiParam(example = "HMOURA", defaultValue = "HMOURA") @PathVariable List<Login> interviewers,
            @ApiParam(example = "NGOMES", defaultValue = "NGOMES") @PathVariable Login candidate) throws CalendarValidationException {

        log.info("Validate Spring Sleuth process - Should be something like : [interviewcalendar,83f1fbf7ae98b0e3,83f1fbf7ae98b0e3]");
        return ok().body(getInterviewArrangements.getInterviewAvailableSchedule(interviewers, candidate));

    }


    /**
     * GET BY LOGIN
     */

    @GetMapping("/byLogin/{login}")
    @ApiOperation(value = "Get All Calendar Slots by Login",
            notes =
            "<p> Important - For a better experience. Read API Instructions at th top of Swagger UI Home Page </p>")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 401, message = "Unauthorised"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<UserReportDTO> getUserCalendarByLogin(
            @ApiParam(required = true, example = "NGOMES") @PathVariable String login) throws NoRegisteredException {

        return ok().body(getCalendarService.getUserCalendar(login));

    }


    /**
     * GET BY LOGIN, START, END LOCAL DATE AND TIME
     */
    @GetMapping("/byLogin/{login}/start/{startLocalDateTime}/end/{endLocalDateTime}")
    @ApiOperation(value = "Get Calendar Slots by Login and Period",
            notes =
                            "<p> Please use this format to send calendar information : " + SLOT_DATE_TIME_FORMAT + "</p>" +
                            "<p> Important - For a better experience. Read API Instructions at th top of Swagger UI Home Page </p>")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 401, message = "Unauthorised"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<UserReportDTO> getUserCalendarByLoginAndPeriod(
            @ApiParam(required = true, example = "NGOMES") @PathVariable String login,
            @ApiParam(required = true, example = "10/09/2021 14", defaultValue = "01/01/2000 17") @PathVariable String startLocalDateTime,
            @ApiParam(required = true, example = "15/09/2021 14", defaultValue = "01/01/2999 17") @PathVariable String endLocalDateTime) throws NoRegisteredException {

        return ok().body(getCalendarService.getUserCalendar(login, startLocalDateTime, endLocalDateTime));

    }





    /**
     * REMOVE ALL ENTRIES BY LOGIN
     */
    @DeleteMapping("/byLogin/{login}")
    @ApiOperation(value = "Remove All Calendar by Login",
            notes =
                            "<p> Important - For a better experience. Read API Instructions at th top of Swagger UI Home Page </p>")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 401, message = "Unauthorised"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object> removeUserCalendar(
            @ApiParam(required = true, example = "NGOMES") @PathVariable String login) throws NoRegisteredException {

        removeCalendarService.removeUserCalendar(login);
        return ok().build();

    }

    /**
     * REMOVE BY LOGIN, START, END LOCAL DATE AND TIME
     * Implementing Resilience 4J BulkHead Pattern
     */
    @DeleteMapping("/byLogin/{login}/start/{startLocalDateTime}/end/{endLocalDateTime}")
    @ApiOperation(value = "Remove Calendar Slots by Login and Period",
            notes =
                            "<p> Please use this format to send calendar information : " + SLOT_DATE_TIME_FORMAT + "</p>" +
                            "<p> Important - For a better experience. Read API Instructions at th top of Swagger UI Home Page </p>")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 401, message = "Unauthorised"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object> removeUserCalendarByLoginAndPeriod(
            @ApiParam(required = true, example = "NGOMES") @PathVariable String login,
            @ApiParam(required = true, example = "15/09/2021 09") @PathVariable String startLocalDateTime,
            @ApiParam(required = true, example = "15/09/2021 14") @PathVariable String endLocalDateTime) throws NoRegisteredException {

        removeCalendarService.removeUserCalendar(login, startLocalDateTime, endLocalDateTime);
        return ok().build();

    }


}
