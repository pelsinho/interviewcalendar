package calendar.interview.common.predicates;

import calendar.interview.common.enums.Login;
import calendar.interview.common.enums.Roles;

import java.util.List;
import java.util.function.BiPredicate;

public class UserPredicates {

    private UserPredicates() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    /**
     * Validates if all List item have the same Role.
     */
    public static BiPredicate<List<Login>, Roles> allHaveThisRole = (loginList, roles) ->
            loginList.stream().allMatch(x -> x.getRole().equals(roles));

}
