package calendar.interview.common.predicates;

import calendar.interview.data.entities.CalendarDB;

import javax.validation.constraints.NotNull;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

import static calendar.interview.common.constants.CalendarInformation.*;

public class CalendarPredicates {

    private CalendarPredicates() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    /**
     * Validate if its a Weekend Day
     */
    public static Predicate<@NotNull DayOfWeek> isAWeekendDay = dayOfWeek -> dayOfWeek.equals(DayOfWeek.SATURDAY) || dayOfWeek.equals(DayOfWeek.SUNDAY);

    /**
     * Validate if its a Valid Start Hour (Based on Application Assumptions)
     */
    public static Predicate<LocalDateTime> isValidSlotStartHour = startLocalDateTime ->
            startLocalDateTime.getHour() >= START_SLOT_LOWER_HOUR && startLocalDateTime.getHour() <= START_SLOT_HIGHER_HOUR;

    /**
     * Validate if its a Valid Schedule Date and Time (Based on Application Assumptions)
     */
    public static Predicate<LocalDateTime> isValidSlotLocalDateTime = localDateTime -> {

        boolean weekendDay = isAWeekendDay.test(localDateTime.getDayOfWeek());
        boolean validSlotHour = isValidSlotStartHour.test(localDateTime);
        return !weekendDay && validSlotHour;

    };

    /**
     * Validate if its a Valid End Hour (Based on Application Assumptions)
     */
    public static Predicate<LocalDateTime> isValidSlotEndHour = endLocalDateTime ->
            endLocalDateTime.getHour() >= END_SLOT_LOWER_HOUR && endLocalDateTime.getHour() <= END_SLOT_HIGHER_HOUR;

    /**
     * Validate if its a Valid End Year (Based on Application Assumptions)
     */
    public static Predicate<LocalDateTime> isValidSlotEndYear = endLocalDateTime -> endLocalDateTime.getYear() <= END_SLOT_YEAR;

    /**
     * Validate if if Start Schedule Date and Time is before End Schedule Date and Time
     */
    public static BiPredicate<LocalDateTime, LocalDateTime> isStartSlotDateBeforeEndSlotDate = LocalDateTime::isBefore;

    /**
     * Validate if if Start Schedule Date and Time is After Today
     */
    public static Predicate<LocalDateTime> isStartSlotDateAfterToday = startLocalDateTime -> startLocalDateTime.isAfter(LocalDateTime.now());


    /**
     * Avoid to add duplicate calendar entries
     * If there is a performance issue in this point
     * We could try a difference approach to validate if a slot its already allocated to the calendar
     */
    public static BiPredicate<LocalDateTime, List<CalendarDB>> slotAlreadyAllocated = (localDateTime, calendarDBList) -> {

        if (GenericPredicates.checkIfNullOrEmpty.test(calendarDBList)) return false;
        return calendarDBList.parallelStream().anyMatch(x -> x.getSlotDateTime().toLocalDateTime().equals(localDateTime));

    };


}
