package calendar.interview.common.constants;

import java.time.LocalDate;

import static calendar.interview.common.constants.CalendarInformation.*;

public class CalendarValidationMessages {

    private CalendarValidationMessages() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    public static final String ERROR_INVALID_START_DATE_WEEKEND_DAY_DETECTED =
            "Invalid - Start Date Day (Weekend Days Not Allowed) : %s.";

    public static final String ERROR_INVALID_END_DATE_WEEKEND_DAY_DETECTED =
            "Invalid - End Date Day (Weekend Days Not Allowed) : %s.";

    public static final String ERROR_INVALID_START_DATE_AFTER_END_DATE =
            "Invalid - Start Date : %s is after End Date : %s.";

    public static final String ERROR_INVALID_START_DATE_MUST_BE_IN_THE_FUTURE =
            "Invalid - Start Date : %s is before Today Date : " + LocalDate.now();

    public static final String ERROR_INVALID_START_DATE_HOUR =
            "Invalid - Start Date Hour : %s. Should be between : " + (START_SLOT_LOWER_HOUR) + " and " + (START_SLOT_HIGHER_HOUR);

    public static final String ERROR_INVALID_END_DATE_HOUR =
            "Invalid - End Date Hour : %s. Should be between : " + (END_SLOT_LOWER_HOUR) + " and " + (END_SLOT_HIGHER_HOUR);

    public static final String ERROR_INVALID_END_DATE_YEAR =
            "Invalid - End Date Year : %s. Should be before or equal to : " + (END_SLOT_YEAR);

    public static final String ERROR_INVALID_INTERVIEW_LIST =
            "Invalid - Interview List is Empty or there is a Candidate present : %s.";

    public static final String ERROR_INVALID_CANDIDATE_LIST =
            "Invalid - Candidate List is Empty or there is a Interviewer present : %s.";

}
