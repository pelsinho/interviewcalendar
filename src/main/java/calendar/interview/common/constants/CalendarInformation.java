package calendar.interview.common.constants;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

@Service
public class CalendarInformation {

    /**
     * FROM ENV VARIABLES
     * To be able to change the year in runtime
     * If necessary we may add get other information using this process
     */

    public static Integer END_SLOT_YEAR;

    @Value("${end.slot.year}")
    public void setEndSlotYear(Integer endSlotYear) {
        END_SLOT_YEAR = endSlotYear;
    }


    /**
     * Constants
     */

    public static final String SLOT_DATE_TIME_FORMAT = "dd-MM-yyyy-HH";
    public static final String SLOT_START_DATE_TIME_EXAMPLE = "10-09-2021-14";
    public static final String SLOT_END_DATE_TIME_EXAMPLE = "20-09-2021-14";

    public static final String SLOT_DATE_TIME_FORMAT_REGEX
            = "^(0[1-9]|1[0-9]|2[0-9]|3[0-1])+(-)+(0[1-9]|1[0-2])+(-)+(202[1-9]|202[1-9])+(-)+(09|10|11|12|13|14|15|16|17)$";
    public static final String SLOT_END_DATE_TIME_FORMAT_REGEX
            = "^(0[1-9]|1[0-9]|2[0-9]|3[0-1])+(-)+(0[1-9]|1[0-2])+(-)+(202[1-9]|202[1-9])+(-)+(10|11|12|13|14|15|16|17|18)$";

    public static final String SLOT_START_DATE_TIME_FORMAT_REGEX_MESSAGE = "Please validate your start date & time entry using the following pattern : " +
            SLOT_DATE_TIME_FORMAT_REGEX;

    public static final String SLOT_END_DATE_TIME_FORMAT_REGEX_MESSAGE = "Please validate your end date & time entry using the following pattern : " +
            SLOT_END_DATE_TIME_FORMAT_REGEX;

    public static final Integer START_SLOT_LOWER_HOUR = 9;
    public static final Integer START_SLOT_HIGHER_HOUR = 17;
    public static final Integer END_SLOT_LOWER_HOUR = 10;
    public static final Integer END_SLOT_HIGHER_HOUR = 18;
    public static final Integer SLOT_HOUR = 1;

    public static final String LOGIN_NAME_DATA_TYPE = "string";
    public static final String LOGIN_NAME_EXAMPLE = "NGOMES";
    public static final String LOGIN_NAME_NOTES = "Users must be registered in calendar interview application";

    public static final String END_CALENDAR_LOCAL_DATE_TIME_NOTES =
            "<p>1 - Local Date and Time Slot must be send in this format : " + SLOT_DATE_TIME_FORMAT + "</p>" +
                    "<p>2 - Weekend Days Not Allowed.</p>" +
                    "<p>3 - End Range Time from 10 to 18.</p>" +
                    "<p>4 - You can use this regex to validate your entry : " + SLOT_END_DATE_TIME_FORMAT_REGEX + "</p>" +
                    "<p>5 - Must be After startLocalDateTime</p>";

    public static final String START_CALENDAR_LOCAL_DATE_TIME_NOTES =
            "<p>1 - Local Date and Time Slot must be send in this format : " + SLOT_DATE_TIME_FORMAT + "</p>" +
                    "<p>2 - Weekend Days Not Allowed</p>" +
                    "<p>3 - Must be After today</p>" +
                    "<p>4 - Start Range Time from 09 to 17</p>" +
                    "<p>5 - You can use this regex to validate your entry : " + SLOT_DATE_TIME_FORMAT_REGEX + "</p>" +
                    "<p>6 - Must be Before endLocalDateTime</p>";

    public static final String THIS_FIELD_CANNOT_BE_NULL = "This field cannot be null.";


    /**
     * ERROR MSGS
     */

    public static final String NO_REGISTER_FOUND = "No register has been found";
    public static final String UNABLE_TO_SAVE_REGISTER = "Unable to Save Register. Missing Necessary Parameters : User or Calendar";




}
