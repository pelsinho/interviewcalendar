package calendar.interview.common.log;

import calendar.interview.common.enums.CalendarValidationPhase;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DefaultLog {

    private DefaultLog() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    /**
     * Default Method to have a common log error msg.
     * Can be add more information in the future if necessary
     * Like Objects information
     */
    public static void defaultLogError(Exception ex) {

        log.error(String.format("ERROR - Exception : %s -  on Class : %s - on Method : %s " +
                        " - StackTrace : ",
                ex.getClass().toString(),
                ex.getStackTrace()[0].getClassName(),
                ex.getStackTrace()[0].getMethodName()), ex);

    }


    public static void registerNotFoundLogError(
            String className,
            String login,
            String startLocalDateTime,
            String endLocalDateTime) {

        log.error(String.format("ERROR - Register Not Found - on Class : %s " +
                        " - Login : %s - startLocalDateTime : %s -  endLocalDateTime : %s",
                className,
                login,
                startLocalDateTime,
                endLocalDateTime));

    }


    public static void validationLogError(
            CalendarValidationPhase calendarValidationPhase,
            String errorMsg) {

        log.error(String.format("ERROR - on Validation Phase : %s - Error Messages : %s ",
                calendarValidationPhase,
                errorMsg));

    }


}
