package calendar.interview.common.suppliers;

import calendar.interview.common.predicates.CalendarPredicates;
import calendar.interview.common.predicates.GenericPredicates;
import calendar.interview.data.entities.CalendarDB;
import calendar.interview.data.entities.UserDB;
import calendar.interview.data.model.CreateCalendar;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import static calendar.interview.common.constants.CalendarInformation.SLOT_HOUR;

public class CalendarSuppliers {

    private CalendarSuppliers() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    /**
     * Map and Get CalendarDB
     * We need to check if is a Valid Slot Local Data and Time
     * No Weekend Days Allow
     * Start Interview Slot
     * End Interview Slot
     */
    public static Supplier<List<CalendarDB>> getCalendarDB(CreateCalendar createCalendar, UserDB userDB) {

        if (GenericPredicates.checkIfNullOrEmpty.test(createCalendar) ||
                GenericPredicates.checkIfNullOrEmpty.test(createCalendar.getStartCalendarLocalDateTime()) ||
                GenericPredicates.checkIfNullOrEmpty.test(userDB) ||
                GenericPredicates.checkIfNullOrEmpty.test(userDB.getLogin())) return ArrayList::new;

        List<CalendarDB> calendarDBList = getCalendarDBList(userDB);
        LocalDateTime start = createCalendar.getStartCalendarLocalDateTime();

        while (start.isBefore(createCalendar.getEndCalendarLocalDateTime())) {

            if (CalendarPredicates.isValidSlotLocalDateTime.test(start) &&
                    CalendarPredicates.slotAlreadyAllocated.negate().test(start, userDB.getCalendarList())) {

                CalendarDB calendarDB = new CalendarDB();
                calendarDB.setSlotDateTime(Timestamp.valueOf(start));
                calendarDB.setUser(userDB);

                calendarDBList.add(calendarDB);

            }

            start = start.plusHours(SLOT_HOUR);

        }

        return () -> calendarDBList;

    }


    /**
     * Uses existent one if exists, or create a new List
     */
    private static List<CalendarDB> getCalendarDBList(UserDB userDB) {

        if (GenericPredicates.checkIfNullOrEmpty.test(userDB.getCalendarList())) {
            return new ArrayList<>();
        }

        return userDB.getCalendarList();

    }


}
