package calendar.interview.common.functions;

import calendar.interview.common.predicates.GenericPredicates;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import static calendar.interview.common.constants.CalendarInformation.SLOT_DATE_TIME_FORMAT;

public class GenericFunctions {

    private GenericFunctions() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    /**
     * Format a Map Values to a String splitted by
     */
    public static BiFunction<@NotNull @NotEmpty Map<String, ?>, String, String> getMapValueAsStringBy = (map, splitBy) -> {

        if (GenericPredicates.checkIfNullOrEmpty.test(map)) return "";

        splitBy = GenericPredicates.checkIfNullOrEmpty.test(splitBy) ? "" : splitBy;

        return map.keySet().stream()
                .map(key -> map.get(key) + " ")
                .collect(Collectors.joining(splitBy, "", ""));

    };


    /**
     * Transform a String into a Local Date Time using application pattern format
     */
    public static Function<@NotNull @NotEmpty String, LocalDateTime> getSlotDateTimeFormatter = localDateTime -> {

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(SLOT_DATE_TIME_FORMAT);
        return LocalDateTime.parse(localDateTime, dateTimeFormatter);

    };


    /**
     * Transform a TimeStamp List into a Local Date and Time List
     */
    public static Function<List<Timestamp>, List<LocalDateTime>> getLocalDateTime = timestampList -> {

        if (GenericPredicates.checkIfNullOrEmpty.test(timestampList)) return new ArrayList<>();

        return timestampList
                .stream()
                .map(Timestamp::toLocalDateTime)
                .collect(Collectors.toList());

    };





}
