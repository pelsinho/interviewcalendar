package calendar.interview.common.enums;

import lombok.Getter;

/**
 * Please check this home page to understand how to use Eureka Server
 * https://www.youtube.com/watch?v=GxLjcOE35oA&list=PLqq-6Pq4lTTZSKAFG6aCDVDP86Qx4lNas&index=21
 */
@Getter
public enum EurekaEnvironmentApplications {

    INTERVIEW_CALENDAR("http://interviewcalendar/"),
    REFERENCE_DATA("http://referencedata/");

    private String eurekaUrl;

    EurekaEnvironmentApplications(String eurekaUrl) {
        this.eurekaUrl = eurekaUrl;
    }



}
