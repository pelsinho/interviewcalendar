package calendar.interview.common.enums;

public enum CalendarValidationPhase {

    CREATE_CALENDAR,
    SCHEDULE_INTERVIEW;

}
