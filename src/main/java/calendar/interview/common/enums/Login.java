package calendar.interview.common.enums;

import lombok.Getter;

/**
 * To make easy to test,
 * I´ve create this class, with some "people" register in our system
 * Create
 * 5 interviewers
 * and
 * 5 candidates
 */
@Getter
public enum Login {

    NGOMES(Roles.CANDIDATE),
    JPINTO(Roles.CANDIDATE),
    RMELO(Roles.CANDIDATE),
    PPAULO(Roles.CANDIDATE),
    PSANCHES(Roles.CANDIDATE),
    RSANCHES(Roles.INTERVIEWER),
    TMOTA(Roles.INTERVIEWER),
    PCOELHO(Roles.INTERVIEWER),
    KMOTA(Roles.INTERVIEWER),
    HMOURA(Roles.INTERVIEWER);

    private Roles role;

    Login(Roles role) {
        this.role = role;
    }

}
