package calendar.interview.common.enums;

import lombok.Getter;

/**
 * Application User Roles
 */
@Getter
public enum Roles {

    INTERVIEWER("Interviewer"),
    CANDIDATE("Candidate");

    private String roleName;

    Roles(String roleName) {
        this.roleName = roleName;
    }

    public String roleName() {
        return roleName;
    }

    public static Roles fromString(String roleName) {
        for (Roles b : Roles.values()) {
            if (b.roleName.equalsIgnoreCase(roleName)) {
                return b;
            }
        }
        return null;
    }

}
