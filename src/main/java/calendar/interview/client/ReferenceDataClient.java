package calendar.interview.client;

import calendar.interview.client.interfaces.IReferenceDataClient;
import calendar.interview.common.enums.EurekaEnvironmentApplications;
import calendar.interview.data.dto.UserReportDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
@RequiredArgsConstructor
public class ReferenceDataClient implements IReferenceDataClient {


    private final RestTemplate restTemplate;


    /**
     * Reference Data Will be Done in Phase 2
     */
    @Override
    public String getReferenceDataByClassId(String referenceDataId) {

        return restTemplate.getForObject(EurekaEnvironmentApplications.REFERENCE_DATA.name() + referenceDataId, String.class);

    }


    /**
     * Implementing Eureka Call to Test Discovery Process
     * This endpoint will be removed when Reference Data API is available
     */
    @Override
    public UserReportDTO getScheduleCalendarByLoginByEureka(String login) {

        return restTemplate.getForObject(EurekaEnvironmentApplications.INTERVIEW_CALENDAR.getEurekaUrl() + "/calendar/byLogin/" + login, UserReportDTO.class);

    }



}
