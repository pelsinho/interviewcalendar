package calendar.interview.client.interfaces;

import calendar.interview.data.dto.UserReportDTO;

public interface IReferenceDataClient {
    String getReferenceDataByClassId(String referenceDataId);

    UserReportDTO getScheduleCalendarByLoginByEureka(String login);
}
