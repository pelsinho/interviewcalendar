package calendar.interview.config.validation;

import calendar.interview.common.predicates.GenericPredicates;

import javax.validation.*;
import java.util.Set;

public abstract class SelfValidation<T> {

    private Validator validator;

    public SelfValidation() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    protected void validateSelf() {

        Set<ConstraintViolation<T>> violations = validator.validate((T) this);
        if (GenericPredicates.checkIfNullOrEmpty.negate().test(violations.isEmpty())) {
            throw new ConstraintViolationException(violations);
        }

    }

}
