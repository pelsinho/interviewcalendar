package calendar.interview.config.controladvice;

import calendar.interview.common.log.DefaultLog;
import calendar.interview.config.controladvice.interfaces.IMyControllerAdvice;
import calendar.interview.exceptions.CalendarValidationException;
import calendar.interview.exceptions.ExceptionType;
import calendar.interview.exceptions.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

@ControllerAdvice
public class MyControllerAdvice extends ResponseEntityExceptionHandler implements IMyControllerAdvice {

    @Override
    @ExceptionHandler(CalendarValidationException.class)
    public ResponseEntity<ServiceException> handleCalendarValidationException(CalendarValidationException ex) {

        ServiceException serviceException = getDefaultServiceException(ex, ExceptionType.UNPROCESSABLE_ENTITY);
        return new ResponseEntity<>(serviceException, HttpStatus.UNPROCESSABLE_ENTITY);

    }

    @Override
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ServiceException> handleConstraintViolationException(ConstraintViolationException ex) {

        ServiceException serviceException = getDefaultServiceException(ex, ExceptionType.UNPROCESSABLE_ENTITY);
        return new ResponseEntity<>(serviceException, HttpStatus.UNPROCESSABLE_ENTITY);

    }

    @Override
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleDefaultException(Exception ex) {

        ServiceException serviceException = getDefaultServiceException(ex, ExceptionType.SERVER_ERROR);
        return new ResponseEntity<>(serviceException, HttpStatus.BAD_REQUEST);

    }



    private ServiceException getDefaultServiceException(
            Exception ex,
            ExceptionType exceptionType) {

        DefaultLog.defaultLogError(ex);

        return new ServiceException(
                LocalDateTime.now(),
                exceptionType,
                ex.getClass().toString(),
                ex.getMessage());

    }

}
