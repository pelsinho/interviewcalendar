package calendar.interview.config.controladvice.interfaces;

import calendar.interview.exceptions.CalendarValidationException;
import calendar.interview.exceptions.ServiceException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;

/**
 * All must return ServiceException.class
 */
public interface IMyControllerAdvice {
    @ExceptionHandler(CalendarValidationException.class)
    ResponseEntity<ServiceException> handleCalendarValidationException(CalendarValidationException ex);

    @ExceptionHandler(ConstraintViolationException.class)
    ResponseEntity<ServiceException> handleConstraintViolationException(ConstraintViolationException ex);

    @ExceptionHandler(Exception.class)
    ResponseEntity<Object> handleDefaultException(Exception ex);
}
