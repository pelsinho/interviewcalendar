package calendar.interview.config.resttemplate;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestTemplateStrategy {

    @Bean
    @LoadBalanced //Eureka Process
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

}
