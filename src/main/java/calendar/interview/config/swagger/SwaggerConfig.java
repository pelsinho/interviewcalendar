package calendar.interview.config.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

import static calendar.interview.common.constants.CalendarInformation.END_SLOT_YEAR;
import static calendar.interview.common.constants.CalendarInformation.SLOT_DATE_TIME_FORMAT;
import static springfox.documentation.builders.RequestHandlerSelectors.withClassAnnotation;

/**
 * Swagger Control Class
 */
@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .select()
                .paths(PathSelectors.any())
                .apis(withClassAnnotation(RestController.class))
                .build()
                .apiInfo(internalApiInfo())
                .globalResponseMessage(RequestMethod.GET, responseMessage())
                .globalResponseMessage(RequestMethod.POST, responseMessage())
                .globalResponseMessage(RequestMethod.PUT, responseMessage())
                .globalResponseMessage(RequestMethod.DELETE, responseMessage());
    }



    private ApiInfo internalApiInfo() {
        String title = "Calendar Interview API";
        String description =
                        "<p>Calendar Interview API Documentation</p>" +
                        "<p>This API schedule 1 hour slot interviews from today to the end of " + END_SLOT_YEAR +
                                ", please follow the rules bellow :</p>" +
                        "<br/><p>Business Rules : </p>" +
                        "<p>1 - An interview slot is a 1-hour period of time that spreads from the beginning of any " +
                                "hour until the beginning of the next hour. For example, a time span between 9am and " +
                                "10am is a valid interview slot, whereas between 9:30am and 10:30am it is not.</p>" +
                        "<p>2 - Each of the interviewers sets their availability slots. For example, the interviewer " +
                                "David is available next week each day from 9am through 4pm without breaks and " +
                                "the interviewer Ingrid is available from 12pm to 6pm on Monday and Wednesday " +
                                "next week, and from 9am to 12pm on Tuesday and Thursday.</p>" +
                        "<p>3 - Each of the candidates sets their requested slots for the interview. For example, the " +
                                "candidate Carl is available for the interview from 9am to 10am any weekday next " +
                                "week and from 10am to 12pm on Wednesday.</p>" +
                        "<p>4 - Anyone may then query the API to get a collection of periods of time when it’s " +
                                "possible arrange an interview for a particular candidate and one or more interviewers. " +
                                "In this example, if the API is queries for the candidate Carl and interviewers Ines and " +
                                "Ingrid, the response should be a collection of 1-hour slots: from 9am to 10am on " +
                                "Tuesday, from 9am to 10am on Thursday.</p>" +
                        "<p>5 - We can only offer interviews from Monday to Friday during working hours - Starting at 09 and " +
                                " Ending at 18.</p>" +
                        "<p>Other Info : </p>" +
                        "<p>1 - Local Date and Time (Starting and Ending) format is : " + SLOT_DATE_TIME_FORMAT + "</p>" +
                        "<p>2 - Available Candidate Users Logins : NGOMES, JPINTO, RMELO, PPAULO, PSANCHES</p>" +
                        "<p>3 - Available Interview Users Logins : RSANCHES, TMOTA, PCOELHO, KMOTA, HMOURA</p>" +
                        "<p>4 - If you need to register a new user please contact our support team.</p>" +
                        "<p>5 - You will find more information in each Dto/Endpoint documentation.</p>";
        String version = "0.0.1";
        Contact contact = new Contact("Application Support Team", "https://www.company.com/contact-us", "company@support.com");

        return new ApiInfo(title, description, version, null, contact, null, null,
                Collections.emptyList());
    }


    private List<ResponseMessage> responseMessage() {

        ResponseMessage internalServerError = new ResponseMessageBuilder()
                .code(500)
                .message("Internal server error")
                .build();

        ResponseMessage forbidden = new ResponseMessageBuilder()
                .code(403)
                .message("Forbidden")
                .build();

        ResponseMessage unauthorised = new ResponseMessageBuilder()
                .code(401)
                .message("Unauthorised")
                .build();

        ResponseMessage notFound = new ResponseMessageBuilder()
                .code(404)
                .message("Not found")
                .build();

        ResponseMessage success = new ResponseMessageBuilder()
                .code(200)
                .message("Success")
                .build();

        ResponseMessage invalidRequestBody = new ResponseMessageBuilder()
                .code(422)
                .message("Invalid request body")
                .build();

        return List.of(
                internalServerError,
                forbidden,
                unauthorised,
                notFound,
                success,
                invalidRequestBody);

    }
}
