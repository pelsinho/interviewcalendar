package calendar.interview.services.calendarvalidation;

import calendar.interview.common.log.DefaultLog;
import calendar.interview.data.model.CalendarValidationRequest;
import calendar.interview.data.model.CalendarValidationResponse;
import calendar.interview.exceptions.CalendarValidationException;
import calendar.interview.services.calendarvalidation.interfaces.ICalendarValidationProcess;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class CalendarValidationProcess implements ICalendarValidationProcess {

    private final CalendarValidationControl calendarValidationControl;
    private final CalendarValidationErrorServices calendarValidationErrorServices;

    /**
     * Controls Calendar Validation Process
     * Logs and Throws Exception if an invalid process has been founded
     */
    @Override
    public void validateCalendarProcess(
            CalendarValidationRequest calendarValidationRequest) throws CalendarValidationException {

        try {

            CalendarValidationResponse calendarValidationResponse
                    = calendarValidationControl.isAValidCalendarRequest(calendarValidationRequest);
            calendarValidationErrorServices.checkValidationExistErrors(calendarValidationRequest, calendarValidationResponse);

        } catch (Exception ex) {

            DefaultLog.defaultLogError(ex);
            throw new CalendarValidationException(ex.getMessage());

        }

    }

}
