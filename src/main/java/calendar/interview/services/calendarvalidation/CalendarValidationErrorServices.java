package calendar.interview.services.calendarvalidation;

import calendar.interview.common.functions.GenericFunctions;
import calendar.interview.common.log.DefaultLog;
import calendar.interview.common.predicates.GenericPredicates;
import calendar.interview.data.model.CalendarValidationRequest;
import calendar.interview.data.model.CalendarValidationResponse;
import calendar.interview.exceptions.CalendarValidationException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CalendarValidationErrorServices {

    protected void checkValidationExistErrors(
            CalendarValidationRequest calendarValidationRequest,
            CalendarValidationResponse calendarValidationResponse) throws CalendarValidationException {

        if (GenericPredicates.checkIfNullOrEmpty.negate().test(calendarValidationResponse.getInvalidMessages())) {

            String errorMsg = GenericFunctions.getMapValueAsStringBy.apply(calendarValidationResponse.getInvalidMessages(), ",");

            DefaultLog.validationLogError(calendarValidationRequest.getCalendarValidationPhase(), errorMsg);

            throw new CalendarValidationException(errorMsg);

        }

    }

}
