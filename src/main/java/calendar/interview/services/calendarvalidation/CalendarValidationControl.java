package calendar.interview.services.calendarvalidation;

import calendar.interview.data.model.CalendarValidationRequest;
import calendar.interview.data.model.CalendarValidationResponse;
import calendar.interview.services.calendarvalidation.handles.*;
import calendar.interview.services.calendarvalidation.manager.CalendarValidationHandlesMap;
import calendar.interview.services.calendarvalidation.manager.ICalendarValidationHandles;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

@Slf4j
@Component
@RequiredArgsConstructor
public class CalendarValidationControl {

    private final EndSlotDateDayIsValidHandle endSlotDateDayIsValidHandle;
    private final EndSlotDateHourIsValidHandle endSlotDateHourIsValidHandle;
    private final StartSlotDateDayIsValidHandle startSlotDateDayIsValidHandle;
    private final StartSlotDateHourIsValidHandle startSlotDateHourIsValidHandle;
    private final StartSlotDateIsBeforeEndSlotDateHandle startSlotDateIsBeforeEndSlotDateHandle;
    private final EndSlotDateYearIsValidHandle endSlotDateYearIsValidHandle;
    private final StartSlotDateIsValidHandle startSlotDateIsValidHandle;
    private final CandidateListIsValidHandle candidateListIsValidHandle;
    private final InterviewListIsValidHandle interviewListIsValidHandle;

    private CalendarValidationHandlesMap calendarValidationHandlesMap;

    @PostConstruct
    private void startCalendarValidationHandles() {
        populateCalendarValidationHandles();
    }

    private void populateCalendarValidationHandles() {

        //Cargo Validation
        calendarValidationHandlesMap = new CalendarValidationHandlesMap();
        calendarValidationHandlesMap.addHandle(endSlotDateDayIsValidHandle);
        calendarValidationHandlesMap.addHandle(endSlotDateYearIsValidHandle);
        calendarValidationHandlesMap.addHandle(endSlotDateHourIsValidHandle);
        calendarValidationHandlesMap.addHandle(startSlotDateDayIsValidHandle);
        calendarValidationHandlesMap.addHandle(startSlotDateIsValidHandle);
        calendarValidationHandlesMap.addHandle(startSlotDateHourIsValidHandle);
        calendarValidationHandlesMap.addHandle(startSlotDateIsBeforeEndSlotDateHandle);
        calendarValidationHandlesMap.addHandle(candidateListIsValidHandle);
        calendarValidationHandlesMap.addHandle(interviewListIsValidHandle);

    }


    /**
     * Controls Validation Handles
     * Add Invalid Message if necessary
     */
    protected CalendarValidationResponse isAValidCalendarRequest(
            CalendarValidationRequest calendarValidationRequest) {

        CalendarValidationResponse calendarValidationResponse = new CalendarValidationResponse();

        for (ICalendarValidationHandles calendarValidationHandles : calendarValidationHandlesMap.handlesList()) {

            if (calendarValidationHandles.canHandle(calendarValidationRequest) && calendarValidationHandles.isInvalidRequest(calendarValidationRequest)) {

                /**
                * I set to validate all the possible errors in the request
                * If we want we can add a break and add just the first error founded.
                */

                calendarValidationResponse.getInvalidMessages().putAll((Map<String, String>) calendarValidationHandles.addInvalidMessage(calendarValidationRequest));

            }

        }

        return calendarValidationResponse;

    }

}
