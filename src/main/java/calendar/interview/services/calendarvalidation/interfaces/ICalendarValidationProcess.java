package calendar.interview.services.calendarvalidation.interfaces;

import calendar.interview.data.model.CalendarValidationRequest;
import calendar.interview.exceptions.CalendarValidationException;

public interface ICalendarValidationProcess {
    void validateCalendarProcess(
            CalendarValidationRequest calendarValidationRequest) throws CalendarValidationException;
}
