package calendar.interview.services.calendarvalidation.handles;

import calendar.interview.common.enums.CalendarValidationPhase;
import calendar.interview.common.predicates.CalendarPredicates;
import calendar.interview.data.model.CalendarValidationRequest;
import calendar.interview.services.calendarvalidation.manager.ICalendarValidationHandles;
import org.springframework.stereotype.Service;

import java.util.Map;

import static calendar.interview.common.constants.CalendarValidationMessages.ERROR_INVALID_START_DATE_HOUR;

@Service
public class StartSlotDateHourIsValidHandle implements ICalendarValidationHandles<CalendarValidationRequest, Map<String, String>> {

    @Override
    public boolean canHandle(CalendarValidationRequest calendarValidationRequest) {
        return calendarValidationRequest.getCalendarValidationPhase().equals(CalendarValidationPhase.CREATE_CALENDAR);
    }

    /**
     * Validate if Start Hour is Invalid
     */
    @Override
    public boolean isInvalidRequest(CalendarValidationRequest calendarValidationRequest) {
        return CalendarPredicates.isValidSlotStartHour.negate().test(calendarValidationRequest.getCreateCalendar().getStartCalendarLocalDateTime());
    }

    @Override
    public Map<String, String> addInvalidMessage(CalendarValidationRequest calendarValidationRequest) {

        return Map.of(
                calendarValidationRequest.getCalendarValidationPhase().name(),
                String.format(
                        ERROR_INVALID_START_DATE_HOUR,
                        calendarValidationRequest.getCreateCalendar().getStartCalendarLocalDateTime().getHour()));

    }

}