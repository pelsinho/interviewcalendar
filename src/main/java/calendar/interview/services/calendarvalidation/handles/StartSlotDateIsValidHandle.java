package calendar.interview.services.calendarvalidation.handles;

import calendar.interview.common.enums.CalendarValidationPhase;
import calendar.interview.common.predicates.CalendarPredicates;
import calendar.interview.data.model.CalendarValidationRequest;
import calendar.interview.services.calendarvalidation.manager.ICalendarValidationHandles;
import org.springframework.stereotype.Service;

import java.util.Map;

import static calendar.interview.common.constants.CalendarValidationMessages.ERROR_INVALID_START_DATE_MUST_BE_IN_THE_FUTURE;

@Service
public class StartSlotDateIsValidHandle implements ICalendarValidationHandles<CalendarValidationRequest, Map<String, String>> {

    @Override
    public boolean canHandle(CalendarValidationRequest calendarValidationRequest) {
        return calendarValidationRequest.getCalendarValidationPhase().equals(CalendarValidationPhase.CREATE_CALENDAR);
    }

    /**
     * Invalid if Start Slot if after today
     */
    @Override
    public boolean isInvalidRequest(CalendarValidationRequest calendarValidationRequest) {
        return CalendarPredicates.isStartSlotDateAfterToday.negate().test(calendarValidationRequest.getCreateCalendar().getStartCalendarLocalDateTime());
    }

    @Override
    public Map<String, String> addInvalidMessage(CalendarValidationRequest calendarValidationRequest) {

        return Map.of(
                calendarValidationRequest.getCalendarValidationPhase().name(),
                String.format(
                        ERROR_INVALID_START_DATE_MUST_BE_IN_THE_FUTURE,
                        calendarValidationRequest.getCreateCalendar().getStartCalendarLocalDateTime()));

    }

}