package calendar.interview.services.calendarvalidation.handles;

import calendar.interview.common.enums.CalendarValidationPhase;
import calendar.interview.common.enums.Roles;
import calendar.interview.common.predicates.GenericPredicates;
import calendar.interview.common.predicates.UserPredicates;
import calendar.interview.data.model.CalendarValidationRequest;
import calendar.interview.services.calendarvalidation.manager.ICalendarValidationHandles;
import org.springframework.stereotype.Component;

import java.util.Map;

import static calendar.interview.common.constants.CalendarValidationMessages.ERROR_INVALID_INTERVIEW_LIST;

@Component
public class InterviewListIsValidHandle implements ICalendarValidationHandles<CalendarValidationRequest, Map<String, String>> {

    @Override
    public boolean canHandle(CalendarValidationRequest calendarValidationRequest) {
        return calendarValidationRequest.getCalendarValidationPhase().equals(CalendarValidationPhase.SCHEDULE_INTERVIEW);
    }

    /**
     * Invalid if List is Null or Empty , or has a Candidate
     */
    @Override
    public boolean isInvalidRequest(CalendarValidationRequest calendarValidationRequest) {

        boolean interviewListNull = GenericPredicates.checkIfNullOrEmpty.test(calendarValidationRequest.getInterviewSchedule().getInterviewers());
        if (interviewListNull) return true;

        return UserPredicates.allHaveThisRole.negate().test(calendarValidationRequest.getInterviewSchedule().getInterviewers(), Roles.INTERVIEWER);

    }

    @Override
    public Map<String, String> addInvalidMessage(CalendarValidationRequest calendarValidationRequest) {

        return Map.of(
                calendarValidationRequest.getCalendarValidationPhase().name(),
                ERROR_INVALID_INTERVIEW_LIST);

    }

}