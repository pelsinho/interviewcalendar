package calendar.interview.services.calendarvalidation.handles;

import calendar.interview.common.enums.CalendarValidationPhase;
import calendar.interview.common.predicates.CalendarPredicates;
import calendar.interview.data.model.CalendarValidationRequest;
import calendar.interview.services.calendarvalidation.manager.ICalendarValidationHandles;
import org.springframework.stereotype.Service;

import java.util.Map;

import static calendar.interview.common.constants.CalendarValidationMessages.ERROR_INVALID_START_DATE_WEEKEND_DAY_DETECTED;

@Service
public class StartSlotDateDayIsValidHandle implements ICalendarValidationHandles<CalendarValidationRequest, Map<String, String>> {

    @Override
    public boolean canHandle(CalendarValidationRequest calendarValidationRequest) {
        return calendarValidationRequest.getCalendarValidationPhase().equals(CalendarValidationPhase.CREATE_CALENDAR);
    }

    /**
     * Invalid if Start Slot Calendar Day is a Weekend Day
     */
    @Override
    public boolean isInvalidRequest(CalendarValidationRequest calendarValidationRequest) {
        return CalendarPredicates.isAWeekendDay.test(calendarValidationRequest.getCreateCalendar().getStartCalendarLocalDateTime().getDayOfWeek());
    }

    @Override
    public Map<String, String> addInvalidMessage(CalendarValidationRequest calendarValidationRequest) {

        return Map.of(
                calendarValidationRequest.getCalendarValidationPhase().name(),
                String.format(
                        ERROR_INVALID_START_DATE_WEEKEND_DAY_DETECTED,
                        calendarValidationRequest.getCreateCalendar().getStartCalendarLocalDateTime().getDayOfWeek()));

    }

}