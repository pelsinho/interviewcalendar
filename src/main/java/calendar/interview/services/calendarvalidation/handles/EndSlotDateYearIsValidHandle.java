package calendar.interview.services.calendarvalidation.handles;

import calendar.interview.common.enums.CalendarValidationPhase;
import calendar.interview.common.predicates.CalendarPredicates;
import calendar.interview.data.model.CalendarValidationRequest;
import calendar.interview.services.calendarvalidation.manager.ICalendarValidationHandles;
import org.springframework.stereotype.Component;

import java.util.Map;

import static calendar.interview.common.constants.CalendarValidationMessages.ERROR_INVALID_END_DATE_YEAR;

@Component
public class EndSlotDateYearIsValidHandle implements ICalendarValidationHandles<CalendarValidationRequest, Map<String, String>> {

    @Override
    public boolean canHandle(CalendarValidationRequest calendarValidationRequest) {
        return calendarValidationRequest.getCalendarValidationPhase().equals(CalendarValidationPhase.CREATE_CALENDAR);
    }

    /**
     * Validate if End Year is Invalid
     */
    @Override
    public boolean isInvalidRequest(CalendarValidationRequest calendarValidationRequest) {
        return CalendarPredicates.isValidSlotEndYear.negate().test(calendarValidationRequest.getCreateCalendar().getEndCalendarLocalDateTime());
    }

    @Override
    public Map<String, String> addInvalidMessage(CalendarValidationRequest calendarValidationRequest) {

        return Map.of(
                calendarValidationRequest.getCalendarValidationPhase().name(),
                String.format(
                        ERROR_INVALID_END_DATE_YEAR,
                        calendarValidationRequest.getCreateCalendar().getEndCalendarLocalDateTime().getYear()));

    }

}