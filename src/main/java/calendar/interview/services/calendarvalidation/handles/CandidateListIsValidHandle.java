package calendar.interview.services.calendarvalidation.handles;

import calendar.interview.common.enums.CalendarValidationPhase;
import calendar.interview.common.enums.Roles;
import calendar.interview.common.predicates.GenericPredicates;
import calendar.interview.common.predicates.UserPredicates;
import calendar.interview.data.model.CalendarValidationRequest;
import calendar.interview.services.calendarvalidation.manager.ICalendarValidationHandles;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static calendar.interview.common.constants.CalendarValidationMessages.ERROR_INVALID_CANDIDATE_LIST;

@Component
public class CandidateListIsValidHandle implements ICalendarValidationHandles<CalendarValidationRequest, Map<String, String>> {

    @Override
    public boolean canHandle(CalendarValidationRequest calendarValidationRequest) {
        return calendarValidationRequest.getCalendarValidationPhase().equals(CalendarValidationPhase.SCHEDULE_INTERVIEW);
    }

    /**
     * Invalid if List is Null or has an Interviewer
     */
    @Override
    public boolean isInvalidRequest(CalendarValidationRequest calendarValidationRequest) {

        boolean candidateListNull = GenericPredicates.checkIfNullOrEmpty.test(calendarValidationRequest.getInterviewSchedule().getCandidate());
        if (candidateListNull) return true;

        return UserPredicates.allHaveThisRole.negate().test(List.of(calendarValidationRequest.getInterviewSchedule().getCandidate()), Roles.CANDIDATE);

    }

    @Override
    public Map<String, String> addInvalidMessage(CalendarValidationRequest calendarValidationRequest) {

        return Map.of(
                calendarValidationRequest.getCalendarValidationPhase().name(),
                ERROR_INVALID_CANDIDATE_LIST);

    }

}