package calendar.interview.services.calendarvalidation.manager;

import java.util.List;

public interface ICalendarValidationHandlesMap<H> {

    List<H> handlesList();

    void addHandle(H handle);

}
