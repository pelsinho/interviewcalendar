package calendar.interview.services.calendarvalidation.manager;

import java.util.ArrayList;
import java.util.List;

public class CalendarValidationHandlesMap implements ICalendarValidationHandlesMap<ICalendarValidationHandles> {

    private final ArrayList<ICalendarValidationHandles> handlesList;

    public CalendarValidationHandlesMap() {
        handlesList = new ArrayList<>();
    }


    @Override
    public List<ICalendarValidationHandles> handlesList() {
        return handlesList;
    }

    @Override
    public void addHandle(ICalendarValidationHandles handle) {
        if (handle != null)
            handlesList.add(handle);
    }
}
