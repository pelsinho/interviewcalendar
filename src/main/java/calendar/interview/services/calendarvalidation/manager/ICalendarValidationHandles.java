package calendar.interview.services.calendarvalidation.manager;


public interface ICalendarValidationHandles<Q, P> {

    boolean canHandle(Q calendarValidationRequest);

    boolean isInvalidRequest(Q calendarValidationRequest);

    P addInvalidMessage(Q calendarValidationRequest);

}
