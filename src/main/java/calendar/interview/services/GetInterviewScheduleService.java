package calendar.interview.services;

import calendar.interview.common.enums.CalendarValidationPhase;
import calendar.interview.common.enums.Login;
import calendar.interview.common.functions.GenericFunctions;
import calendar.interview.data.dto.InterviewAvailableScheduleReportDTO;
import calendar.interview.data.model.CalendarValidationRequest;
import calendar.interview.data.model.InterviewSchedule;
import calendar.interview.exceptions.CalendarValidationException;
import calendar.interview.services.calendarvalidation.CalendarValidationProcess;
import calendar.interview.services.interfaces.IGetInterviewSchedule;
import calendar.interview.services.repositories.CalendarRepositoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class GetInterviewScheduleService implements IGetInterviewSchedule {

    private final CalendarValidationProcess calendarValidationProcess;
    private final CalendarRepositoryService calendarRepositoryService;

    @Override
    public InterviewAvailableScheduleReportDTO getInterviewAvailableSchedule(List<Login> interviewersList, Login candidate) throws CalendarValidationException {

        /**
         * Validate if Request is Valid - Based on Chain of Responsibility Design Pattern
         * Candidate -> Must be Present, and must be a Candidate
         * Interviewers -> Must be Present, and must only contains Interviewers
         * Throws Exception if is invalid
         */
        calendarValidationProcess.validateCalendarProcess(
                CalendarValidationRequest.builder()
                        .interviewSchedule(InterviewSchedule
                                .builder()
                                .candidate(candidate)
                                .interviewers(interviewersList)
                                .build())
                        .calendarValidationPhase(CalendarValidationPhase.SCHEDULE_INTERVIEW)
                        .build());

        /**
         * Get Object from DB to be send to User
         */
        List<Timestamp> timestampList
                = calendarRepositoryService.getInterviewAvailableSchedule(getFullLoginList(interviewersList,candidate));

        /**
         * Prepare information and send back
         */
        return getInterviewAvailableScheduleReportDTO(timestampList, interviewersList, candidate);

    }


    /**
     * Get InterviewAvailableScheduleReportDTO to send to user
     */
    private InterviewAvailableScheduleReportDTO getInterviewAvailableScheduleReportDTO(
            List<Timestamp> timestampList,
            List<Login> interviewersList,
            Login candidate) {

        return InterviewAvailableScheduleReportDTO
                .builder()
                .candidate(candidate)
                .interviewers(interviewersList)
                .interviewAvailableSchedules(GenericFunctions.getLocalDateTime.apply(timestampList))
                .build();

    }


    /**
     * Get Full Login List - Interviewers + Candidate
     * Avoid Mutability
     */
    private List<Login> getFullLoginList(List<Login> interviewersList, Login candidate) {

        List<Login> loginList = new ArrayList<>();
        loginList.add(candidate);
        loginList.addAll(interviewersList);

        return loginList;

    }


}
