package calendar.interview.services;

import calendar.interview.common.enums.CalendarValidationPhase;
import calendar.interview.data.dto.CreateCalendarDTO;
import calendar.interview.data.entities.UserDB;
import calendar.interview.data.mappers.CreateCalendarDTOToCreateCalendarMapper;
import calendar.interview.data.model.CalendarValidationRequest;
import calendar.interview.data.model.CreateCalendar;
import calendar.interview.exceptions.CalendarValidationException;
import calendar.interview.exceptions.UnableToSaveUserException;
import calendar.interview.services.calendarvalidation.interfaces.ICalendarValidationProcess;
import calendar.interview.services.interfaces.ICreateCalendarService;
import calendar.interview.services.interfaces.IUserRepositoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CreateCalendarService implements ICreateCalendarService {

    private final ICalendarValidationProcess calendarValidationProcess;
    private final IUserRepositoryService userRepositoryService;

    @Override
    public UUID createCalendarSlot(CreateCalendarDTO createCalendarDTO) throws CalendarValidationException, UnableToSaveUserException {

        /**
         * Map DTO to Internal Object
         */

        CreateCalendar createCalendar = CreateCalendarDTOToCreateCalendarMapper.createCalendarDTOToCreateCalendar(createCalendarDTO);

        /**
         * Validate if Request is Valid - Based on Chain of Responsibility Design Pattern
         * Throws Exception if is invalid
         */
        calendarValidationProcess.validateCalendarProcess(
                CalendarValidationRequest.builder()
                        .createCalendar(createCalendar)
                        .calendarValidationPhase(CalendarValidationPhase.CREATE_CALENDAR)
                        .build());

        /**
         * Get Object to be saved in DB
         */
        UserDB userDB = userRepositoryService.buildUserDBWithCalendar(createCalendar);


        /**
         * Calls Repository to Save Object in DB and return an ID to user
         */
        return userRepositoryService.saveUserCalendar(userDB);

    }





}
