package calendar.interview.services;

import calendar.interview.data.dto.UserReportDTO;
import calendar.interview.data.entities.CalendarDB;
import calendar.interview.data.entities.UserDB;
import calendar.interview.data.mappers.CalendarDBListToUserReportDTOMapper;
import calendar.interview.data.mappers.UserDBListToUserReportDTOMapper;
import calendar.interview.exceptions.NoRegisteredException;
import calendar.interview.services.interfaces.IGetCalendarService;
import calendar.interview.services.repositories.CalendarRepositoryService;
import calendar.interview.services.repositories.UserRepositoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class GetCalendarService implements IGetCalendarService {

    private final UserRepositoryService userRepositoryService;
    private final CalendarRepositoryService calendarRepositoryService;


    /**
     * Get Object from DB By Login and Period and Map to Return Object and Return It
     */
    @Override
    public UserReportDTO getUserCalendar(
            String login,
            String startLocalDateTime,
            String endLocalDateTime) throws NoRegisteredException {

        return CalendarDBListToUserReportDTOMapper.calendarDBToUserReportDTO(getCalendar(login, startLocalDateTime, endLocalDateTime));

    }


    /**
     * Get Object from DB By Login and Map to Return Object and Return It
     */
    @Override
    public UserReportDTO getUserCalendar(
            String login) throws NoRegisteredException {

        return UserDBListToUserReportDTOMapper.userDBToUserReportDTO(getCalendar(login));

    }





    private UserDB getCalendar(
            String login) throws NoRegisteredException {

        return userRepositoryService.getUserSlotCalendar(login);

    }

    private List<CalendarDB> getCalendar(
            String login,
            String startLocalDateTime,
            String endLocalDateTime) throws NoRegisteredException {

        return calendarRepositoryService.getSlotCalendar(login, startLocalDateTime, endLocalDateTime);

    }


}
