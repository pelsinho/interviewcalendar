package calendar.interview.services.interfaces;

import calendar.interview.data.entities.UserDB;
import calendar.interview.data.model.CreateCalendar;
import calendar.interview.exceptions.NoRegisteredException;
import calendar.interview.exceptions.UnableToSaveUserException;

import java.util.UUID;

public interface IUserRepositoryService {
    UserDB buildUserDBWithCalendar(CreateCalendar createCalendar);

    UserDB getUserSlotCalendar(
            String login) throws NoRegisteredException;

    UUID saveUserCalendar(UserDB userDB) throws UnableToSaveUserException;
}
