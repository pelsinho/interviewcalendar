package calendar.interview.services.interfaces;

import calendar.interview.exceptions.NoRegisteredException;

public interface IRemoveCalendarService {

    void removeUserCalendar(
            String login,
            String startLocalDateTime,
            String endLocalDateTime) throws NoRegisteredException;

    void removeUserCalendar(
            String login) throws NoRegisteredException;

}
