package calendar.interview.services.interfaces;

import calendar.interview.common.enums.Login;
import calendar.interview.data.dto.InterviewAvailableScheduleReportDTO;
import calendar.interview.exceptions.CalendarValidationException;

import java.util.List;

public interface IGetInterviewSchedule {
    InterviewAvailableScheduleReportDTO getInterviewAvailableSchedule(List<Login> interviewers, Login candidate) throws CalendarValidationException;
}
