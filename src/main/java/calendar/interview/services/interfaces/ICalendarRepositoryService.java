package calendar.interview.services.interfaces;

import calendar.interview.common.enums.Login;
import calendar.interview.data.entities.CalendarDB;

import java.sql.Timestamp;
import java.util.List;

public interface ICalendarRepositoryService {

    List<Timestamp> getInterviewAvailableSchedule(List<Login> loginList);

    void removeUserCalendar(List<CalendarDB> calendarList);
}
