package calendar.interview.services.interfaces;

import calendar.interview.data.dto.UserReportDTO;
import calendar.interview.exceptions.NoRegisteredException;

public interface IGetCalendarService {

    UserReportDTO getUserCalendar(
            String login,
            String startLocalDateTime,
            String endLocalDateTime) throws NoRegisteredException;

    UserReportDTO getUserCalendar(
            String login) throws NoRegisteredException;
}
