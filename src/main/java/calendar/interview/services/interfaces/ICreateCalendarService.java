package calendar.interview.services.interfaces;

import calendar.interview.data.dto.CreateCalendarDTO;
import calendar.interview.exceptions.CalendarValidationException;
import calendar.interview.exceptions.UnableToSaveUserException;

import java.util.UUID;

public interface ICreateCalendarService {
    UUID createCalendarSlot(CreateCalendarDTO createCalendarDTO) throws CalendarValidationException, UnableToSaveUserException;
}
