package calendar.interview.services;

import calendar.interview.data.entities.CalendarDB;
import calendar.interview.data.entities.UserDB;
import calendar.interview.exceptions.NoRegisteredException;
import calendar.interview.services.interfaces.IRemoveCalendarService;
import calendar.interview.services.repositories.CalendarRepositoryService;
import calendar.interview.services.repositories.UserRepositoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RemoveCalendarService implements IRemoveCalendarService {

    private final UserRepositoryService userRepositoryService;
    private final CalendarRepositoryService calendarRepositoryService;


    /**
     * Remove User Data By Login/Period
     */
    @Override
    public void removeUserCalendar(
            String login,
            String startLocalDateTime,
            String endLocalDateTime) throws NoRegisteredException {
        removeCalendar(login, startLocalDateTime, endLocalDateTime);
    }

    /**
     * Remove User Data By Login
     */
    @Override
    public void removeUserCalendar(String login) throws NoRegisteredException {
        removeUser(login);
    }


    private void removeUser(
            String login) throws NoRegisteredException {

        UserDB userDB = userRepositoryService.getUserSlotCalendar(login);
        userRepositoryService.removeUser(userDB);

    }

    private void removeCalendar(
            String login,
            String startLocalDateTime,
            String endLocalDateTime) throws NoRegisteredException {

        List<CalendarDB> slotCalendar = calendarRepositoryService.getSlotCalendar(login, startLocalDateTime, endLocalDateTime);
        calendarRepositoryService.removeUserCalendar(slotCalendar);

    }


}
