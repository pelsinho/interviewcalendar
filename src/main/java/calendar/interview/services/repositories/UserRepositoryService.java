package calendar.interview.services.repositories;

import calendar.interview.common.log.DefaultLog;
import calendar.interview.common.predicates.GenericPredicates;
import calendar.interview.common.suppliers.CalendarSuppliers;
import calendar.interview.data.entities.UserDB;
import calendar.interview.data.model.CreateCalendar;
import calendar.interview.exceptions.NoRegisteredException;
import calendar.interview.exceptions.UnableToSaveUserException;
import calendar.interview.repositories.UserRepository;
import calendar.interview.services.interfaces.IUserRepositoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

import static calendar.interview.common.constants.CalendarInformation.NO_REGISTER_FOUND;
import static calendar.interview.common.constants.CalendarInformation.UNABLE_TO_SAVE_REGISTER;

@Transactional
@Service
@Slf4j
@RequiredArgsConstructor
public class UserRepositoryService implements IUserRepositoryService {

    private final UserRepository userRepository;


    /**
     * GET User Info from DB based on Login and Period
     */
    @Override
    public UserDB buildUserDBWithCalendar(CreateCalendar createCalendar) {

        Optional<UserDB> user = userRepository.findByLogin(createCalendar.getLogin().name());

        UserDB userDB;

        if (user.isPresent()) {
            userDB = user.get();
        } else {
            userDB = new UserDB();
            userDB.setRoles(createCalendar.getLogin().getRole());
            userDB.setLogin(createCalendar.getLogin().name());
        }

        userDB.setCalendarList(CalendarSuppliers.getCalendarDB(createCalendar, userDB).get());

        return userDB;

    }



    /**
     * GET User Info from DB based on Login
     */
    @Override
    public UserDB getUserSlotCalendar(
            String login) throws NoRegisteredException {

        Optional<UserDB> userDB = userRepository.findByLogin(login);

        if (userDB.isPresent()) {
            return userDB.get();
        }

        DefaultLog.registerNotFoundLogError(getClass().getSimpleName(), login, "", "");
        throw new NoRegisteredException(NO_REGISTER_FOUND);

    }

    /**
     * Remove user info into DB
     */
    public void removeUser(UserDB userDB)  {

        if (GenericPredicates.checkIfNullOrEmpty.test(userDB)) return;
        userRepository.delete(userDB);

    }


    /**
     * Save user info into DB
     * At this moment we are not going to allow save a user without a calendar
     */
    @Override
    public UUID saveUserCalendar(UserDB userDB) throws UnableToSaveUserException {

        if (GenericPredicates.checkIfNullOrEmpty.test(userDB) ||
                GenericPredicates.checkIfNullOrEmpty.test(userDB.getCalendarList())) {
            throw new UnableToSaveUserException(UNABLE_TO_SAVE_REGISTER);
        }

        return userRepository.save(userDB).getId();

    }

}
