package calendar.interview.services.repositories;

import calendar.interview.common.enums.Login;
import calendar.interview.common.functions.GenericFunctions;
import calendar.interview.common.log.DefaultLog;
import calendar.interview.common.predicates.GenericPredicates;
import calendar.interview.data.entities.CalendarDB;
import calendar.interview.exceptions.NoRegisteredException;
import calendar.interview.repositories.CalendarRepository;
import calendar.interview.services.interfaces.ICalendarRepositoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static calendar.interview.common.constants.CalendarInformation.NO_REGISTER_FOUND;

@Transactional
@Service
@RequiredArgsConstructor
public class CalendarRepositoryService implements ICalendarRepositoryService {

    private final CalendarRepository calendarRepository;


    /**
     * GET Calendar Info from DB based on Login and Period
     */
    public List<CalendarDB> getSlotCalendar(
            String login,
            String startLocalDateTime,
            String endLocalDateTime) throws NoRegisteredException {

        LocalDateTime start = GenericFunctions.getSlotDateTimeFormatter.apply(startLocalDateTime);
        LocalDateTime end = GenericFunctions.getSlotDateTimeFormatter.apply(endLocalDateTime);

        List<CalendarDB> calendarSlotDTOList = calendarRepository.getSlotDateTimeByUserAndPeriod(Timestamp.valueOf(start), Timestamp.valueOf(end), login);

        if (GenericPredicates.checkIfNullOrEmpty.negate().test(calendarSlotDTOList)) {
            return calendarSlotDTOList;
        }

        DefaultLog.registerNotFoundLogError(getClass().getSimpleName(), login, startLocalDateTime, endLocalDateTime);
        throw new NoRegisteredException(NO_REGISTER_FOUND);

    }


    /**
     * Remove user calendar(s)
     */
    @Override
    public List<Timestamp> getInterviewAvailableSchedule(List<Login> loginList) {

        if (GenericPredicates.checkIfNullOrEmpty.test(loginList)) return new ArrayList<>();
        return calendarRepository.getInterviewAvailableSchedule(
                loginList.stream().map(Enum::name).collect(Collectors.toList()),
                (long) loginList.size());

    }


    /**
     * Remove user calendar(s)
     */
    @Override
    public void removeUserCalendar(List<CalendarDB> calendarList) {

        if (GenericPredicates.checkIfNullOrEmpty.test(calendarList)) return;
        calendarRepository.deleteAll(calendarList);

    }



}
