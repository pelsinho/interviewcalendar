package calendar.interview.repositories;

import calendar.interview.data.entities.CalendarDB;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Repository
public interface CalendarRepository extends JpaRepository<CalendarDB, UUID> {

    @Query(value = "SELECT cal.slotDateTime FROM CalendarDB AS cal " +
            "LEFT JOIN UserDB AS user ON user.id = cal.user.id " +
            "WHERE user.login in :usernameList " +
            "GROUP BY cal.slotDateTime " +
            "HAVING COUNT(cal.slotDateTime) = :countNumber")
    List<Timestamp> getInterviewAvailableSchedule(List<String> usernameList, Long countNumber);


    @Query(value = "SELECT cal FROM CalendarDB AS cal " +
            "LEFT JOIN UserDB AS user ON user.id = cal.user.id " +
            "WHERE user.login = :username " +
            "AND cal.slotDateTime >= :startLocalDateTime " +
            "AND cal.slotDateTime < :endLocalDateTime")
    List<CalendarDB> getSlotDateTimeByUserAndPeriod(Timestamp startLocalDateTime, Timestamp endLocalDateTime, String username);


}
