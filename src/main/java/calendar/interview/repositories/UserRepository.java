package calendar.interview.repositories;

import calendar.interview.data.entities.UserDB;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<UserDB, UUID> {

    Optional<UserDB> findByLogin(String username);

}
