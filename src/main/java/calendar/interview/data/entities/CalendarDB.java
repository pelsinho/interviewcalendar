package calendar.interview.data.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "calendar")
public class CalendarDB extends Audit {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(name = "slot_date_time")
    private Timestamp slotDateTime;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "app_user_id")
    private UserDB user;

}
