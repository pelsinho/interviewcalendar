package calendar.interview.data.entities;


import calendar.interview.common.enums.Roles;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "app_user")
public class UserDB extends Audit {

    @Id
    @GeneratedValue
    //@org.hibernate.annotations.Type(type = "pg-uuid")
    private UUID id;

    @Column(name = "roles")
    @NotNull
    @Enumerated(EnumType.STRING)
    private Roles roles;

    @Column(name = "login")
    @NotBlank
    @NotNull
    @Size(min = 1, max = 20)
    private String login;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    @NotNull
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonManagedReference
    private List<CalendarDB> calendarList = new ArrayList<>();

    @Override
    public String toString() {
        return "UserDB{" +
                "id=" + id +
                ", roles=" + roles +
                ", username='" + login + '\'' +
                '}';
    }

}
