package calendar.interview.data.dto;


import calendar.interview.common.enums.Roles;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserReportDTO {

    private UUID id;

    private Roles roles;

    private String login;

    private List<CalendarReportDTO> calendarList = new ArrayList<>();

}
