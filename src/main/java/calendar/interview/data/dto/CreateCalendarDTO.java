package calendar.interview.data.dto;

import calendar.interview.common.enums.Login;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import static calendar.interview.common.constants.CalendarInformation.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateCalendarDTO {

    @NotNull(message = THIS_FIELD_CANNOT_BE_NULL)
    @Pattern(regexp = SLOT_DATE_TIME_FORMAT_REGEX, message = SLOT_START_DATE_TIME_FORMAT_REGEX_MESSAGE)
    @ApiModelProperty(
            example = SLOT_START_DATE_TIME_EXAMPLE,
            required = true,
            dataType = SLOT_DATE_TIME_FORMAT,
            notes = START_CALENDAR_LOCAL_DATE_TIME_NOTES)
    private String startCalendarLocalDateTime;

    @NotNull(message = THIS_FIELD_CANNOT_BE_NULL)
    @Pattern(regexp = SLOT_END_DATE_TIME_FORMAT_REGEX, message = SLOT_END_DATE_TIME_FORMAT_REGEX_MESSAGE)
    @ApiModelProperty(
            example = SLOT_END_DATE_TIME_EXAMPLE,
            required = true,
            dataType = SLOT_DATE_TIME_FORMAT,
            notes = END_CALENDAR_LOCAL_DATE_TIME_NOTES)
    private String endCalendarLocalDateTime;

    @NotNull(message = THIS_FIELD_CANNOT_BE_NULL)
    @ApiModelProperty(example = LOGIN_NAME_EXAMPLE,
            required = true,
            dataType = LOGIN_NAME_DATA_TYPE,
            notes = LOGIN_NAME_NOTES)
    private Login login;


}
