package calendar.interview.data.dto;


import calendar.interview.common.enums.Login;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InterviewAvailableScheduleReportDTO {

    private Login candidate;

    private List<Login> interviewers;

    private List<LocalDateTime> interviewAvailableSchedules = new ArrayList<>();

}
