package calendar.interview.data.model;

import calendar.interview.common.enums.CalendarValidationPhase;
import calendar.interview.config.validation.SelfValidation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * This is a Generic Request Object for Validation Process, in the future we may add more validation objects
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CalendarValidationRequest extends SelfValidation<CalendarValidationRequest> {

    private CreateCalendar createCalendar;

    private InterviewSchedule interviewSchedule;

    @NotNull
    @NotEmpty
    private CalendarValidationPhase calendarValidationPhase;

}
