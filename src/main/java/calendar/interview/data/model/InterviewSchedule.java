package calendar.interview.data.model;


import calendar.interview.common.enums.Login;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InterviewSchedule {

    private Login candidate;

    private List<Login> interviewers;

}
