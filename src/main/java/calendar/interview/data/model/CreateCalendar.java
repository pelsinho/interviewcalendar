package calendar.interview.data.model;

import calendar.interview.common.enums.Login;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateCalendar {

    private LocalDateTime startCalendarLocalDateTime;

    private LocalDateTime endCalendarLocalDateTime;

    private Login login;

}
