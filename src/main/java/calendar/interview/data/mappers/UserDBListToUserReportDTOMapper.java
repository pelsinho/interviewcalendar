package calendar.interview.data.mappers;

import calendar.interview.common.predicates.GenericPredicates;
import calendar.interview.data.dto.UserReportDTO;
import calendar.interview.data.entities.UserDB;

public class UserDBListToUserReportDTOMapper {

    private UserDBListToUserReportDTOMapper() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    public static UserReportDTO userDBToUserReportDTO(UserDB userDB) {

        if (GenericPredicates.checkIfNullOrEmpty.test(userDB)) {
            return new UserReportDTO();
        }

        return UserReportDTO
                .builder()
                .calendarList(CalendarDBListToCalendarReportDTOListMapper.calendarDBListToCalendarReportDTOList(userDB.getCalendarList()))
                .id(userDB.getId())
                .login(userDB.getLogin())
                .roles(userDB.getRoles())
                .build();

    }


}
