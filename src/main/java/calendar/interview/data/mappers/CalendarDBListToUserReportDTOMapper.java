package calendar.interview.data.mappers;

import calendar.interview.common.predicates.GenericPredicates;
import calendar.interview.data.dto.UserReportDTO;
import calendar.interview.data.entities.CalendarDB;

import java.util.List;

public class CalendarDBListToUserReportDTOMapper {

    private CalendarDBListToUserReportDTOMapper() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    public static UserReportDTO calendarDBToUserReportDTO(List<CalendarDB> calendarDBList) {

        if (GenericPredicates.checkIfNullOrEmpty.test(calendarDBList)) {
            return new UserReportDTO();
        }

        return UserReportDTO
                .builder()
                .calendarList(CalendarDBListToCalendarReportDTOListMapper.calendarDBListToCalendarReportDTOList(calendarDBList))
                .id(calendarDBList.get(0).getId())
                .login(calendarDBList.get(0).getUser().getLogin())
                .roles(calendarDBList.get(0).getUser().getRoles())
                .build();

    }


}
