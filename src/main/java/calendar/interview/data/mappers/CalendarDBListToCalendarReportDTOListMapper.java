package calendar.interview.data.mappers;

import calendar.interview.common.predicates.GenericPredicates;
import calendar.interview.data.dto.CalendarReportDTO;
import calendar.interview.data.entities.CalendarDB;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CalendarDBListToCalendarReportDTOListMapper {

    private CalendarDBListToCalendarReportDTOListMapper() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    public static List<CalendarReportDTO> calendarDBListToCalendarReportDTOList(List<CalendarDB> calendarDBList) {

        if (GenericPredicates.checkIfNullOrEmpty.test(calendarDBList)) {
            return new ArrayList<>();
        }

        return calendarDBList.parallelStream()
                .map(calendarDB ->
                        CalendarReportDTO
                                .builder()
                                .id(calendarDB.getId())
                                .slotDateTime(calendarDB.getSlotDateTime().toLocalDateTime())
                                .build())
                .collect(Collectors.toList());

    }


}
