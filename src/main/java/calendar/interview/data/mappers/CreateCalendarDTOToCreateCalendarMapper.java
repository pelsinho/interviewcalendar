package calendar.interview.data.mappers;

import calendar.interview.common.functions.GenericFunctions;
import calendar.interview.common.predicates.GenericPredicates;
import calendar.interview.data.dto.CreateCalendarDTO;
import calendar.interview.data.model.CreateCalendar;

public class CreateCalendarDTOToCreateCalendarMapper {

    private CreateCalendarDTOToCreateCalendarMapper() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    public static CreateCalendar createCalendarDTOToCreateCalendar(CreateCalendarDTO createCalendarDTO) {

        if (GenericPredicates.checkIfNullOrEmpty.test(createCalendarDTO) ||
                GenericPredicates.checkIfNullOrEmpty.test(createCalendarDTO.getLogin()) ||
                GenericPredicates.checkIfNullOrEmpty.test(createCalendarDTO.getStartCalendarLocalDateTime()) ||
                GenericPredicates.checkIfNullOrEmpty.test(createCalendarDTO.getEndCalendarLocalDateTime())) {
            return new CreateCalendar();
        }

        return CreateCalendar
                .builder()
                .endCalendarLocalDateTime(GenericFunctions.getSlotDateTimeFormatter.apply(createCalendarDTO.getEndCalendarLocalDateTime()))
                .login(createCalendarDTO.getLogin())
                .startCalendarLocalDateTime(GenericFunctions.getSlotDateTimeFormatter.apply(createCalendarDTO.getStartCalendarLocalDateTime()))
                .build();

    }


}
