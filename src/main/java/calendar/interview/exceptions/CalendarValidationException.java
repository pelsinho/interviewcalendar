package calendar.interview.exceptions;

public class CalendarValidationException extends Exception {

    public CalendarValidationException(String message) {
        super(message);
    }

}
