package calendar.interview.exceptions;

public class UnableToSaveUserException extends Exception {

    public UnableToSaveUserException(String message) {
        super(message);
    }

}
