package calendar.interview.exceptions;

import lombok.Getter;
import java.time.LocalDateTime;

@Getter
public class ServiceException extends RuntimeException {

    private LocalDateTime localDateTime;
    private ExceptionType exceptionType;
    private String exceptionName;
    private String errorMessage;

    public ServiceException(
            LocalDateTime localDateTime,
            ExceptionType exceptionType,
            String exceptionName,
            String errorMessage) {
        this.localDateTime = localDateTime;
        this.exceptionType = exceptionType;
        this.errorMessage = errorMessage;
        this.exceptionName = exceptionName;
    }

}
