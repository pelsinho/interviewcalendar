package calendar.interview.exceptions;

public enum ExceptionType {
    NOT_FOUND,
    NOT_ALLOWED,
    UNAUTHORIZED,
    UNPROCESSABLE_ENTITY,
    FORBIDDEN,
    BAD_REQUEST,
    SERVER_ERROR,
    CONFLICT
}

