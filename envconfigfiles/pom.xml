<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>parent</groupId>
	<artifactId>pom</artifactId>
	<version>1.0</version>
	<packaging>pom</packaging>
	<description>Environment Parent POM</description>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.5.2</version>
		<relativePath/>
	</parent>

	<properties>

		<apache.commons.lang3>3.8.1</apache.commons.lang3>
		<apache.httpcomponents>4.3.4</apache.httpcomponents>

		<git-commit-id-maven-plugin>4.9.9</git-commit-id-maven-plugin>

		<hibernate.types.52>2.9.3</hibernate.types.52>

		<io.springfox.swagger2>2.9.2</io.springfox.swagger2>
		<io.springfox.swagger.ui>2.9.2</io.springfox.swagger.ui>
		<io.swagger.annotations>1.5.20</io.swagger.annotations>
		<io.swagger.bean.validators>2.9.2</io.swagger.bean.validators>

		<java.version>11</java.version>

		<jaxb-api>2.3.0</jaxb-api>
		<jaxb-impl>2.3.0</jaxb-impl>
		<jaxb-runtime>2.3.0</jaxb-runtime>
		<javax.activation>1.1.1</javax.activation>

		<junit.vintage.engine.version>5.3.2</junit.vintage.engine.version>

		<maven.checkstyle.version>3.0.0</maven.checkstyle.version>
		<maven.compiler.plugin>3.8.1</maven.compiler.plugin>
		<maven.jacoco.plugin>0.8.2</maven.jacoco.plugin>
		<maven.failsafe.plugin>2.22.2</maven.failsafe.plugin>
		<maven.surefire.plugin>2.22.2</maven.surefire.plugin>

		<mockito.core>3.11.0</mockito.core>

		<postgresql>42.2.23</postgresql>
		<projectlombok>1.18.20</projectlombok>

		<puppycrawl.checkstyle.version>8.19</puppycrawl.checkstyle.version>

		<spring-boot-admin.version>2.3.1</spring-boot-admin.version>
		<spring-boot-admin-server-ui-login>1.5.7</spring-boot-admin-server-ui-login>
		<spring-boot-admin-starter-client>2.4.1</spring-boot-admin-starter-client>
		<spring-boot-resilience4j>1.4.0</spring-boot-resilience4j>
		<spring-boot-starter-security>2.4.0</spring-boot-starter-security>

		<spring-cloud.version>2020.0.3</spring-cloud.version>
		<spring-cloud-starter-zipkin>2.2.8.RELEASE</spring-cloud-starter-zipkin>

		<spring-boot-native>0.10.1</spring-boot-native>

	</properties>

	<!-- MVN PROFILES -->
	<profiles>
		<profile>
			<id>local</id>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
			<properties>
				<spring.profiles.active>local</spring.profiles.active>
			</properties>
			<dependencies>
				<dependency>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-devtools</artifactId>
					<scope>runtime</scope>
				</dependency>
			</dependencies>
		</profile>
		<profile>
			<id>native</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.springframework.boot</groupId>
						<artifactId>spring-boot-maven-plugin</artifactId>
						<configuration>
							<classifier>${repackage.classifier}</classifier>
							<image>
								<builder>paketobuildpacks/builder:tiny</builder>
								<env>
									<BP_NATIVE_IMAGE>true</BP_NATIVE_IMAGE>
								</env>
							</image>
						</configuration>
					</plugin>
					<plugin>
						<groupId>org.springframework.experimental</groupId>
						<artifactId>spring-aot-maven-plugin</artifactId>
						<version>${spring-native.version}</version>
						<executions>
							<execution>
								<id>test-generate</id>
								<goals>
									<goal>test-generate</goal>
								</goals>
							</execution>
							<execution>
								<id>generate</id>
								<goals>
									<goal>generate</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
		<profile>
			<id>prd</id>
			<properties>
				<spring.profiles.active>prd</spring.profiles.active>
			</properties>
		</profile>
		<profile>
			<id>test</id>
			<properties>
				<spring.profiles.active>test</spring.profiles.active>
			</properties>
			<dependencies>
				<dependency>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-devtools</artifactId>
					<scope>runtime</scope>
				</dependency>
			</dependencies>
		</profile>

	</profiles>

	<dependencies>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-validation</artifactId>
		</dependency>

		<!-- Spring Admin Client -->
		<dependency>
			<groupId>de.codecentric</groupId>
			<artifactId>spring-boot-admin-starter-client</artifactId>
			<version>${spring-boot-admin-starter-client}</version>
		</dependency>

		<!-- Spring Cloud Client -->
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-config-client</artifactId>
		</dependency>

		<!-- Spring Native AOT Client -->
		<dependency>
			<groupId>org.springframework.experimental</groupId>
			<artifactId>spring-native</artifactId>
			<version>${spring-boot-native}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.experimental</groupId>
			<artifactId>spring-aot</artifactId>
			<version>${spring-boot-native}</version>
		</dependency>

		<!-- Spring Mail -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-mail</artifactId>
		</dependency>

		<dependency>
			<groupId>com.sun.mail</groupId>
			<artifactId>javax.mail</artifactId>
			<version>1.6.2</version>
		</dependency>

		<!-- Spring Resilient 4J -->
		<dependency>
			<groupId>io.github.resilience4j</groupId>
			<artifactId>resilience4j-spring-boot2</artifactId>
		</dependency>

		<!-- Spring Swagger -->
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
			<version>${io.springfox.swagger.ui}</version>
		</dependency>

		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-bean-validators</artifactId>
			<version>${io.swagger.bean.validators}</version>
		</dependency>

		<dependency>
			<groupId>io.swagger</groupId>
			<artifactId>swagger-annotations</artifactId>
			<version>${io.swagger.annotations}</version>
		</dependency>

		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
			<version>${io.springfox.swagger2}</version>
		</dependency>

		<!-- DB -->
		<dependency>
			<groupId>org.flywaydb</groupId>
			<artifactId>flyway-core</artifactId>
		</dependency>

		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
			<version>${postgresql}</version>
		</dependency>

		<dependency>
			<groupId>com.vladmihalcea</groupId>
			<artifactId>hibernate-types-52</artifactId>
			<version>${hibernate.types.52}</version>
		</dependency>

		<!-- Apache Libs -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>${apache.commons.lang3}</version>
		</dependency>

		<!-- Lombok -->
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<version>${projectlombok}</version>
		</dependency>

		<!-- Prometheus Metrics -->
		<dependency>
			<groupId>io.micrometer</groupId>
			<artifactId>micrometer-registry-prometheus</artifactId>
		</dependency>

		<!--XML Additional -->
		<dependency>
			<groupId>javax.xml.bind</groupId>
			<artifactId>jaxb-api</artifactId>
			<version>${jaxb-api}</version>
		</dependency>

		<dependency>
			<groupId>com.sun.xml.bind</groupId>
			<artifactId>jaxb-impl</artifactId>
			<version>${jaxb-impl}</version>
		</dependency>

		<dependency>
			<groupId>org.glassfish.jaxb</groupId>
			<artifactId>jaxb-runtime</artifactId>
			<version>${jaxb-runtime}</version>
		</dependency>

		<dependency>
			<groupId>javax.activation</groupId>
			<artifactId>activation</artifactId>
			<version>${javax.activation}</version>
		</dependency>

	</dependencies>

	<dependencyManagement>

		<dependencies>

			<dependency>
				<groupId>de.codecentric</groupId>
				<artifactId>spring-boot-admin-dependencies</artifactId>
				<version>${spring-boot-admin.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>

			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>${spring-cloud.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>

		</dependencies>

	</dependencyManagement>

	<build>

		<finalName>${project.artifactId}</finalName>

		<plugins>

			<!-- Git Commit Id Plug-In to be Used with Spring/Actuator/Info endpoint -->
			<plugin>
				<groupId>io.github.git-commit-id</groupId>
				<artifactId>git-commit-id-maven-plugin</artifactId>
				<version>${git-commit-id-maven-plugin}</version>
				<executions>
					<execution>
						<id>get-the-git-infos</id>
						<goals>
							<goal>revision</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<dotGitDirectory>${project.basedir}/.git</dotGitDirectory>
					<prefix>git</prefix>
					<verbose>false</verbose>
					<generateGitPropertiesFile>true</generateGitPropertiesFile>
					<generateGitPropertiesFilename>${project.build.outputDirectory}/git.properties</generateGitPropertiesFilename>
					<format>json</format>
					<gitDescribe>
						<skip>false</skip>
						<always>false</always>
						<dirty>-dirty</dirty>
					</gitDescribe>
				</configuration>
			</plugin>

			<!--CheckStyle -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
				<version>${maven.checkstyle.version}</version>
				<dependencies>
					<dependency>
						<groupId>com.puppycrawl.tools</groupId>
						<artifactId>checkstyle</artifactId>
						<version>${puppycrawl.checkstyle.version}</version>
					</dependency>
				</dependencies>
				<executions>
					<execution>
						<id>validate</id>
						<phase>validate</phase>
						<configuration>
							<excludes>
							</excludes>
							<configLocation>checkstyle.xml</configLocation>
							<consoleOutput>true</consoleOutput>
							<logViolationsToConsole>false</logViolationsToConsole>
							<encoding>UTF-8</encoding>
						</configuration>
						<goals>
							<goal>check</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!--Compiler With Lombok -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>${maven.compiler.plugin}</version>
				<configuration>
					<source>${java.version}</source>
					<target>${java.version}</target>
					<annotationProcessorPaths>
						<path>
							<groupId>org.projectlombok</groupId>
							<artifactId>lombok</artifactId>
							<version>${projectlombok}</version>
						</path>
					</annotationProcessorPaths>
					<useIncrementalCompilation>false</useIncrementalCompilation> <!-- https://issues.apache.org/jira/browse/MCOMPILER-209 -->
				</configuration>
			</plugin>

			<!--Jacoco Test Report -->
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>${maven.jacoco.plugin}</version>
				<executions>
					<execution>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
					<execution>
						<id>report</id>
						<phase>prepare-package</phase>
						<goals>
							<goal>report</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!-- SureFire - Run Unit Tests -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>${maven.surefire.plugin}</version>
				<dependencies>
					<dependency>
						<groupId>org.junit.jupiter</groupId>
						<artifactId>junit-jupiter-engine</artifactId>
						<version>${junit-jupiter.version}</version>
					</dependency>
				</dependencies>
				<configuration>
					<forkedProcessExitTimeoutInSeconds>240</forkedProcessExitTimeoutInSeconds>
					<includes>
						<include>**/*.class</include>
					</includes>
					<excludes>
						<exclude>**/*_FT.java</exclude>
					</excludes>
					<useSystemClassLoader>false</useSystemClassLoader>
				</configuration>
			</plugin>

			<!-- Run Functional Tests -->
			<!-- Tags -> https://mkyong.com/junit5/junit-5-tagging-and-filtering-tag-examples/ -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-failsafe-plugin</artifactId>
				<version>${maven.failsafe.plugin}</version>
				<executions>
					<execution>
						<id>api-tests</id>
						<phase>none</phase>
						<configuration>
							<testSourceDirectory>src/test/java/functionaltests</testSourceDirectory>
							<includes>
								<include>**/*FT*</include>
							</includes>
						</configuration>
					</execution>
				</executions>
				<configuration>
					<forkedProcessExitTimeoutInSeconds>240</forkedProcessExitTimeoutInSeconds>
					<useSystemClassLoader>false</useSystemClassLoader>
					<trimStackTrace>false</trimStackTrace>
				</configuration>
			</plugin>

			<!-- Spring Boot -->
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<configuration>
					<excludeDevtools>false</excludeDevtools>
					<excludes>
						<exclude>
							<groupId>org.projectlombok</groupId>
							<artifactId>lombok</artifactId>
						</exclude>
					</excludes>
				</configuration>
			</plugin>

		</plugins>
	</build>

	<repositories>

		<repository>
			<id>spring-release</id>
			<name>Spring release</name>
			<url>https://repo.spring.io/release</url>
		</repository>

	</repositories>

	<pluginRepositories>

		<pluginRepository>
			<id>spring-release</id>
			<name>Spring release</name>
			<url>https://repo.spring.io/release</url>
		</pluginRepository>

	</pluginRepositories>

</project>
