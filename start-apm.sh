mvn package -Dmaven.test.skip=true -DskipTests -Djacoco.skip=true -Dspring-boot.run.profiles=local
cd target
echo ------------------------------------------------------
echo - Kibana Access Link : http://localhost:5601/app/apm -
echo ------------------------------------------------------
java -javaagent:../src/main/resources/apm/elastic-apm-agent-1.25.0.jar \
     -Delastic.apm.service_name=interview-calendar \
     -Delastic.apm.server_urls=http://localhost:8200 \
     -Delastic.apm.environment=local \
     -Delastic.apm.application_packages=interviewcalendar \
     -Dspring-boot.run.profiles=local \
     -jar interview.jar