FROM adoptopenjdk/maven-openjdk11

EXPOSE 8080

ENV APP_USER="app"
ENV MAVEN_CLI_OPTS: "-s .m2/settings.xml --batch-mode"
ENV MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"

MAINTAINER nelsongomes.com

#In Progress
#COPY checkstyle.xml /tmp/
#COPY pom.xml /tmp/
#COPY src /tmp/src/
#WORKDIR /tmp/
#RUN mvn clean package -Dmaven.test.skip=true -DskipTests -Djacoco.skip=true

COPY target/interview.jar interview.jar
COPY src/main/resources/apm/elastic-apm-agent-1.25.0.jar elastic-apm-agent-1.25.0.jar

ENTRYPOINT ["java","-jar","/interview.jar"]
#ENTRYPOINT ["java", "-javaagent:/elastic-apm-agent-1.25.0.jar","-Delastic.apm.service_name=interview-calendar","-Delastic.apm.server_urls=http://localhost:8200","-Delastic.apm.environment=local","-Delastic.apm.application_packages=interviewcalendar","-jar","/interview.jar"]
